/**
 * @file	loader.s
 * @author  Emil Madsen <skeen@cs.au.dk>
 * @version 1.0
 *
 * @section LICENSE
 *
 * Public Domain.
 *
 * @section DESCRIPTION
 *
 * Contains the Kernel start location.
 * Defined the multiboot header. 
 * Also calls C++ static constructors and destructors.
 */

# Export the entry point (make it visible to the linker)
.global loader

# setting up the Multiboot header - see GRUB docs for details
.set MBOOT_PAGE_ALIGN,    1<<0                                      # align loaded modules/kernel on page boundaries
.set MBOOT_PAGE_MEMINFO,  1<<1                                      # provide memory map
# NOTE: We do not use MBOOT_AOUT_KLUDGE. It means that GRUB does not
# pass us a symbol table.
.set MBOOT_HEADER_FLAGS,    MBOOT_PAGE_ALIGN | MBOOT_PAGE_MEMINFO   # this is the Multiboot 'flag' field
.set MBOOT_HEADER_MAGIC,    0x1BADB002                              # 'magic number' lets bootloader find the header
.set MBOOT_CHECKSUM, -(MBOOT_HEADER_MAGIC + MBOOT_HEADER_FLAGS)     # checksum required
 
.align 4
.long MBOOT_HEADER_MAGIC
.long MBOOT_HEADER_FLAGS
.long MBOOT_CHECKSUM
 
# reserve initial kernel stack space
.set STACKSIZE, 0x4000                  # that is, 16k.
.comm stack, STACKSIZE, 32             # reserve 16k stack on a doubleword boundary
.comm mbd, 4                           # we will use this in kmain
.comm magic, 4                         # we will use this in kmain
 
loader:
    mov   $(stack + STACKSIZE), %esp    # set up the stack
    movl  %eax, magic                   # Multiboot magic number
    movl  %ebx, mbd                     # Multiboot data structure

    # Load multiboot information:
    push %ebx
    push %eax
 
    call kernel_main                          # call kernel proper

    cli
hang:
    hlt                                 # halt machine should kernel return
    jmp   hang

