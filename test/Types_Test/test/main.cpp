#include "../test.h"

#include <misc/types.h>
#include <stdint.h>

void func()
{
    test(sizeof(int8_t)==sizeof(s8int), "int8_t = s8int (size check)");
    test(sizeof(int16_t)==sizeof(s16int), "int16_t = s16int (size check)");
    test(sizeof(int32_t)==sizeof(s32int), "int32_t = s32int (size check)");
    test(sizeof(int64_t)==sizeof(s64int), "int64_t = s64int (size check)");
    
    test(sizeof(uint8_t)==sizeof(u8int), "uint8_t = u8int (size check)");
    test(sizeof(uint16_t)==sizeof(u16int), "uint16_t = u16int (size check)");
    test(sizeof(uint32_t)==sizeof(u32int), "uint32_t = u32int (size check)");
    test(sizeof(uint64_t)==sizeof(u64int), "uint64_t = u64int (size check)");
}

void bar()
{
}


TEST_FUNC(func);
