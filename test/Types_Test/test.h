#include <misc/compiler.h>

typedef void (*init_function_pointer)(int magic, void* mbd);
typedef void (*start_function_pointer)();
typedef void (*test_function_pointer)();

extern "C" init_function_pointer start_test_init[];
extern "C" start_function_pointer start_test_start[];
extern "C" test_function_pointer start_test_func[];

#define TEST_INIT(function) SECTION(".testcall6.init") \
void* test_init_##function = (void*) function
#define TEST_START(function) SECTION(".testcall6.start") \
void* test_start_##function = (void*) function
#define TEST_FUNC(function) SECTION(".testcall6.func") \
void* test_func_##function = (void*) function

void test(bool b, const char* msg);
