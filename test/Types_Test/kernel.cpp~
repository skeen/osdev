/**
 * @file	kernel.c
 * @author  Emil Madsen <skeen@cs.au.dk>
 * @version 1.0
 *
 * @section LICENSE
 *
 * Public Domain.
 *
 * @section DESCRIPTION
 *
 * The kernel entry point
 */

#include <misc/compiler.h>
#include <misc/new.hpp>
#include <drivers/Serial/Serial.hpp>
#include <drivers/EmulatorShutdown/EmulatorShutdown.h>

#include "test.h"

Serial *com1;

void test(bool b, const char* msg)
{
    if(b)
    {
        com1->write("1 : ");
    }
    else
    {
        com1->write("0 : ");
    }
    com1->write(msg);
    com1->write('\n');
}

ASM_LINK int kernel_main(int magic, void* mbd)
{
    Serial s = Serial(1);
    com1 = &s;

    u32int i = 0;
    while(true)
    {
        test_function_pointer test_func = start_test_func[i];
        if(test_func == nullptr)
        {
            break;
        }
        i++;
    }
    u32int number_of_tests = i;
    com1->write(number_of_tests);
    com1->write(" tests found\n");

    if(start_test_init[0] != nullptr)
    {
        init_function_pointer init = start_test_init[0];
        init(magic, mbd);
    }
    for(u32int x = 0; x < number_of_tests; x++)
    {
        if(start_test_start[0] != nullptr)
        {
            start_function_pointer start = start_test_start[0];
            start();
        }
        test_function_pointer test_func = start_test_func[x];
        test_func();
    }

    // Shutdown this test
    EmulatorShutdown();
    // A code will be easy to spot, in the debug log
    return 0xDEADDEAD;
}

