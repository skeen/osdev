#!/usr/bin/env python
import os, sys

os.system("scons --tree=all --just-print " + " ".join(sys.argv[1:]) + " | ./conf/graph.py | dot -Tsvg -o build/deps_graph.svg")
