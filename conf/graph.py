#!/usr/bin/python

import sys

class node(object):
        def __init__(self,path):
                self.path = path
                self.children = []

        def add_child(self,child):      
                if not child in self.children: self.children.append(child)

        def print_rec(self,depth):
                print " "*depth,self.name
                for c in self.children:
                        c.print_rec(depth+1)

        def get_name(self):
                return self.clean_for_dot(self.path)
        
        def get_label_short(self):
                return self.path
                
        def clean_for_dot(self,expr):
                return expr.replace(".","").replace("/","").replace("-","")

        def is_fixed_tool(self):
                return self.path[0] == "/"



def initial_filter(datalines):
        return [d for d in datalines if d.strip() and d.strip()[0] in ["|","+"] ] 



def main():
        data = sys.stdin.readlines()
        data = initial_filter(data)

        root = node("root")
        scons_obj_stack = []
        scons_obj_stack.append( root )
        scons_object_map = {}
        def does_object_exist(name): return name in scons_object_map.keys()


        # Read the Data:
        for l in data:

                l= l.replace("+","").replace("-","").replace("|"," ")   #Remove the "tree parts" from the text
                nodename =  l.strip()
                if nodename[0] != "/": nodename = "./"+nodename

                
                if not does_object_exist(nodename):
                        scons_object_map[nodename] = node(nodename)

                depth = (len(l) - len(l.strip() ) +1)/2
                
                # If the depth is one higher than the depth of the stack,
                # add as a child object.
                if len(scons_obj_stack) == depth -1:
                        scons_obj_stack[-1].add_child( scons_object_map[nodename] )
                        scons_obj_stack.append( scons_object_map[nodename] )
                
                # Otherwise pop the stack until we reach the parent... and add as a child
                else:
                        while( len(scons_obj_stack) > depth ) : scons_obj_stack.pop()

                        scons_obj_stack[-1].add_child( scons_object_map[nodename] )
                        scons_obj_stack.append( scons_object_map[nodename] )
                        


        #root.print_rec(0)      
        

        o = sys.stdout
        def wl(s): o.write("%s\n"%s)


        wl("digraph G {")
        wl("""size="100x100" """)
        wl("""rankdir=RL """)
        wl("ratio = 1")

        #Nodes:
        for scons_obj in scons_object_map.values():
                if not scons_obj.is_fixed_tool():
                        wl("""%s [label="%s"]"""%(scons_obj.get_name(),scons_obj.get_label_short()) )
        
        #Connections:
        for scons_obj in scons_object_map.values():
                for c in scons_obj.children:
                        if not scons_obj.is_fixed_tool() and not c.is_fixed_tool():
                                wl("%s -> %s"%(scons_obj.get_name(), c.get_name() ) )
        wl("}")

main()
