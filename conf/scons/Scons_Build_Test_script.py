Import(['env'])

env['BUILDDIR'] = "#build/test"

# Recursive build all
SConscript('#test/SConscript', variant_dir = env['BUILDDIR'], exports = ['env'], duplicate = 0)
