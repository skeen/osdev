import os
import sys
import string

Import(['env'])

from SCons.Builder import Builder

def breakUpLink(source):
    source_string = str(source)
    index = string.rfind(source_string, "/")

    libpath = source_string[:index]
    libpath = "#build/" + libpath
    library = source_string[index+1:]

    #print("libpath: " + libpath)
    #print("library: " + library)

    return library, libpath

def LinkWith(env, sources):
    if len(sources) >= 1:
        libraries = []
        libpaths = []
        for link in sources:
            lib,libpath = breakUpLink(str(link))
            libraries.append(lib)
            libpaths.append(libpath)
        return libraries, libpaths
    else:
        return [],[]

env.AddMethod(LinkWith, "LinkWith")
