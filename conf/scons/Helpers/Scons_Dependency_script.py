import os
import sys
import string

Import(['env'])

def removeLast(x):
    index = string.rfind(x, "/")
    return x[:index]

def root_path(x):
    root_dir = Dir('#').srcnode().abspath
    current_dir = Dir('.').srcnode().abspath
    path = os.path.relpath(root_dir, current_dir) + "/" + x
    return path

def forDependency(env, sources):
    dependency_paths = [root_path(x) for x in sources]
    remove_last = [removeLast(x) for x in dependency_paths]

    #print(dependency_paths)
    #print(remove_last)

    return remove_last

env.AddMethod(forDependency, "forDependency")
