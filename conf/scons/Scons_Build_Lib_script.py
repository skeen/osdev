Import(['env'])

env['BUILDDIR'] = "#build/lib"

# Recursive build all
SConscript('#lib/SConscript', variant_dir = env['BUILDDIR'], exports = ['env'], duplicate = 0)
