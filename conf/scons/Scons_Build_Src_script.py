# -*- coding: utf-8 -*-
Import(['env'])

env['BUILDDIR'] = "#build/src"

from Scons_Compiler_Flags import *

env.Append(CFLAGS=CFLAGS)
#env.Append(CCFLAGS=CFLAGS)
env.Append(CXXFLAGS=CXXFLAGS)
env.Append(ASFLAGS=ASFLAGS)
env.Append(LINKFLAGS=LINKFLAGS)

#env["_LIBFLAGS"] = " --start-group " + env["_LIBFLAGS"] + " --end-group "

# Recursive build all
SConscript('#src/SConscript', variant_dir = env['BUILDDIR'], exports = ['env'], duplicate = 0)
