import os
import sys

Import(['env'])

crossBase = "./tools/crossgcc/bin/i586-elf-"

env['CC']       = crossBase + 'gcc'
env['CXX']      = crossBase + 'g++'
env['AS']       = crossBase + 'as'
env['AR']       = crossBase + 'ar'
env['RANLIB']   = crossBase + 'ranlib'
env['LD']       = crossBase + 'ld'
env['OBJCOPY']  = crossBase + 'objcopy'
env['LINK']     = env['LD']
