import os
Import(['env'])

objname = 'doxygen'
env.jAlias("doc", objname, "Builds the documentation of the source code")

def generateDocumentation(target, source, env):
    cmd = "doxygen conf/doxygen/Doxyfile.cfg"
    os.system(cmd)

env.Command(objname, [], Action(generateDocumentation, None))
