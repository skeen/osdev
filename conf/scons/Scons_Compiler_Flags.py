# Default flags for SkeenOS's build, per architecture

# Generic entry-level flags (that everyone should have)
c_flags = '-std=gnu99 -fno-builtin -nostdinc -nostdlib -nodefaultlibs -ffreestanding \
           -fno-builtin -nostartfiles -g0  -finline-functions -O0 -fno-stack-protector \
           -fstrength-reduce -fomit-frame-pointer '
cxx_flags = c_flags.replace('-std=gnu99', '-std=c++11') + '-fno-exceptions -fno-rtti '

# Warning flags (that force us to write betterish code)
warning_flags = ' -Wfatal-errors -Wall -Wextra -Werror -Wpointer-arith -Wcast-align \
                  -Wwrite-strings -Wno-long-long -Wno-variadic-macros -Wno-unused \
                  -Wno-unused-variable -Wno-conversion -Wno-format -Wno-empty-body '

# Language-specific warnings
warning_flags_c = ''#'-Wnested-externs '
warning_flags_cxx = ''

# Generic assembler flags
as_flags = '--warn -am '

# Generic link flags (that everyone should have)
link_flags = ' -nostdlib -z nodefaultlib '

# 32-bit CFLAGS and CXXFLAGS
x86_c_flags = ' -march=i586 '
x86_cxx_flags = ' -march=i586 '

# 32-bit linker flags
x86_link_flags = ' -T conf/linker.ld '

# OUTPUT FLAGS
CFLAGS = c_flags + x86_c_flags + warning_flags + warning_flags_c
CXXFLAGS = cxx_flags + x86_cxx_flags + warning_flags + warning_flags_cxx
ASFLAGS = as_flags
LINKFLAGS = x86_link_flags + link_flags
