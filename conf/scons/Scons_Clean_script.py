import os
Import(['env'])

objname = 'clean-all'
env.jAlias("clean", objname, "Cleans up the build environment")

def cleanBuild():
    cmd = "rm -rf build"
    os.system(cmd)

def cleanDoc():
    cmd = "rm -rf doc"
    os.system(cmd)

def cleanAll(target, source, env):
    cleanBuild()
    cleanDoc()

env.Command(objname, [], Action(cleanAll, None))

