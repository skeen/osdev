import shutil, os

def PublishLibrary_Worker(target, source, env):
    if(len(target) > 1):
        print("ERROR: PublishLibrary_Worker takes a single string target argument")
        return 1

    if(len(source) > 1):
        print("ERROR: PublishLibrary_Worker takes a single string source argument")
        return 1

    # Copy the result out
    shutil.copyfile(str(source[0]), str(target[0]))
    
    return 0  
    
