import os, subprocess
import string, random
import shutil

Import(['env'])

from SCons.Builder import Builder

qemu_path = "./tools/qemu/qemu-system-i386w.exe"

def RunImage_Worker(source_file_path):
    # Setup the qemu executable path
    cmd = qemu_path
    # The cdrom image
    cmd = cmd + " -cdrom " + source_file_path
    # The BIOS location
    cmd = cmd + " -L tools/qemu/bios"
    # Redirect serial to telnet 127.0.0.1:4444
    #cmd = cmd + " -serial mon:telnet:127.0.0.1:4444,server,nowait"
    cmd = cmd + " -serial file:" + "OUTPUT.txt"
    # Execute qemu
    os.system(cmd)

def RunImage(target, source, env):
    if(len(source) > 1):
        print("ERROR: RunImage takes a single string source argument")
        return 1

    # We need to isolate the file name
    source_file_path = str(source[0])

    RunImage_Worker(source_file_path)

env['BUILDERS']['RunImage'] = Builder(
    action=RunImage,
    src_suffix='.iso')
