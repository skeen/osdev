import os, subprocess
import string, random
import shutil
import subprocess
import time

Import(['env'])

from SCons.Builder import Builder

def print_evaluation(source):
    passed_tests = 0
    failed_tests = 0

    f = open(source)
    lines = f.readlines()
    f.close()

    print("FAILED TESTS:")
    for line in lines:
        if line == "SHUTDOWN":
            break
        # Increment counter
        char = line[0]
        if char == '0':
            failed_tests += 1
            # Print out testing message
            print("* " + line[4:(len(line)-1)])
        else:
            passed_tests += 1
    # return values
    return passed_tests, failed_tests

def EvaluateTest(target, source, env):
    if(len(target) > 1):
        print("ERROR: EvaluateTest takes a single string target argument")
        return 1

    total_passed_tests = 0
    total_failed_tests = 0
    for i in source:
        i_passed_tests, i_failed_tests = print_evaluation(str(i))
        total_passed_tests += i_passed_tests
        total_failed_tests += i_failed_tests

    total_tests = total_passed_tests + total_failed_tests

    print("PASS " + str(total_passed_tests) + " / " + str(total_tests))
    print("FAIL " + str(total_failed_tests) + " / " + str(total_tests))
    
    return 0

env['BUILDERS']['EvaluateTest'] = Builder(
    action=EvaluateTest)
