import os, subprocess
import string, random
import shutil

Import(['env'])

from SCons.Builder import Builder

mkisofs_path = "./tools/mkisofs.exe"
image_path = "tools/image"

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for x in range(size))

def createImage(tmp_folder, target_file_path):
    cmd = mkisofs_path + " -input-charset ascii -R \
            -b boot/grub/stage2_eltorito \
            -no-emul-boot -boot-load-size 4 -boot-info-table \
            -o " + target_file_path + " " + tmp_folder;
    os.system(cmd);

def makeImage(source_file_path, target_file_path):

    tmp_folder = "tmp/" + id_generator() + "/"
    shutil.copytree(image_path, tmp_folder)
    shutil.copyfile(source_file_path, tmp_folder + "boot/kernel.bin")

    createImage(tmp_folder, target_file_path)

    shutil.rmtree(tmp_folder)

def BuildCDImage_Worker(target, source, env):
    # We need to isolate the file name
    source_file_path = str(source[0])

    # We need to isolate the file name
    target_file_name = os.path.basename(str(target[0]))

    # We need to isolate the file path
    target_file_path = os.path.dirname(str(source[0]))
    # Build the image and clean up
    makeImage(source_file_path, target_file_path + "/" + target_file_name)
    
def BuildCDImage(target, source, env):
    if(len(target) > 1):
        print("ERROR: BuildCDImage_Worker takes a single string target argument")
        return 1
    if(len(source) > 1):
        print("ERROR: BuildCDImage_Worker takes a single string source argument")
        return 1
    
    BuildCDImage_Worker(target, source, env)

env['BUILDERS']['BuildCDImage'] = Builder(
    action=BuildCDImage,
    suffix='.iso',
    src_suffix='.bin')
