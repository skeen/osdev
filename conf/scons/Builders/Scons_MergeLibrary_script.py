import os, subprocess
import string, random
import shutil

Import(['env'])

from Scons_PublishLibrary_Worker_script import *

from SCons.Builder import Builder

ar_path = env['AR']
ranlib_path = env['RANLIB']

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for x in range(size))

def MergeLibrary_Worker(target, source, env):
    # Let's make a working folder
    target_tmp = 'tmp/' + id_generator() + '/'
    if not os.path.exists(target_tmp):
        os.makedirs(target_tmp)

    # Now let's change to this new directory
    saved_global_Path = os.getcwd()
    
    # For each sublibrary
    for my_source in source:
        # Make an unpacking folder
        subdir = target_tmp + id_generator()
        if not os.path.exists(subdir):
            os.makedirs(subdir)

        # We need to isolate the file name
        my_source_file_name = os.path.basename(str(my_source))
        shutil.copyfile(str(my_source), subdir + '/' + my_source_file_name)

        # Now let's change to this new directory
        #saved_library_Path = os.getcwd()
        os.chdir(subdir)

        # Execute the command to unpack the library
        os.system(ar_path + " x " + my_source_file_name)

        # Get the old directory back
        os.chdir(saved_global_Path)

    # We need to isolate the file name
    target_file_name = os.path.basename(str(target[0]))

    # Lets go to our temporary root
    os.chdir(target_tmp)

    # Pack everything into a single library
    os.system(ar_path + " cr " + target_file_name + " */*.o")
    # Ranlib the library
    os.system(ranlib_path + " " + target_file_name)

    # Restore the global directory 
    os.chdir(saved_global_Path)

    # Lets publish our new library
    PublishLibrary_Worker_Internal(target_file_name, target_tmp + target_file_name)

    # Remove all our temporary stuff
    shutil.rmtree('tmp', ignore_errors=True)

    return None

def MergeLibrary(target, source, env):
    if(len(target) > 1):
        print("ERROR: MergeLibrary_Worker takes a single string target argument")
        return 1
    
    MergeLibrary_Worker(target, source, env)

env['BUILDERS']['MergeLibrary'] = Builder(
    action=MergeLibrary,
    suffix='.a',
    src_suffix='.a',
    prefix='lib')


