import shutil, os

Import(['env'])

from Scons_PublishLibrary_Worker_script import *

from SCons.Builder import Builder

env['BUILDERS']['PublishLibrary'] = Builder(
    action=PublishLibrary_Worker,
    suffix='.a',
    src_suffix='.a',
    prefix='lib')


