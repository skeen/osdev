import os, subprocess
import string, random
import shutil
import subprocess
import time

Import(['env'])

from SCons.Builder import Builder

qemu_path = "./tools/qemu/qemu-system-i386w.exe"

def TestImage_Worker(target_file_path, source_file_path):
    # Setup the qemu executable path
    cmd = qemu_path
    # The cdrom image
    cmd = cmd + " -cdrom " + source_file_path
    # The BIOS location
    cmd = cmd + " -L tools/qemu/bios"
    # Redirect serial to console
    cmd = cmd + " -serial file:" + target_file_path
    # Disable graphics
    cmd = cmd + " -nographic"
    # Execute qemu
    proc = subprocess.Popen(cmd, shell=True)
    proc.wait();
            

def TestImage(target, source, env):
    if(len(source) > 1):
        print("ERROR: TestImage takes a single string source argument")
        return 1
    if(len(target) > 1):
        print("ERROR: TestImage takes a single string target argument")
        return 1

    # We need to isolate the file name
    source_file_path = str(source[0])
    target_file_path = str(target[0])

    TestImage_Worker(target_file_path, source_file_path)
    return 0

env['BUILDERS']['TestImage'] = Builder(
    action=TestImage,
    src_suffix='.iso')
