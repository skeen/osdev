struct Dl_info {
    const char * dli_fname;
    void       * dli_fbase;
    const char * dli_sname;
    void       * dli_saddr;
    int          dli_version;
    int          dli_reserved1;
    long         dli_reserved[4];
};

int dladdr(void*, Dl_info*) {
	return 0;
}

