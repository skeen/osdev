/**
 * @file	compiler.h
 * @author  Emil Madsen <skeen@cs.au.dk>
 * @version 1.0
 *
 * @section LICENSE
 *
 * Public Domain.
 *
 * @section DESCRIPTION
 *
 * Defines several compiler specific defines, such that the source code can be
 * compiled with different compilers.
 */

#ifndef _OSDEV_COMPILER_H
#define _OSDEV_COMPILER_H

#ifdef __GNUC__ 
	/** Compiler specific to force inlining */
    #define FORCE_INLINE __attribute__((always_inline)) 
    /** Compiler specific for inline assembly */
    #define INLINE_ASSEMBLY(...) asm volatile(__VA_ARGS__);
    /** Compiler specific for aligning data */
    // TODO: Replace ALIGN(...) with alignas from c++11
    #define ALIGN(...) __attribute__((aligned(__VA_ARGS__)))
    /** Compiler specific for non-returning functions */
    #define NORETURN __attribute__((noreturn))
    /** Compiler specific for rarely called functions */
    #define RARECALL __attribue__((cold))
    /** Compiler specific for packing structs */
    #define PACKED __attribute__((packed))
    /** Compiler specific for branch prediction */
    #define LIKELY(x) __builtin_expect((x),1)
    #define UNLIKELY(x) __builtin_expect((x),1)
    /** Compiler specific for asm linkage */
    #define ASM_LINK extern "C"
    /** Compiler specific for linker section */
    #define SECTION(S) __attribute__((__section__(#S)))
#else //__GNUC__ 
	/** Compiler specific to force inlining */
	#define FORCE_INLINE __forceinline 
    /** Compiler specific for inline assembly */
	#define INLINE_ASSEMBLY(...) 
    /** Compiler specific for aligning data */
    #define ALIGN(...)
    /** Compiler specific for non-returning functions */
    #define NORETURN
    /** Compiler specific for rarely called functions */
    #define RARECALL
    /** Compiler specific for packing structs */
    #define PACKED 
    /** Compiler specific for branch prediction */
    #define LIKELY(x) 
    #define UNLIKELY(x)
    /** Compiler specific for asm linkage */
    #define ASM_LINK
    /** Compiler specific for linker section hints */
    #define SECTION(S)
#endif //__GNUC__

#endif // _OSDEV_COMPILER_H

