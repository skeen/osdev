template<int N>
StaticBitset<N>::StaticBitset(bool default_value)
{
    Util::memset(frames, default_value, N);
}

template<int N>
void StaticBitset<N>::set(u32int index)
{
    if(index >= N)
    {
        PANIC("INDEX OUT OF BOUNDS");
    }

    frames[index] = true;
}

template<int N>
void StaticBitset<N>::clear(u32int index)
{
    if(index >= N)
    {
        PANIC("INDEX OUT OF BOUNDS");
    }

    frames[index] = false;
}

template<int N>
bool StaticBitset<N>::test(u32int index)
{
    if(index >= N)
    {
        PANIC("INDEX OUT OF BOUNDS");
    }

    return frames[index];
}

template<int N>
u32int StaticBitset<N>::getNumFrames()
{
    return N;
}

template<int N>
u32int StaticBitset<N>::firstClear()
{
    for(u32int x=0; x<N; x++)
    {
        if(test(x) == false)
        {
            return x;
        }
    }
   // THIS SHOULD NOT HAPPEN
   PANIC("NO FREE FRAME FOUND");
   return 0;
}
