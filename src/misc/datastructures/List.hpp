template<typename T>
class ListNode
{
    public:
        ListNode(T data, ListNode<T> *next)
            : data(data), next(next)
        {
        }
    public:
        T data;
        ListNode<T> *next;
};

template<typename T>
class ListIterator
{
    public:
        ListIterator(ListNode<T> *ptr)
            : pointer(ptr)
        {
        }

        bool operator!=(ListIterator<T> it)
        {
            return (pointer == it.pointer);
        }

        T operator*()
        {
            return pointer->data;
        }

        ListIterator& operator++(int)
        {
            pointer = pointer->next;
            return (*this);
        }
    public:
        ListNode<T>* getPointee()
        {
            return pointer;
        }

    private:
        ListNode<T> *pointer;
};
// TODO: Remove
#define nullptr 0

template<typename T>
class List
{
    public:
        typedef ListIterator<T> iterator;

        List()
            : head(nullptr)
        {
        }

        iterator begin()
        {
            return head;
        }

        iterator end()
        {
            return getTail();
        }

        bool empty() const
        {
            if(head == nullptr)
            {
                return true;
            }
            return false;
        }

        int size() const
        {
            int size = 0;
            for(iterator i=begin(); i != end(); i++)
            {
                size++;
            }
            return size;
        }

        void pop_front()
        {
            if(head->next != nullptr)
            {
                ListNode<T>* old_head = head;
                head = head->next;
                delete old_head;
            }
        }

        void push_front(T data)
        {
            ListNode<T>* new_head = new ListNode<T>(data, head);
            head = new_head;
        }

        void pop_back()
        {
            ListNode<T>* one_before_tail = getOneBeforeTail();
            if(one_before_tail != nullptr)
            {
                erase(one_before_tail);
            }
        }

        void push_back(T data)
        {
            ListNode<T>* old_tail = getTail();
            if(old_tail == nullptr)
            {
                head = new ListNode<T>(data, nullptr);
            }
            else
            {
                insert(old_tail, data);
            }
        }

        iterator insert(iterator node, T data)
        {
            ListNode<T>* list_node = node.getPointee();
            ListNode<T>* new_node = new ListNode<T>(data, list_node->next);
            list_node->next = new_node;
            return node;
        }

        iterator erase(iterator node)
        {
            ListNode<T>* list_node = node.getPointee();
            ListNode<T>* old_next = list_node->next;
            list_node->next = old_next->next;
            delete old_next;
        }

        void clear()
        {
            while(head != nullptr)
            {
                erase(head);
            }
        }
    private:
        ListNode<T>* getOneBeforeTail()
        {
            // Sanity check
            if(head == nullptr)
            {
                return nullptr;
            }
            ListNode<T>* current = head;
            ListNode<T>* next = head->next;
            while(next != nullptr)
            {
                current = next;
                next = current->next;
            }
            return current;
        } 

        ListNode<T>* getTail()
        {
            ListNode<T>* oneBefore = getOneBeforeTail();
            if(oneBefore == nullptr)
            {
                return nullptr;
            }
            else
            {
                return getOneBeforeTail()->next;
            }
        }

        ListNode<T>* head;
};

