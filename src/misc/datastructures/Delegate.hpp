#ifndef _OSDEV_MISC_DATASTRUCTURES_DELEGATE_HPP
#define _OSDEV_MISC_DATASTRUCTURES_DELEGATE_HPP

#include "Tuple.hpp"
#include "Sequence.hpp"
#include "TypeTraits.hpp"

template<typename T, typename... Ts>
struct member_of
{
    static const bool value = false; 
};

template<typename T, typename U, typename... Ts>
struct member_of<T, U, Ts...>
{
    static const bool value = conditional<
        is_same<T, U>::value,
        true_type,
        member_of<T, Ts...>
        >::type::value;
};

template<typename, typename>
struct tuple_insert { };

template<typename T, typename... Ts>
struct tuple_insert<T, Tuple<Ts...>>
{
    using type = Tuple<T, Ts...>;
};

template<typename, typename>
struct tuple_suffix
{
    using type = Tuple<>;
};

template<typename T, typename... Ts, typename... Us>
struct tuple_suffix<Tuple<T, Ts...>, Tuple<Us...>>
{
    using type = typename conditional<
        !member_of<T, Us...>::value,
        typename tuple_insert<
            T,
            typename tuple_suffix<
                Tuple<Ts...>,
                Tuple<Us...>
                >::type
            >::type,
        typename tuple_suffix<
            Tuple<Ts...>,
            Tuple<Us...>>
            ::type
        >::type;
};

template<typename ReturnType, typename... Parameters>
using FunctionPointer = ReturnType (*)(Parameters...);

template<typename ObjectType, typename ReturnType, typename... Parameters>
using ObjectFunctionPointer = ReturnType (ObjectType::*)(Parameters...);

template<typename ReturnType, typename ParametersTuple>
struct FunctionPointerFromTuple
{
};

template<typename ReturnType, typename... Parameters>
struct FunctionPointerFromTuple<ReturnType, Tuple<Parameters...>>
{
    using FunctionPtr = FunctionPointer<ReturnType, Parameters...>;
};

template<typename ObjectType, typename ReturnType, typename ParametersTuple>
struct ObjectFunctionPointerFromTuple
{
};

template<typename ObjectType, typename ReturnType, typename... Parameters>
struct ObjectFunctionPointerFromTuple<ObjectType, ReturnType, Tuple<Parameters...>>
{
    using FunctionPtr = ObjectFunctionPointer<ObjectType, ReturnType, Parameters...>;
};

template<typename ReturnType, typename... RunTimeParams> 
class Runnable
{
    public:
        virtual ReturnType invoke(RunTimeParams...) = 0;
        virtual ~Runnable() 
        {
        }
};

template<typename ReturnType, typename ConstructionTimeParamsTuple, typename CombinedParamsTuple, typename RunTimeParamsTuple> 
class FunctionDelegate
{
};

template<typename ReturnType, typename ConstructionTimeParamsTuple, typename CombinedParamsTuple, typename... RunTimeParams> 
class FunctionDelegate<ReturnType, ConstructionTimeParamsTuple, CombinedParamsTuple, Tuple<RunTimeParams...>>
    : public Runnable<ReturnType, RunTimeParams...>
{
    public:
        using Function = typename FunctionPointerFromTuple<ReturnType, CombinedParamsTuple>::FunctionPtr;
    
        FunctionDelegate(Function function, ConstructionTimeParamsTuple ct_args_tuple)
            : function(function), ct_args_tuple(ct_args_tuple)
        {
        }
        
        ReturnType invoke(RunTimeParams... rt_args) 
        {
            auto rt_args_tuple = makeTuple(rt_args...);
            auto combined_tuple = TupleConcat(ct_args_tuple, rt_args_tuple);
            return invoke(combined_tuple, typename GenerateSequence<TupleSize<CombinedParamsTuple>::value>::type());
        }
        
    private:
        template<int... S>
        ReturnType invoke(CombinedParamsTuple args, Sequence<S...>)
        {
            return function(get<S>(args)...);
        }
    
        Function function;
        ConstructionTimeParamsTuple ct_args_tuple;
};

template<typename ObjectType, typename ReturnType, typename ConstructionTimeParamsTuple, typename CombinedParamsTuple, typename RunTimeParamsTuple> 
class ObjectDelegate
{
};

template<typename ObjectType, typename ReturnType, typename ConstructionTimeParamsTuple, typename CombinedParamsTuple, typename... RunTimeParams> 
class ObjectDelegate<ObjectType, ReturnType, ConstructionTimeParamsTuple, CombinedParamsTuple, Tuple<RunTimeParams...>>
    : public Runnable<ReturnType, RunTimeParams...>
{
    public:
        using Function = typename ObjectFunctionPointerFromTuple<ObjectType, ReturnType, CombinedParamsTuple>::FunctionPtr;
    
        ObjectDelegate(ObjectType &object, Function function, ConstructionTimeParamsTuple ct_args_tuple)
            : object(object), function(function), ct_args_tuple(ct_args_tuple)
        {
        }
        
        ReturnType invoke(RunTimeParams... rt_args) 
        {
            auto rt_args_tuple = makeTuple(rt_args...);
            auto combined_tuple = TupleConcat(ct_args_tuple, rt_args_tuple);
            return invoke(combined_tuple, typename GenerateSequence<TupleSize<CombinedParamsTuple>::value>::type());
        }
        
    private:
        template<int... S>
        ReturnType invoke(CombinedParamsTuple args, Sequence<S...>)
        {
            return (object.*function)(get<S>(args)...);
        }
    
        ObjectType &object;
        Function function;
        ConstructionTimeParamsTuple ct_args_tuple;
};

/** PackDiff: 
 *  Template Metafunction to deduce the difference of two parameter packs.
 *  To distinguish two packs we have to brace them in two tuples
 */
template <class Tup1, class Tup2> struct PackDiff;

/** Basic algorithm: (T, X1...) - (T, X2...) = (X1...) - (X2...) */
template <class T, class... Pack1, class... Pack2>
struct PackDiff<Tuple<T, Pack1...>, Tuple<T, Pack2...>>
  : PackDiff<Tuple<Pack1...>, Tuple<Pack2...>> {};

/** End of recursion: (X1...) - () = (X1...) */
template <class... Pack1>
struct PackDiff<Tuple<Pack1...>, Tuple<>>
{
  typedef Tuple<Pack1...> type;
};

/** Mismatch: (T, X1...) - (U, X2...) is undefined */
template <class T1, class... Pack1, class T2, class... Pack2>
struct PackDiff<Tuple<T1, Pack1...>, Tuple<T2, Pack2...>>
{ typedef struct PACK_MISMATCH {} type; };

/** Rest: () - (X2...) is undefined */  
template <class... Pack2>
struct PackDiff<Tuple<>, Tuple<Pack2...>>
{ typedef struct LEFT_PACK_TOO_SHORT{} type; };


template<typename T, typename V>
struct create_runnable;

template<typename T, typename... Pack>
struct create_runnable<T, Tuple<Pack...>>
{
    using type = Runnable<T, Pack...>;
};

template<typename ReturnType, typename... CombinedParams, typename... ConstructionTimeParams> 
typename create_runnable<ReturnType, typename PackDiff<Tuple<CombinedParams...>, Tuple<ConstructionTimeParams...>>::type>::type*
    makeRunnable(FunctionPointer<ReturnType,CombinedParams...> function, ConstructionTimeParams... ct_args)
{
    return new FunctionDelegate<ReturnType,
                                Tuple<ConstructionTimeParams...>,
                                Tuple<CombinedParams...>,
                                typename PackDiff<Tuple<CombinedParams...>, Tuple<ConstructionTimeParams...>>::type>
                                (function, makeTuple(ct_args...));
}

template<typename ReturnType, typename... CombinedParams, typename ObjectType, typename... ConstructionTimeParams> 
typename create_runnable<ReturnType, typename PackDiff<Tuple<CombinedParams...>, Tuple<ConstructionTimeParams...>>::type>::type*
    makeRunnable(ObjectType& object, ObjectFunctionPointer<ObjectType, ReturnType, CombinedParams...> function, ConstructionTimeParams... ct_args)
{
    return new ObjectDelegate<ObjectType,
                              ReturnType,
                              Tuple<ConstructionTimeParams...>,
                              Tuple<CombinedParams...>,
                              typename PackDiff<Tuple<CombinedParams...>, Tuple<ConstructionTimeParams...>>::type>
                              (object, function, makeTuple(ct_args...));
}

/*
#include <iostream>

int print_function(char arg1, int arg2, const char* arg3)
{
    std::cout << "print: " << arg1 << arg2 << arg3 << std::endl;
    return 2;
}

int main()
{
    static_assert(
        is_same<
            tuple_suffix<
                Tuple<char, int, const char*>,
                Tuple<char, int>
                >::type,
            Tuple<const char*>
            >::value,
            "Error!"
            );
            
    auto run1 = makeRunnable(print_function, 'C', -3);
    int n = run1->invoke("PO");
    
    auto run2 = makeRunnable(print_function, 'R');
    run2->invoke(n,"D2");
}
*/

#endif //_OSDEV_MISC_DATASTRUCTURES_DELEGATE_HPP
