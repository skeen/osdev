#ifndef _OSDEV_VECTOR_HPP
#define _OSDEV_VECTOR_HPP

template <class T>
class Vector
{
    public:

        typedef T* iterator;

        Vector();
        Vector(size_t size);
        Vector(size_t size, const T& initial);
        Vector(const Vector<T> & v);      
        ~Vector();

        unsigned int capacity() const;
        unsigned int size() const;
        bool empty() const;
        iterator begin();
        iterator end();
        T& front();
        T& back();
        void push_back(const T& value); 
        void pop_back();  

        iterator erase(iterator position);
        iterator insert(iterator position, const T& value);

        void reserve(size_t capacity);   
        void resize(size_t size);   

        T& at(size_t index);
        const T& at(size_t index) const;
        T& operator[](size_t index);  
        const T& operator[] (size_t n) const;
        
        Vector<T> & operator=(const Vector<T> &);
        void clear();
    private:
        size_t my_size;
        size_t my_capacity;
        T *buffer;
};

#include "Vector_impl.hpp"

#endif //_OSDEV_VECTOR_HPP
