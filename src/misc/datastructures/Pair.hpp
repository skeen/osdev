#ifndef _OSDEV_MISC_DATASTRUCTURES_PAIR_HPP
#define _OSDEV_MISC_DATASTRUCTURES_PAIR_HPP

template<typename T, typename U>
class Pair
{
    public:
        Pair()
        {
        }

        Pair(T first, U second)
            : first(first), second(second)
        {
        }

        T getFirst()
        {
            return first;
        }

        U getSecond()
        {
            return second;
        }

    private:
        T first;
        U second;
};

template<typename T, typename U>
Pair<T,U> makePair(T t, U u) 
{
    return Pair<T,U>(t,u);
}

#endif //_OSDEV_MISC_DATASTRUCTURES_PAIR_HPP
