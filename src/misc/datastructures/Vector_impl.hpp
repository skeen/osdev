#include <misc/new.hpp>
    
template<class T>
Vector<T>::Vector()
{
    my_capacity = 0;
    my_size = 0;
    buffer = 0;
}

    template<class T>
Vector<T>::Vector(const Vector<T> & v)
{
    my_size = v.my_size;
    my_capacity = v.my_capacity;
    buffer = new T[my_size];  
    for (unsigned int i = 0; i < my_size; i++)
        buffer[i] = v.buffer[i];  
}

    template<class T>
Vector<T>::Vector(size_t size)
{
    my_capacity = size;
    my_size = size;
    buffer = new T[size];
}

    template<class T>
Vector<T>::Vector(size_t size, const T & initial)
{
    my_size = size;
    my_capacity = size;
    buffer = new T [size];
    for (unsigned int i = 0; i < size; i++)
        buffer[i] = initial;
    //T();
}

    template<class T>
Vector<T> & Vector<T>::operator = (const Vector<T> & v)
{
    delete[ ] buffer;
    my_size = v.my_size;
    my_capacity = v.my_capacity;
    buffer = new T [my_size];
    for (unsigned int i = 0; i < my_size; i++)
        buffer[i] = v.buffer[i];
    return *this;
}

    template<class T>
typename Vector<T>::iterator Vector<T>::begin()
{
    return buffer;
}

    template<class T>
typename Vector<T>::iterator Vector<T>::end()
{
    return buffer + size();
}

    template<class T>
T& Vector<T>::front()
{
    return buffer[0];
}

    template<class T>
T& Vector<T>::back()
{
    return buffer[size - 1];
}

    template<class T>
void Vector<T>::push_back(const T & v)
{
    if (my_size >= my_capacity)
        reserve(my_capacity +5);
    buffer [my_size++] = v;
}

    template<class T>
void Vector<T>::pop_back()
{
    my_size--;
}

template<class T>
typename Vector<T>::iterator Vector<T>::erase(typename Vector<T>::iterator required_position)
{
    // Shift it to the back
    Vector<T>::iterator position = required_position;
    Vector<T>::iterator next_position = (position + 1);
    while(next_position != end())
    {
        // Copy it over
        *position = *next_position;
        position = next_position;
        next_position++;
    }
    pop_back();
    return required_position;
}

template<class T>
typename Vector<T>::iterator Vector<T>::insert(typename Vector<T>::iterator required_position, const T& v)
{
    push_back(v);
    // Shift it in
    Vector<T>::iterator position = end();
    Vector<T>::iterator next_position = (position - 1);
    while(position != required_position)
    {
        T tmp = *position;
        *position = *next_position;
        *next_position = tmp;

        position = next_position;
        next_position--;
    }
    return required_position;
}

    template<class T>
void Vector<T>::reserve(size_t capacity)
{
    if(buffer == 0)
    {
        my_size = 0;
        my_capacity = 0;
    }    
    T * Newbuffer = new T [capacity];
    //assert(Newbuffer);
    size_t l_Size = capacity < my_size ? capacity : my_size;
    //copy (buffer, buffer + l_Size, Newbuffer);

    for (size_t i = 0; i < l_Size; i++)
        Newbuffer[i] = buffer[i];

    my_capacity = capacity;
    delete[] buffer;
    buffer = Newbuffer;
}

template<class T>
unsigned int Vector<T>::size() const
{
    return my_size;
}

    template<class T>
void Vector<T>::resize(size_t size)
{
    reserve(size);
    my_size = size;
}

   template<class T>
T& Vector<T>::at(size_t index)
{
    return buffer[index];
}  

template<class T>
const T& Vector<T>::at(size_t index) const
{
    return buffer[index];
}  

    template<class T>
T& Vector<T>::operator[](size_t index)
{
    return at(index);
}  

template<class T>
const T& Vector<T>::operator[](size_t index) const
{
    return at(index);
}  

template<class T>
unsigned int Vector<T>::capacity() const
{
    return my_capacity;
}

    template<class T>
Vector<T>::~Vector()
{
    delete[ ] buffer;
}

    template <class T>
void Vector<T>::clear()
{
    my_capacity = 0;
    my_size = 0;
    buffer = 0;
}
