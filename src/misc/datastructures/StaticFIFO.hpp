#ifndef _OSDEV_MISC_DATASTRUCTURES_STATICFIFO_HPP
#define _OSDEV_MISC_DATASTRUCTURES_STATICFIFO_HPP

// This is a statically sized FIFO queue, implemented through the use of a
// cyclic buffer. 

#include <misc/common.hpp>

template<typename T, int N>
class StaticFIFO
{
    public:
        StaticFIFO()
        {
            Util::memset(data, 0, sizeof(T)*N);
            read_position = 0;
            write_position = 0;
        }
        
        // Peeks at the first element.
        // If the queue is empty, a kernel panic occurs.
        T peek()
        {
            if(isEmpty())
            {
                PANIC("TRYING TO READ AN EMPTY STATIC_FIFO");
            }

            T data_item = data[read_position];
            return data_item;
        }
        
        // Returns the first element.
        // If the queue is empty, a kernel panic occurs.
        T get()
        {
            T tmp = peek();
            read_position++;
            if(read_position == N)
            {
                read_position = 0;
            }
            return tmp;
        }

        // Put an element into the list.
        // If the queue is full, a kernel panic occurs.
        void put(T arg)
        {
            if(isFull())
            {
                PANIC("TRYING TO WRITE A FULL STATIC_FIFO");
            }

            data[write_position] = arg;
            write_position++;
            if(write_position == N)
            {
                write_position = 0;
            }
        }

        bool isEmpty()
        {
            if (write_position == read_position)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        bool isFull()
        {
            if ((write_position + 1) == read_position)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    private:
        // The static data array
        T data[N];
        // The indexes for read and write
        // When read == write the buffer is empty.
        // When read == write+1 the buffer is full.
        u32int read_position;
        u32int write_position;
};

#endif //_OSDEV_MISC_DATASTRUCTURES_STATICFIFO_HPP
