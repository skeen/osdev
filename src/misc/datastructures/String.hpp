#ifndef _OSDEV_STRING_HPP
#define _OSDEV_STRING_HPP

#include "Vector.hpp"

class String
{
    //---------
    //Functions
    public:
        void init()
        {
            string_length = 0;
            data_length = 10;
            data = new char[10];
        }

        String()
        {
            init();
        }

        String(const char *s)
        {
            init();
            append(s);
        }

        String(const String& str)
        {
            init();
            append(str);
        }

        String(u32int n)
        {
            init();
            append(n);
        }

        ~String()
        {
            delete data;
        }

        String& operator=(const String& str)
        {
            clear();
            append(str);
            return *this;
        }

        String& operator=(const char* s)
        {
            clear();
            append(s);
            return *this;
        }

        String& operator=(char c)
        {
            clear();
            append(c);
            return *this;
        }

        bool operator==(const String &other) const
        {
            if(length() != other.length())
            {
                return false;
            }
            else
            {
                for(u32int x=0; x<length(); x++)
                {
                    char c1 = at(x);
                    char c2 = other.at(x);
                    if(c1 != c2)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        void clear()
        {
            string_length = 0;
        }

        u32int length() const
        {
            return string_length;
        }

        String& append(const String& str)
        {
            for(u32int x=0; x<str.length(); x++)
            {
                char c = str[x];
                append(c);
            }
            return *this;
        }

        String& append(const char* s)
        {
            for(u32int x=0;; x++)
            {
                char c = s[x];
                if(c == '\0')
                {
                    break;
                }
                append(c);
            }
            return *this;
        }

        String& append(char c)
        {
            if(string_length == data_length+1)
            {
                u32int new_datalength = data_length + 10;
                char* new_data = new char[new_datalength];
                for(u32int x=0; x<string_length; x++)
                {
                    new_data[x] = data[x];
                }

                delete data;

                data = new_data;
                data_length = new_datalength;
            }
            data[string_length] = c;
            string_length++;

            return *this;
        }

        String& append(u32int n, u32int base = 10)
        {
            // Recursive integer to string
            char digit = '0' + (n % base);
            n = n / base;
            if (n > 0)
            {
                append(n);
            }
            append(digit);
            // Require return
            return *this;
        }

        String& operator+= ( const String& str )
        {
            return append(str);
        }

        String& operator+= ( const char* s )
        {
            return append(s);
        }

        String& operator+= ( char c )
        {
            return append(c);
        }

        String& operator+= ( u32int n )
        {
            return append(n);
        }

        const String operator+(const String other) const
        {
            String tmp = String();
            tmp.append(*this);
            tmp.append(other);
            return tmp;
        }

        char& at(size_t pos)
        {
            if(pos > string_length)
            {
                PANIC("INDEX OUT OF RANGE");
            }
            else
            {
                return data[pos];
            }
        }

        const char& at(size_t pos) const
        {
            if(pos > string_length)
            {
                PANIC("INDEX OUT OF RANGE");
            }
            else
            {
                return data[pos];
            }
        }

        char& operator[] ( size_t pos )
        {
            return at(pos);
        }

        const char& operator[] ( size_t pos ) const
        {
            return at(pos);
        }

        const char* toCString()
        {
            char* returnData = new char[string_length+1];
            for(u32int x=0; x<string_length; x++)
            {
                returnData[x] = data[x];
            }
            returnData[string_length] = '\0';
            return returnData;
        }

        int findFirst(char sep)
        {
            for(u32int x=0; x<length(); x++)
            {
                char c = data[x];
                if(c == sep)
                {
                    return x;
                }
            }
            return -1;
        }

        String substring(u32int start, u32int end)
        {
            if ((start > end) || (end > length()))
            {
                PANIC("SUBSTRING OUT OF BOUNDS");
            }

            u32int length = end - start;
            String s = String();
            for(u32int x=start; x<end; x++)
            {
                s.append(data[x]);
            }
            return s;
        }

        bool isDigit(u32int i)
        {
            char c = at(i);
            switch(c)
            {
                case '0': case '1': case '2':
                case '3': case '4': case '5':
                case '6': case '7': case '8':
                case '9':
                    return true;
                    break;
                default:
                    return false;
                    break;
            }
        }

        u32int parseInteger()
        {
            u32int value = 0;

            for(u32int x=0; x<length(); x++)
            {
                if(isDigit(x) == false)
                {
                    return value;
                }
                value *= 10;
                value += (u32int) (at(x) - '0');
            }
            return value;
        }
    private:
    protected:
    //---------
    //Variables
    public:
    private:
        char* data;
        u32int data_length;
        u32int string_length;
    protected:
};

#endif //_OSDEV_STRING_HPP
