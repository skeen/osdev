#ifndef _OSDEV_MISC_DATASTRUCTURES_TYPE_TRAITS_HPP
#define _OSDEV_MISC_DATASTRUCTURES_TYPE_TRAITS_HPP

template <class T, T v>
struct integral_constant 
{
  static constexpr T value = v;
  typedef T value_type;
  typedef integral_constant<T,v> type;
  constexpr operator T() { return v; }
};
 
using true_type = integral_constant<bool, true>;
using false_type = integral_constant<bool, false>;
 
template<bool B, class T, class F>
struct conditional
{
    typedef T type; 
};
 
template<class T, class F>
struct conditional<false, T, F>
{
    typedef F type; 
};
 
template<class T, class U>
struct is_same : false_type {};
 
template<class T>
struct is_same<T, T> : true_type {};

#endif //_OSDEV_MISC_DATASTRUCTURES_TYPE_TRAITS_HPP
