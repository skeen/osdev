#ifndef _OSDEV_MISC_DATASTRUCTURES_SEQUENCE_HPP
#define _OSDEV_MISC_DATASTRUCTURES_SEQUENCE_HPP

template<int...>
struct Sequence
{
};

template<int N, int... S>
struct GenerateSequence : GenerateSequence<N-1, N-1, S...>
{
};

template<int... S>
struct GenerateSequence<0, S...> 
{
  typedef Sequence<S...> type;
};

#endif //_OSDEV_MISC_DATASTRUCTURES_SEQUENCE_HPP
