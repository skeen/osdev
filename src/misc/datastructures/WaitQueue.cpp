#include <string>
#include <iostream>

#include "DoubleList.hpp"
#include "Pair.hpp"

template<typename T>
class TimedWait
{
    public:
        void append(int p, T item)
        {
            for(auto it=waiting.begin();
                    it != waiting.end();
                    it++)
            {
                std::cout << "MAGIC" << std::endl;
                auto pair = (*it);
                int compare = pair.getFirst();
                T old_item = pair.getSecond();
                std::cout << "UNICORN" << std::endl;
                if(compare > p)
                {
                    std::cout << "insert" << std::endl;
                    waiting.insert(it, makePair(p,item));
                    std::cout << "erase" << std::endl;
                    it++;
                    std::cout << "incremented" << std::endl;
                    waiting.erase(it);
                    std::cout << "insert" << std::endl;
                    waiting.insert(it, makePair(compare-p, old_item));
                    return;
                }
                else
                {
                    p = p - compare;
                }
            }
            std::cout << "PUSHBACK" << std::endl;
            waiting.push_back(makePair(p,item));
            std::cout << "NOW" << std::endl;
        }

        void print()
        {
            for(auto it=waiting.begin();
                    it != waiting.end();
                    it++)
            {
                auto pair = (*it);
                std::cout << pair.getFirst() << " : " << pair.getSecond() << std::endl;
            }
        }
    private:
        DoubleList<Pair<int, T>> waiting; 
};

int main()
{
    TimedWait<const char*> t;
    t.append(7, "BALLZ");
    t.append(4, "BECAUSE");
    t.append(3, "PASS!");
    t.append(7, "SUCK");
    t.append(1, "YOU");
    t.append(5, "YOU");
    t.append(2, "CANNOT");
    t.print();
    
    DoubleList<int> list;
    list.push_back(1);
    list.push_back(2);
    list.push_back(3);
    list.push_back(4);
    list.push_back(5);
    
    list.erase(list.begin());
    list.erase(list.begin());

    list.insert(list.begin(), 5);
    list.insert(list.end(), 1);

    for(auto it=list.begin();
            it != list.end();
            it++)
    {
        std::cout << *it << std::endl;
    }
}

