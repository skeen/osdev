#ifndef _OSDEV_BITSET_H
#define _OSDEV_BITSET_H

#include <misc/common.hpp>

class Bitset
{
    public:
        // Set bit at index
        virtual void set(u32int index) = 0;
        // Clear bit at index
        virtual void clear(u32int index) = 0;
        // Test bit at index
        virtual bool test(u32int index) = 0;

        // Find index of first clear bit
        virtual u32int firstClear() = 0;
        // Return the number of frames
        virtual u32int getNumFrames() = 0;
};

/**
 * This class is NOT thread-safe
 */
template<int N>
class StaticBitset : public Bitset
{
    public:
        StaticBitset(bool default_value);

        // Set bit at index
        void set(u32int index);
        // Clear bit at index
        void clear(u32int index);
        // Test bit at index
        bool test(u32int index);

        // Find index of first clear bit
        u32int firstClear();
        // Return the number of frames
        u32int getNumFrames();

    private:
        bool frames[N];
};

#include "bitset_impl.hpp"

#endif //_OSDEV_BITSET_H
