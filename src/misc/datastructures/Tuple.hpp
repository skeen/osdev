#ifndef _OSDEV_MISC_DATASTRUCTURES_TUPLE_HPP
#define _OSDEV_MISC_DATASTRUCTURES_TUPLE_HPP

#include "Sequence.hpp"

template<typename... Values>
class Tuple;

template<>
class Tuple<>
{
};

template<typename Head, typename... Tail>
class Tuple<Head, Tail...> : private Tuple<Tail...> 
{
    protected:
        typedef Tuple<Tail...> cdr;
        Head car;

    public:
        Tuple() 
        {
        }

        Tuple(Head car_init, Tail... cdr_init)
            : cdr(cdr_init...), car(car_init)
        { 
        }

        Head getCar()
        {
            return car;
        }

        cdr& getCdr()
        {
            return *this;
        } 
};

template<typename Tuple>
struct TupleSize;

template<typename... Values>
struct TupleSize<Tuple<Values...>> 
{
    static const int value = sizeof...(Values);
};

template<typename T>
struct TupleResult 
{
    typedef T type;
};

template<typename... Values>
Tuple<typename TupleResult<Values>::type...> makeTuple(Values... values) 
{
    return Tuple<typename TupleResult<Values>::type...>(values...);
}

template<int Index, typename Tuple>
struct TupleElement;

template<int Index, typename Head, typename... Tail>
struct TupleElement<Index, Tuple<Head, Tail...>>
{
    typedef typename TupleElement<Index-1, Tuple<Tail...> >::type type;
};

template<typename Head, typename... Tail>
struct TupleElement<0, Tuple<Head, Tail...>> 
{
    typedef Head type;
};

template<int Index, typename Tuple>
class getImplementation;

template<int Index, typename Head, typename... Values>
class getImplementation<Index, Tuple<Head, Values...>> 
{
    typedef typename TupleElement<Index-1, Tuple<Values...>>::type Element;
    typedef getImplementation<Index-1, Tuple<Values...>> Next;

    public:
    static Element get(Tuple<Head, Values...> t)
    {
        return Next::get(t.getCdr()); 
    }
};

template<typename Head, typename... Values>
class getImplementation<0, Tuple<Head, Values...> >
{
    public:
        static Head get(Tuple<Head, Values...> t) 
        {
            return t.getCar(); 
        }
};

template<int Index, typename... Values>
typename TupleElement<Index, Tuple<Values...> >::type get(Tuple<Values...>& t) 
{
    return getImplementation<Index, Tuple<Values...>>::get(t);
}

template <typename ...Ts>
Tuple<> TupleConcat()
{
    return Tuple<>();
}
 
template <typename ...Ts>
Tuple<Ts...> TupleConcat(Tuple<Ts...> t)
{
    return t; 
}

template<typename Tuple1, int... T1s, typename Tuple2, int... T2s>
auto TupleConcat_helper(Tuple1 t1, Tuple2 t2, Sequence<T1s...>, Sequence<T2s...>) ->
  decltype(makeTuple(get<T1s>(t1)..., get<T2s>(t2)...)) 
{
    return makeTuple(get<T1s>(t1)..., get<T2s>(t2)...);
}

template<typename... Tuple1, typename... Tuple2, typename... Tuples>
auto
TupleConcat(Tuple<Tuple1...> t1, Tuple<Tuple2...> t2, Tuples ...ts) ->
  decltype(TupleConcat(
             TupleConcat_helper(
               t1,t2,
               typename GenerateSequence<sizeof...(Tuple1)>::type(),
               typename GenerateSequence<sizeof...(Tuple2)>::type()
             ),
             ts...
           )
          )
{
  return TupleConcat(
             TupleConcat_helper(
               t1,t2,
               typename GenerateSequence<sizeof...(Tuple1)>::type(),
               typename GenerateSequence<sizeof...(Tuple2)>::type()
             ),
             ts...
           );
}

#endif //_OSDEV_MISC_DATASTRUCTURES_TUPLE_HPP
