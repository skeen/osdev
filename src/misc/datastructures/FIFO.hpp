#ifndef _OSDEV_FIFO_HPP
#define _OSDEV_FIFO_HPP

#include <misc/new.hpp>

template<typename T>
class FIFO_Node
{
    public:
        FIFO_Node(T data, FIFO_Node<T>* next)
        : data(data), next(next)
        {
        }

        FIFO_Node<T>* getNext()
        {
            return next;
        }

        T getData()
        {
            return data;
        }

        void setNext(FIFO_Node<T>* new_next)
        {
            next = new_next;
        }
    private:
        T data;
        FIFO_Node<T>* next;
};

template<typename T>
class FIFO
{
    public:
        FIFO()
        {
            head = nullptr;
        }

        bool empty()
        {
            return (head == nullptr);
        }

        void add(T data)
        {
            FIFO_Node<T>* new_node = new FIFO_Node<T>(data, nullptr);

            FIFO_Node<T>* tail = getTail();
            insertAfter(tail,data);
        }

        T pop()
        {
            T data = head->getData();

            FIFO_Node<T>* old_head = head;
            
            head = old_head->getNext();

            delete old_head;
            return data;
        }
    private:
        void insertAfter(FIFO_Node<T>* node, T data)
        {
            if(node->getNext() == nullptr)
            {
                FIFO_Node<T>* new_node = new FIFO_Node<T>(data, nullptr);
                node->setNext(new_node);
            }
            else // Not null
            {
                FIFO_Node<T>* next = node->getNext();
                FIFO_Node<T>* new_node = new FIFO_Node<T>(data, next);
                node->setNext(new_node);
            }
        }
        void deleteAfter(FIFO_Node<T>* node)
        {
            if(node->getNext() == nullptr)
            {
                // TODO: Exception?
                return;
            }
            else
            {
                FIFO_Node<T>* temp = node->getNext();
                FIFO_Node<T>* tempNext = temp->getNext();
                // Set next
                node->setNext(temp->getNext());
                // Release tempNext
                delete tempNext;
            }
        }


        FIFO_Node<T>* getTail()
        {
            if(empty())
            {
                return nullptr;
            }
            else
            {
                FIFO_Node<T>* tail = head;
                // Run till tail
                while(tail->getNext() != nullptr)
                {
                    tail = tail->getNext();
                }
                return tail;
            }
        }

    private:
        FIFO_Node<T>* head;
};

#endif //_OSDEV_FIFO_HPP
