// TODO: Remove
#define nullptr 0

template<typename T>
class DoubleListNode
{
    public:
        DoubleListNode(T data, DoubleListNode<T> *prev, DoubleListNode<T> *next)
            : data(data), prev(prev), next(next)
        {
        }
    public:
        T data;
        DoubleListNode<T> *prev;
        DoubleListNode<T> *next;
};

template<typename T>
class DoubleListIterator
{
    public:
        DoubleListIterator(DoubleListNode<T> *ptr)
            : pointer(ptr)
        {
        }

        DoubleListIterator(const DoubleListIterator<T> &mit)
            : pointer(mit.pointer)
        {
        }

        bool operator!=(const DoubleListIterator<T>& rhs)
        {
            return (pointer != rhs.pointer);
        }

        bool operator==(const DoubleListIterator<T>& rhs)
        {
            return (pointer == rhs.pointer);
        }

        T& operator*()
        {
            return pointer->data;
        }

        DoubleListIterator<T>& operator++()
        {
            pointer = pointer->next;
            return *this;
        }

        DoubleListIterator<T> operator++(int)
        {
            DoubleListIterator tmp(*this);
            operator++();
            return tmp;
        }
    public:
        DoubleListNode<T>* getPointee()
        {
            return pointer;
        }

    private:
        DoubleListNode<T> *pointer;
};

template<typename T>
class DoubleList
{
    public:
        typedef DoubleListIterator<T> iterator;

        DoubleList()
            : head(nullptr), tail(nullptr)
        {
        }

        iterator begin()
        {
            return head;
        }

        iterator end()
        {
            return nullptr;
        }
/*
        bool empty() const
        {
            if(head == nullptr)
            {
                return true;
            }
            return false;
        }

        int size() const
        {
            int size = 0;
            for(iterator i=begin(); i != end(); i++)
            {
                size++;
            }
            return size;
        }
        */
    /*
        void pop_front()
        {
            if(head->next != nullptr)
            {
                ListNode<T>* old_head = head;
                head = head->next;
                delete old_head;
            }
        }
*/
        /*
        void push_front(T data)
        {
            DoubleListNode<T>* new_head = new DoubleListNode<T>(data, nullptr, head);
            head->prev = new_head;
            head = new_head;
        }
        */
/*
        void pop_back()
        {
            DoubleListNode<T>* one_before_tail = tail->prev;
            if(one_before_tail != nullptr)
            {
                erase(one_before_tail);
            }
        }
*/
        void push_back(T data)
        {
            if(head == nullptr && tail == nullptr)
            {
                head = tail = new DoubleListNode<T>(data, nullptr, nullptr);
            }
            else
            {
                DoubleListNode<T>* new_tail = new DoubleListNode<T>(data, tail, nullptr);
                tail->next = new_tail;
                tail = new_tail;
            }
        }

        iterator insert(iterator node, T data)
        {
            DoubleListNode<T>* list_node = node.getPointee();
            if(list_node == nullptr)
            {
                push_back(data);
                return node;
            }
            DoubleListNode<T>* prev_node = list_node->prev;
            // Only one node in the list
            if(isOnly(list_node) || isHead(list_node))
            {
                head = new DoubleListNode<T>(data, nullptr, list_node);
                list_node->prev = head;
            }
            // Tail of the list
            else if(isTail(list_node))
            {
                tail = new DoubleListNode<T>(data, list_node, nullptr);
                list_node->next = tail;
            }
            // Middle of the list
            else
            {
                DoubleListNode<T>* new_node = new DoubleListNode<T>(data, prev_node, list_node);
                prev_node->next = new_node;
                list_node->prev = new_node;
            }

            return node;
        }
        
        iterator erase(iterator node)
        {
            DoubleListNode<T>* list_node = node.getPointee();
            if(list_node == nullptr)
            {
                return node;
            }
            DoubleListNode<T>* next_node = list_node->next;
            DoubleListNode<T>* prev_node = list_node->prev;

            // Only one node in the list
            if(isOnly(list_node))
            {
                head = tail = nullptr;
            }
            // Tail of the list
            else if(isTail(list_node))
            {
                tail = prev_node;
                tail->next = nullptr;
            }
            // Head of the list
            else if(isHead(list_node))
            {
                head = next_node;
                head->prev = nullptr;
            }
            // Middle of the list
            else
            {
                prev_node->next = next_node;
                next_node->prev = prev_node;
            }
            // Remove the node
            delete list_node;
            // return iter
            return next_node;
        }

        void clear()
        {
            while(head != nullptr)
            {
                erase(head);
            }
        }
    private:
        bool isTail(DoubleListNode<T>* node)
        {
            return (node->next == nullptr);
        }

        bool isHead(DoubleListNode<T>* node)
        {
            return (node->prev == nullptr);
        }

        bool isOnly(DoubleListNode<T>* node)
        {
            return (isTail(node) && isHead(node));
        }

        DoubleListNode<T>* head;
        DoubleListNode<T>* tail;
};

