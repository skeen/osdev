/**
 * @file	hardware.cpp
 * @author  Emil Madsen <skeen@cs.au.dk>
 * @version 1.0
 *
 * @section LICENSE
 *
 * Public Domain.
 *
 * @section DESCRIPTION
 *
 * Implements the hardware class described in common.hpp
 */

#include "common.hpp"
#include "compiler.h"


// Doxygen in common.hpp
void Hardware::outb(u16int port, u8int value)
{
    INLINE_ASSEMBLY("outb %1, %0" : : "dN" (port), "a" (value));
}

// Doxygen in common.hpp
void Hardware::outw(u16int port, u16int value)
{
    INLINE_ASSEMBLY("outw %1, %0" : : "dN" (port), "a" (value));
}

// Doxygen in common.hpp
u8int Hardware::inb(u16int port)
{
    u8int ret;
    INLINE_ASSEMBLY("inb %1, %0" : "=a" (ret) : "dN" (port));
    return ret;
}

// Doxygen in common.hpp
u16int Hardware::inw(u16int port)
{
    u16int ret;
    INLINE_ASSEMBLY("inw %1, %0" : "=a" (ret) : "dN" (port));
    return ret;
}

// Doxygen in common.hpp
void Hardware::DISABLE_CPU_INTERRUPTS()
{
	INLINE_ASSEMBLY("cli");
}

// Doxygen in common.hpp
void Hardware::ENABLE_CPU_INTERRUPTS()
{
	INLINE_ASSEMBLY("sti");
}

bool Hardware::TEST_CPU_INTERRUPTS()
{
    u32int val;
    asm volatile(
            "pushfl; \
            popl %%eax; \
            andl $0x000200, %%eax; \
            mov %%eax, %0"
            : "=r"(val));
    return (bool) val;
}

// Doxygen in common.hpp
void Hardware::HALT_CPU()
{
	INLINE_ASSEMBLY("hlt");
}

// Doxygen in common.hpp
void Hardware::LOCK_CPU()
{
	while(true)
	{
		HALT_CPU();
	}
}

// Doxygen in common.hpp
void Hardware::KILL_LOCK_CPU()
{
	DISABLE_CPU_INTERRUPTS();
	LOCK_CPU();
}
