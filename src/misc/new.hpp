#ifndef _OSDEV_CLIB_NEW_HPP
#define _OSDEV_CLIB_NEW_HPP

#include "compiler.h"

typedef long unsigned int size_t;

void seed_malloc(void* (*malloc_func)(size_t));
void seed_free(void (*free_func)(void*));

/**
 * Implementation of the new and delete operators
 */
void *operator new(size_t size);
void *operator new(size_t size, void *p);
void *operator new[](size_t size);
void *operator new[](size_t size, void *p);
void operator delete(void *p);
void operator delete[](void *p);

#endif //_OSDEV_CLIB_NEW_HPP
