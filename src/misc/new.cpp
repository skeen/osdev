#include "new.hpp"

void* (*new_malloc)(size_t) = 0;
void (*new_free)(void*) = 0;

void seed_malloc(void* (*function)(size_t))
{
    new_malloc = function;
}

void seed_free(void (*function)(void*))
{
    new_free = function;
}

void *operator new(size_t size)
{
    return new_malloc(size);
}

void *operator new(size_t size, void *p)
{
    return p;
}
 
void *operator new[](size_t size)
{
    return new_malloc(size);
}

void *operator new[](size_t size, void *p)
{
    return p;
}
 
void operator delete(void *p)
{
    new_free(p);
}
 
void operator delete[](void *p)
{
    new_free(p);
}
