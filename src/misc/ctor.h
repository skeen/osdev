/**
 * @file	ctor.h
 * @author  Emil Madsen <skeen@cs.au.dk>
 * @version 1.0
 *
 * @section LICENSE
 *
 * Public Domain.
 *
 * @section DESCRIPTION
 *
 * The interface for executing constructors/destructors.
 */

#ifndef _OSDEV_CTOR_H
#define _OSDEV_CTOR_H

/** Executes all the constructors found in the ctor section */
void executeConstructors(void);
/** Executes all the destructors found in the dtor section */
void executeDestructors(void);

#endif //_OSDEV_CTOR_H
