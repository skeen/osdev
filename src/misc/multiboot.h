/* multiboot.h - the header for Multiboot */
/* Copyright (C) 1999, 2001  Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. */

#ifndef _OSDEV_MULTIBOOT_H
#define _OSDEV_MULTIBOOT_H

#include "types.h"
#include "compiler.h"

/* Macros. */
/* The magic number for the Multiboot header. */
#define MULTIBOOT_HEADER_MAGIC          0x1BADB002

/* The flags for the Multiboot header. */
#ifdef __ELF__
    # define MULTIBOOT_HEADER_FLAGS         0x00000003
#else
    # define MULTIBOOT_HEADER_FLAGS         0x00010003
#endif

// This defines the value of the type when usable
#define MULTIBOOT_HEADER_MEMORY_MAP_TYPE_USABLE 1

/* The magic number passed by a Multiboot-compliant boot loader. */
#define MULTIBOOT_BOOTLOADER_MAGIC      0x2BADB002

/* The Multiboot header. */
struct multiboot_header
{
    u32int magic;
    u32int flags;
    u32int checksum;
    u32int header_addr;
    u32int load_addr;
    u32int load_end_addr;
    u32int bss_end_addr;
    u32int entry_addr;
} PACKED;

typedef multiboot_header multiboot_header_t;

static_assert(sizeof(multiboot_header_t)==32,  "Incorrect size of type");

/* The symbol table for a.out. */
struct aout_symbol_table
{
    u32int tabsize;
    u32int strsize;
    u32int addr;
    u32int reserved;
} PACKED;

typedef aout_symbol_table aout_symbol_table_t;

static_assert(sizeof(aout_symbol_table_t)==16,  "Incorrect size of type");

/* The section header table for ELF. */
struct elf_section_header_table
{
    u32int num;
    u32int size;
    u32int addr;
    u32int shndx;
} PACKED;

typedef elf_section_header_table elf_section_header_table_t;

static_assert(sizeof(elf_section_header_table_t)==16,  "Incorrect size of type");

/* The Multiboot information. */
struct multiboot_info
{
    u32int flags;
    u32int mem_lower;
    u32int mem_upper;
    u32int boot_device;
    u32int cmdline;
    u32int mods_count;
    u32int mods_addr;
    union
    {
        aout_symbol_table_t aout_sym;
        elf_section_header_table_t elf_sec;
    } u;
    u32int mmap_length;
    u32int mmap_addr;
} PACKED;

typedef multiboot_info multiboot_info_t;

static_assert(sizeof(multiboot_info_t)==9*4+16,  "Incorrect size of type");

/* The module structure. */
struct module
{
    u32int mod_start;
    u32int mod_end;
    u32int string;
    u32int reserved;
} PACKED;

typedef module module_t;

static_assert(sizeof(module_t)==16,  "Incorrect size of type");

/* The memory map. Be careful that the offset 0 is base_addr_low
   but no size. */
struct memory_map
{
    u32int size;
    u64int base_address;
    u64int length;
    u32int type;
} PACKED;

typedef memory_map memory_map_t;

static_assert(sizeof(memory_map_t)==2*4+2*8,  "Incorrect size of type");

#endif // _OSDEV_MULTIBOOT_H

