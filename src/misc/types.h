/**
 * @file	types.h
 * @author  Emil Madsen <skeen@cs.au.dk>
 * @version 1.0
 *
 * @section LICENSE
 *
 * Public Domain.
 *
 * @section DESCRIPTION
 *
 * Several typedefs to standardise variable sizes a cross different platforms.
 * This version is written for x86-32.
 */

#ifndef _OSDEV_TYPES_H
#define _OSDEV_TYPES_H

// 64bit unsigned and signed
typedef unsigned long long u64int;
typedef          long long s64int;
// 32bit unsigned and signed
typedef unsigned int   	u32int;
typedef          int   	s32int;
// 16bit unsigned and signed
typedef unsigned short 	u16int;
typedef          short 	s16int;
// 8bit unsigned and signed
typedef unsigned char  	u8int;
typedef          char  	s8int;
// size_t typedef
typedef long unsigned int size_t;

static_assert(sizeof(u8int)==1,  "Incorrect size of type");
static_assert(sizeof(s8int)==1,  "Incorrect size of type");

static_assert(sizeof(u16int)==2, "Incorrect size of type");
static_assert(sizeof(s16int)==2, "Incorrect size of type");

static_assert(sizeof(u32int)==4, "Incorrect size of type");
static_assert(sizeof(s32int)==4, "Incorrect size of type");

static_assert(sizeof(u64int)==8, "Incorrect size of type");
static_assert(sizeof(s64int)==8, "Incorrect size of type");


#endif // _OSDEV_TYPES_H

