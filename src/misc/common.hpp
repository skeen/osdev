/**
 * @file	common.hpp
 * @author  Emil Madsen <skeen@cs.au.dk>
 * @version 1.0
 *
 * @section LICENSE
 *
 * Public Domain.
 *
 * @section DESCRIPTION
 *
 * Defines several generally useful function, mainly used to interact with
 * hardware.
 */

#ifndef _OSDEV_COMMON_HPP
#define _OSDEV_COMMON_HPP

#include "types.h"
#include "compiler.h"

/**
 * Writes an assertion failed message, and kill locks the cpu.
 *
 * @param[in]  B		MACRO_VAR 	The bool to be checked. (false = error)
 * @param[in]  MSG      MACRO_VAR   The error message to be printed
 * @return 				void		Never returns.
 */
#define ASSERT(b,msg) ((b) ? (void)0 : Util::PANIC_ASSERT_IMPL(#msg, __FILE__, __LINE__))
#define PANIC(msg) Util::PANIC_IMPL(#msg, __FILE__, __LINE__)

class Hardware
{
    //----------
    // Functions
    public:
        /**
         * Write a byte out to the specified I/O port.
         *
         * @param[in]  Port		u16int	Describes the targeted I/O port.
         * @param[in]  Value	u8int	The value the selected port will be set to.  
         * @return 				void	Returns void.
         */
        static void outb(u16int port, u8int value);
        /**
         * Write a word out to the specified I/O port.
         *
         * @param[in]  Port		u16int	Describes the targeted I/O port.
         * @param[in]  Value	u8int	The value the selected port will be set to.  
         * @return 				void	Returns void.
         */
        static void outw(u16int port, u16int value);
        /**
         * Read a byte from the specified I/O port.
         *
         * @param[in]  Port		u16int	Describes the targeted I/O port.
         * @return 				u8int	Returns the byte value of the selected port.
         */
        static u8int inb(u16int port);
        /**
         * Read a word from the specified I/O port.
         *
         * @param[in]  Port		u16int	Describes the targeted I/O port.
         * @return 				u16int	Returns the word value of the selected port.
         */
        static u16int inw(u16int port);

        /**
         * Disables CPU interrupts
         * @return 			void	Returns void.
         */
        static void DISABLE_CPU_INTERRUPTS();

        /**
         * Enables CPU interrupts
         * @return 			void	Returns void.
         */
        static void ENABLE_CPU_INTERRUPTS();

        /**
         * Check CPU interrupts
         * @return          bool    Returns whether interrupts are enabled
         */
        static bool TEST_CPU_INTERRUPTS();

        /**
         * Halts the CPU, until next interrupt.
         * @return 			void	Returns void.
         */
        static void HALT_CPU();

        /**
         * Halts the CPU permenently in an infinite halt loop,
         * only interrupts can possibly be run.
         * @return 			void	Never returns.
         */
        NORETURN static void LOCK_CPU();

        /**
         * Halts the CPU permenently in an infinite halt loop,
         * interrupts cannot be run.
         * @return 			void	Never returns.
         */
        NORETURN static void KILL_LOCK_CPU();
    private:
    protected:
};

class Util
{
    //----------
    // Functions
    public:
        /**
         * Copies num bytes from source to destination.
         * Note: source and destination may not overlap.
         *
         * @param[out]  Destination	void*		Pointer to the destination.
         * @param[in]  	Source		const void*	Pointer to the source.
         * @param[in]  	Num			size_t		The number of bytes to be copied.
         * @return 					void*		Returns Destination.
         */
        static void *memcpy(void *destination, const void *source, size_t num);

        /**
         * Copies num bytes from source to destination.
         * Note: source and destination may overlap.
         *
         * @param[out]  Destination	void*		Pointer to the destination.
         * @param[in]  	Source		const void*	Pointer to the source.
         * @param[in]  	Num			size_t		The number of bytes to be copied.
         * @return 					void*		Returns Destination.
         */
        static void *memmove(void *destination, const void *source, size_t num);

        /**
         * Sets num bytes of pointer to value.
         *
         * @param[out]  Pointer		void*		The pointer to be set on.
         * @param[in]  	Value		int			Value to be set.
         * @param[in]  	Num			size_t		The number of bytes to be set.
         * @return 					void*		Returns Pointer
         */
        static void *memset(void *pointer, int value, size_t num);

        /**
         * Compare two strings.
         *
         * @param[in]  String1		const char*	C string to be compared.
         * @param[in]  String2		const char* C string to be compared.
         * @return 					int			Returns an integer value incating the
         * relationship between the strings; A zero value indicates that both strings are equal.
         * A value greater than zero indicates that the first character that does not match
         * has a greater value in string1 than in string2; And a value less than zero indicates the opposite.
         */
        static int strcmp(const char *string1, const char *string2);

        /**
         * Copies a string from source to destination.
         *
         * @param[out]  Destination	char*		Pointer to the destination array where the content is to be copied.
         * @param[in]  	Source		const char* C string to be copied.
         * @return 					char*		Returns Destination.
         */
        static char *strcpy(char *destination, const char *source);

        /**
         * Appends the string source to destination.
         *
         * @param[out]  Destination	char*		Pointer to the destination array, which should contain a C string, and be large enough to contain the concatenated resulting string.
         * @param[in]  	Source		const char* C string to be appended. This should not overlap destination.
         * @return 					char*		Returns Destination.
         */
        static char *strcat(char *destination, const char *source);

        /**
         * Returns the length of string
         *
         * @param[in]  String	const char*		C string.
         * @return 				size_t			Returns The length of string.
         */
        static size_t strlen(const char *string);


        /**
         * Writes an error message, and kill locks the cpu.
         * SHOULD BE CALLED ONLY THOUGH THE MACRO 'PANIC'
         *
         * @param[in]  Message	const char*	The error message to be printed.
         * @return 				void		Never returns.
         */
        NORETURN static void PANIC_IMPL(const char* message, const char* file, u32int line);
        
        /**
         * Writes an error message, and kill locks the cpu.
         * SHOULD BE CALLED ONLY THOUGH THE MACRO 'ASSERT'
         *
         * @param[in]  Message	const char*	The error message to be printed.
         * @return 				void		Never returns.
         */
        NORETURN static void PANIC_ASSERT_IMPL(const char* message, const char* file, u32int line);
    private:
        /**
         * Copies num bytes from source to destination.
         * Note: source and destination may not overlap.
         * Note: this function is just like memcpy, except that it copies from the other end.
         *
         * @param[out]  Destination	void*		Pointer to the destination.
         * @param[in]  	Source		const void*	Pointer to the source.
         * @param[in]  	Num			size_t		The number of bytes to be copied.
         * @return 					void*		Returns Destination.
         */
        static void *inverse_memcpy(void *destination, const void *source, size_t num);

    protected:
};

/**
 * Checks a single bit from a variable.
 *
 * @param[in]  var		MACRO_VAR 	The variable to be checked bits from.
 * @param[in]  pos		MACRO_VAR 	The bit position to check.
 * @return 				bool		Returns a boolean based upon the read bit.
 */
#define CHECK_BIT(var,pos) ((var) & (1<<(pos)))

/**
 * Sets a single bit in a variable.
 *
 * @param[in]  var		MACRO_VAR 	The variable to be checked bits from.
 * @param[in]  pos		MACRO_VAR 	The bit position to check.
 * @return 				void		Returns void.
 */
#define SET_BIT(var,pos) ((var) |= (1<<(pos)))

/**
 * Clears a single bit in a variable.
 *
 * @param[in]  var		MACRO_VAR 	The variable to be checked bits from.
 * @param[in]  pos		MACRO_VAR 	The bit position to check.
 * @return 				void		Returns void.
 */
#define CLEAR_BIT(var,pos) ((var) &= ~(1<<(pos)))

/**
 * Toogles a single bit in a variable.
 *
 * @param[in]  var		MACRO_VAR 	The variable to be checked bits from.
 * @param[in]  pos		MACRO_VAR 	The bit position to check.
 * @return 				void		Returns void.
 */
#define TOGGLE_BIT(var,pos) ((var) ^= (1<<(pos)))

#endif // _OSDEV_COMMON_HPP
