/**
 * @file	ctor.cpp
 * @author  Emil Madsen <skeen@cs.au.dk>
 * @version 1.0
 *
 * @section LICENSE
 *
 * Public Domain.
 *
 * @section DESCRIPTION
 *
 * The code responsible for calling the cpp constructor and destructors.
 */

#include "common.hpp"
#include "ctor.h"

/** A typedef for the default constructor function pointer type */
typedef void (*function_pointer) (void);
/** Externs to the linker symbols (these define the endpoints of the lists) */
/** Constructors */
extern function_pointer start_ctors[];
extern function_pointer end_ctors[];
/** Destructors */
extern function_pointer start_dtors[];
extern function_pointer end_dtors[];

/** Executes all the constructors found in the ctor section */
void executeConstructors()
{
    // We don't need to divide by 4 (bytes per function pointer), as the
    // compiler figures this from the typedef.
    u32int numberOfConstructors = (end_ctors - start_ctors);

    // Loop though all the constructors,
    // loading and executing them one at a time.
    for(u32int x=0; x < numberOfConstructors; x++)
    {
        function_pointer constructor = start_ctors[x];
        constructor();
    }
}

/** Executes all the destructors found in the dtor section */
void executeDestructors()
{
    // We don't need to divide by 4 (bytes per function pointer), as the
    // compiler figures this from the typedef.
    u32int numberOfDestructors = (end_dtors - start_dtors);

    // Loop though all the destructors,
    // loading and executing them one at a time.
    for(u32int x=0; x < numberOfDestructors; x++)
    {
        function_pointer destructor = start_dtors[x];
        destructor();
    }
}
