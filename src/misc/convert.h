typedef void (*print_function)(char c);

void convert(u32int n, print_function f)
{
    // Recursive integer to string
    char digit = '0' + (n % 10);
    n = n / 10;
    if (n > 0)
    {
        f(n);
    }
    f(digit);
}
