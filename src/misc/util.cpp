/**
 * @file	util.cpp
 * @author  Emil Madsen <skeen@cs.au.dk>
 * @version 1.0
 *
 * @section LICENSE
 *
 * Public Domain.
 *
 * @section DESCRIPTION
 *
 * Implements the util class described in common.hpp
 */

#include "common.hpp"
#include <Monitor/printf.hpp>

// Doxygen in common.hpp
void* Util::memcpy(void *destination, const void *source, size_t num)
{
    const u8int *source_pointer = (const u8int*) source;
    u8int *destination_pointer = (u8int*) destination;
    for(; num != 0; num--)
	{
		*(destination_pointer++) = *(source_pointer++);
	}
	return destination;
}

// Doxygen in common.hpp
void* Util::inverse_memcpy(void *destination, const void *source, size_t num)
{
    const u8int *source_pointer = (const u8int*) source;
    u8int *destination_pointer = (u8int*) destination;

	source_pointer = source_pointer + num;
	destination_pointer = destination_pointer + num;

    for(; num != 0; num--)
	{
		*(destination_pointer--) = *(source_pointer--);
	}
	return destination;
}

// Doxygen in common.hpp
void* Util::memmove(void *destination, const void *source, size_t num)
{
	if(destination > source)
	{
		Util::inverse_memcpy(destination, source, num);
	}
	else
	{
		Util::memcpy(destination, source, num);
	}
	return destination;
}

// Doxygen in common.hpp
void* Util::memset(void *pointer, int value, size_t num)
{
	u8int *destination = (u8int*) pointer;
	
    for (; num != 0; num--) 
	{
		*(destination++) = value;
	}
	return pointer;
}

// Doxygen in common.hpp
int Util::strcmp(const char *string1, const char *string2)
{
    int dist = 0; 

    while (!dist && *string1 && *string2) 
	{    
		dist = (*string2++) - (*string1++); 
	}

	if (dist == 0)
		return 0;
	else if (dist > 0) 
        return -1; 
	else //if (dist < 0) 
        return 1; 
}

// Doxygen in common.hpp
char* Util::strcpy(char *destination, const char *source)
{
    do
    {
      *(destination++) = *(source++);
    }
    while(*source != 0);
	return destination;
}

// Doxygen in common.hpp
char* Util::strcat(char *destination, const char *source)
{
	destination = destination + strlen(destination);

    do
    {
        *(destination++) = *(source++);
    }
    while (*source != 0);
    return destination;
}

// Doxygen in common.hpp
size_t Util::strlen(const char *string)
{
    size_t i = 0;
    while (*(string++))
        i++;
    return i;
}

// Doxygen in common.hpp
void Util::PANIC_IMPL(const char* message, const char* file, u32int line)
{
    printf("PANIC (%s) at %s : %d\n", message, file, line);
    Hardware::KILL_LOCK_CPU();
}

// Doxygen in common.hpp
void Util::PANIC_ASSERT_IMPL(const char* message, const char* file, u32int line)
{
    printf("ASSERTION-FAILED (%s) at %s : %d\n", message, file, line);
    Hardware::KILL_LOCK_CPU();
}
