#ifndef _OSDEV_LIBGLOSS_FILEOPS_HPP
#define _OSDEV_LIBGLOSS_FILEOPS_HPP

#include <VFS/FileSystem.hpp>

// Initialize fileops and thereby most of the file functions
void fileops_init(FileNode *root);

ASM_LINK int open(char *file,int flags, int mode);
ASM_LINK int read(int file, char *ptr, int len);
ASM_LINK int write(int file, char *ptr, int len);
ASM_LINK int close(int fildes);

ASM_LINK int fstat(int fildes, struct stat *st);
ASM_LINK int isatty(int file);

ASM_LINK int stat(const char *file, struct stat *st);
ASM_LINK int lseek(int file, int ptr, int dir);

ASM_LINK int chown(const char *path, uid_t owner, gid_t group);
ASM_LINK int readlink(const char *path, char *buf, size_t bufsize);
ASM_LINK int symlink(const char *path1, const char *path2);

#endif //_OSDEV_LIBGLOSS_FILEOPS_HPP
