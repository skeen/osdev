#ifndef _OSDEV_SBRK_HPP
#define _OSDEV_SBRK_HPP

// Initialize sbrk and thereby most of the clib
void sbrk_init(LinearMemoryManager *lmm_init);

#endif //_OSDEV_SBRK_HPP
