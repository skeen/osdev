#include "libgloss.hpp"

#include <paging/Paging.hpp>
#include <paging/PhysicalMemoryManager.hpp>
#include <paging/LinearMemoryManager.hpp>

// Requires paging to be enabled!

// Heap start address (virtual)
const u32int HEAP_START_ADDRESS    = 0x1000000;

// Current end (page) of the heap
PhysicalPageAddress heap_end = PhysicalPageAddress(0x0);
// Offset into current page
u16int offset = 0;

// Linear Memory Manager
LinearMemoryManager *lmm = nullptr;

// Allocate a page on the kernel directory at the given location
void allocate_page(PhysicalPageAddress address)
{
    page_directory_t* kernel_directory = lmm->getKernelDirectory();
    virtual_address virt = virtual_address(address.getAddress());
    lmm->allocPage(virt, kernel_directory);
}

// Initialize sbrk
void sbrk_init(LinearMemoryManager *lmm_init)
{
    lmm = lmm_init;
    heap_end = PhysicalPageAddress(HEAP_START_ADDRESS); 
    allocate_page(heap_end);
}

ASM_LINK void *sbrk(s32int incr)
{
    if (heap_end == 0)
    {
        PANIC("sbrk called, before init");
    }

    if(incr < 0)
    {
        PANIC("Decrementing sbrk not allowed");
    }

    u32int block = heap_end.getAddress() + offset;
    
    // If the allocation would overrun our currently allocated page,
    // allocate another one
    if (offset + incr > (s32int) BYTES_PER_PAGE)
    {
        // Allocate another page
        heap_end = PhysicalMemoryManager::incrementPageAddress(heap_end);
        allocate_page(heap_end);
        // Calculate the new offset
        offset = (offset + incr) % BYTES_PER_PAGE;
    }
    // Otherwise simply add the incr to our offset
    else
    {
        offset = offset + incr;
    }

    return (void*) block; 
} 
