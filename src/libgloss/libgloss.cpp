#include "libgloss.hpp"

#include <misc/common.hpp>

#include <_ansi.h>
#include <_syslist.h>
#include <errno.h>
#include <limits.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/times.h>

extern u32int end;

struct timeval;

char *__env[1] = { 0 }; 
char **environ = __env; 

#undef errno
ASM_LINK int errno;

ASM_LINK void exit(int rc)
{
  /* Default stub just causes a divide by 0 exception.  */
  int x = rc / INT_MAX;
  x = 4 / x;

  /* Convince GCC that this function never returns.  */
  for (;;)
    ;
}

ASM_LINK int execve(char  *name, char **argv, char **env)
{
  errno = ENOSYS;
  return -1;
}

ASM_LINK int fork()
{
  errno = ENOSYS;
  return -1;
}

ASM_LINK int getpid()
{
  return 1;
}

ASM_LINK int gettimeofday(struct timeval  *ptimeval, void *ptimezone)
{
  errno = ENOSYS;
  return -1;
}

ASM_LINK int kill(int pid, int sig)
{
  errno = ENOSYS;
  return -1;
}

ASM_LINK int link(char *existing, char *cnew)
{
  errno = ENOSYS;
  return -1;
}


ASM_LINK clock_t times(struct tms *buf)
{
  errno = ENOSYS;
  return -1;
}

ASM_LINK int unlink(char *name)
{
  errno = ENOSYS;
  return -1;
}

ASM_LINK int wait(int *status)
{
  errno = ENOSYS;
  return -1;
}
