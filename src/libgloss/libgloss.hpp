#ifndef _OSDEV_LIBGLOSS_HPP
#define _OSDEV_LIBGLOSS_HPP

#include <misc/compiler.h>

#include <sys/types.h>

ASM_LINK int errno;

ASM_LINK void exit(int rc);
ASM_LINK int execve(char  *name, char **argv, char **env);
ASM_LINK int fork();
ASM_LINK int getpid();
ASM_LINK int gettimeofday(struct timeval  *ptimeval, void *ptimezone);
ASM_LINK int isatty(int file);
ASM_LINK int kill(int pid, int sig);
ASM_LINK int link(char *existing, char *cnew);
ASM_LINK clock_t times(struct tms *buf);
ASM_LINK int unlink(char *name);
ASM_LINK int wait(int *status);

#endif //_OSDEV_LIBGLOSS_HPP
