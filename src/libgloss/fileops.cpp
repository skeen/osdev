#include "fileops.hpp"

ASM_LINK int errno;

#include <misc/datastructures/Vector.hpp>
#include <misc/datastructures/Pair.hpp>

FileNode *root = nullptr;

Vector<Pair<u32int,FileNode*>*> *openFiles;

struct stat
{
};

u32int unique_id()
{
    static u32int i = 0;
    i++;
    return i;
}

// Initialize fileops and thereby most of the file functions
void fileops_init(FileNode *rootnode)
{
    root = rootnode;
    openFiles = new Vector<Pair<u32int,FileNode*>*>();
}

FileNode* findNode(u32int file_descriptor)
{
    for(u32int x=0; x<openFiles->size(); x++)
    {
        Pair<u32int, FileNode*> *p = openFiles->at(x);
        if(p->getFirst() == file_descriptor)
        {
            FileNode *found = p->getSecond();
            return found;
        }
    }
    return nullptr;
}

#include <monitor/printf.hpp>

void printString(String s)
{
    const char* cstr = s.toCString();
    printf("%s\n", cstr);
    delete cstr;
}

FileNode* findNode(FileNode *start, String path)
{
    // Split on '/'
    int split = path.findFirst('/');
    // If no '/' left, we found our file, lets return it
    if(split == -1)
    {
        FileNode *node = start->finddir(path);
        if(node == nullptr)
        {
            return nullptr;
        }
        else
        {
            return node;
        }
    }
    else // Otherwise search for it
    {
        String folder_name = path.substring(0, split);

        FileNode *node = start->finddir(folder_name);
        if(node == nullptr)
        {
            return nullptr;
        }
        else
        {
            // Skip '/' character.
            String rest = path.substring(split+1, path.length());
            printString(rest);
            return findNode(node, rest);
        }
    }
}

ASM_LINK int open(char *file, int flags, int mode)
{
    String file_path = String(file);
    printString(String("Open: ") + file_path);
    //file_path = file_path.substring(1,file_path.length());

    FileNode *node = findNode(root, file_path);
    if(node == nullptr)
    {
        return -1;
    }
    else
    {
        u32int uniqueID = unique_id();
        openFiles->push_back(new Pair<u32int, FileNode*>(uniqueID, node));
        return uniqueID;
    }
}

ASM_LINK int fstat(int fildes, struct stat *st)
{
    printf("fstat\n");
    //st->st_mode = S_IFCHR;
    return 0;
}

ASM_LINK int readlink(const char *path, char *buf, size_t bufsize)
{
    printf("readlink\n");
  //errno = ENOSYS;
  return -1;
}

ASM_LINK int symlink(const char *path1, const char *path2)
{
    printf("symlink\n");
  //errno = ENOSYS;
  return -1;
}

ASM_LINK int chown(const char *path, uid_t owner, gid_t group)
{
    printf("chown\n");
  //errno = ENOSYS;
  return -1;
}

ASM_LINK int stat(const char *file, struct stat *st)
{
    printf("stat\n");
    //st->st_mode = S_IFCHR;
    return 0;
}

ASM_LINK int lseek(int file, int ptr, int dir)
{
    printf("lseek\n");
    return 0;
}

ASM_LINK int close(int fildes)
{
    printf("close\n");
  return -1;
}

ASM_LINK int isatty(int file)
{
    printf("isatty\n");
    return 0;
}

ASM_LINK int read(int file, char *ptr, int len)
{
    //printf("read\n");
    FileNode *node = findNode(file);
    if(node == nullptr)
    {
        PANIC("NODE NOT FOUND");
        return -1;
    }
    return node->read(0, len, (u8int*) ptr);
}

ASM_LINK int write(int file, char *ptr, int len)
{
    //printf("write\n");
    FileNode *node = findNode(file);
    if(node == nullptr)
    {
        PANIC("NODE NOT FOUND");
        return -1;
    }
    return node->write(0, len, (u8int*) ptr);
}
