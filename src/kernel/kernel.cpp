/**
 * @file	kernel.c
 * @author  Emil Madsen <skeen@cs.au.dk>
 * @version 1.0
 *
 * @section LICENSE
 *
 * Public Domain.
 *
 * @section DESCRIPTION
 *
 * The kernel entry point
 */

#define MALLOC_DEFINED

#include "Console.hpp"

#include <monitor/printf.hpp>

#include <tables/InterruptDescriptorTable.hpp>
#include <tables/GlobalDescriptorTable.hpp>
#include <tables/InterruptServiceRoutine.h>
#include <tables/CPU_Dedicated_Interrupts.h>

#include <misc/compiler.h>
#include <misc/ctor.h>
#include <misc/common.hpp>
#include <paging/PagingSystem.hpp>

#include <misc/multiboot.h>

#include <drivers/CDEV/PIT/PITCharacterDevice.hpp>
#include <drivers/CDEV/Serial/SerialCharacterDevice.hpp>
#include <drivers/CDEV/Console/ConsoleCharacterDevice.hpp>
#include <drivers/CDEV/EmulatorShutdown/EmulatorShutdownCharacterDevice.hpp>
#include <drivers/CDEV/Keyboard/KeyboardCharacterDevice.hpp>

#include <VFS/FileSystem.hpp>
#include <VFS/RootFileSystem.hpp>

#include <libgloss/sbrk.hpp>
#include <libgloss/fileops.hpp>
#include <malloc.h>

#include <multitasking/Scheduler.hpp>

void sleep(u32int i)
{
    for(u32int x=0; x<i; x++)
    {
        for(u32int y=0; y<0xFFFF; y++)
        {
        }
    }
}

namespace clib
{
    #include <stdio.h>
    #include <malloc.h>
}

struct SystemConfig
{
    PagingSystem *ps;
    Scheduler *sched;
    InterruptService *interrupt;
};

SystemConfig config;

void test_paging()
{
    u32int *ptr = (u32int*)0xFACEBEEF;
    u32int do_page_fault = *ptr;
    PANIC("NO PAGEFAULT HAPPEND");
}

void enable_paging(multiboot_info_t* mbd)
{
    void* memory_map_address = (void*) mbd->mmap_addr;
    memory_map* memory_map_array = (memory_map*) memory_map_address;
    
    u64int bytes = 0;
 
    u8int number_of_memory_map_entries = (mbd->mmap_length / sizeof(memory_map));
    for(u8int x=0; x<number_of_memory_map_entries; x++)
    {
        memory_map memory_map_entry = memory_map_array[x];
        printf("Size: %d, Address: %x, Length: %x, Type: %d\n", memory_map_entry.size, (u32int) memory_map_entry.base_address, (u32int) memory_map_entry.length, memory_map_entry.type);

        if(memory_map_entry.type == 1)
        {
            bytes = bytes + memory_map_entry.length;
        }
    }

    printf("TOTAL MEMORY: %d bytes = %d mb\n", (u32int) bytes, (u32int) (bytes/(1024*1024)));

    static PagingSystem ps = PagingSystem(mbd);
    config.ps = &ps;
    printf("PAGING ACTIVATED\n");
}

void test_clib()
{
    void* ptr1 = malloc(256);
    printf("ptr1 location; %x\n", (u32int) ptr1);
    void* ptr2 = malloc(256);
    printf("ptr2 location; %x\n", (u32int) ptr2);
    printf("ptr1 free\n");
    free(ptr1);
    void* ptr3 = malloc(256);
    printf("ptr3 location; %x\n", (u32int) ptr3);
}

void enable_clib()
{
    LinearMemoryManager *lmm = (config.ps)->getLinearMemoryManager();
    sbrk_init(lmm);
    // Seed for NEW
    seed_malloc(malloc);
    seed_free(free);
    printf("malloc ACTIVATED\n");
}
/*
void printage(u32int arg)
{
    u32int esp;
    asm volatile("mov %%esp, %0" : "=r"(esp));
    printf("Stack is at; %x\n", esp);
    config.sched->schedule();
    printf("Argument is; %d\n", arg);
    config.sched->schedule();
}
*/

void loopfunction(char c)
{
    while(true)
    {
        printf("%c", c);
        /*
        TCB* current = Scheduler::getCurrentThread();
        printf("TID = %d\n", current->getTID());
        Scheduler::yield();
        */
        sleep(10);
    }
}

void print_increment(u32int increment)
{
    for(u32int x=0; x<increment; x++)
    {
        printf("\t");
    }
}

void fileSystemPrint(FileNode *f, u32int increment=0, String prefix="")
{
    if(increment==0)
    {
        printf("Printing contents of / \n");
    }
    else
    {
        String prefixed_name = prefix;
        const char* node_name = prefixed_name.toCString();
        print_increment(increment);
        printf("Printing contents of %s \n", node_name);
        delete node_name;
    }

    u32int i=0;
    dirent_t* node = f->readdir(0);
    while(node != nullptr)
    {
        FileNode* filenode = f->finddir(node->name);
        if(filenode != nullptr)
        {
            String prefixed_node_name = (prefix + "/" + filenode->getName());
            const char* name = prefixed_node_name.toCString();
            switch(filenode->getNodeType())
            {
                case File:
                    print_increment(increment);
                    printf("(file): %s\n", name);
                    break;
                case Directory:
                    fileSystemPrint(filenode, increment+1, prefixed_node_name);
                    break;
                case CharDevice:
                    print_increment(increment);
                    printf("(cdev): %s\n", name);
                    break;
                default:
                    print_increment(increment);
                    printf("MAGIC\n");
                    break;
            }
            delete name;
        }

        i++;
        node = f->readdir(i);
    }
};

ASM_LINK u32int read_eip();

ASM_LINK int kernel_main(u32int magic, multiboot_info_t* mbd)
{
    // These are defined in the assembly loader

    if ( magic != 0x2BADB002 )
    {
        PANIC("MULTIBOOT MAGIC NUMBER MALFORMED");
    }
    
    // Call our constructors
    executeConstructors();

    // Initialise all the ISRs and segmentation
    GlobalDescriptorTable::instance();
    InterruptDescriptorTable::instance();

    enable_paging(mbd);

    enable_clib();
    test_clib();

    config.interrupt = new InterruptService();
    seedInterruptService(config.interrupt);

    Vector<Pair<u8int, Runnable<void, registers_t*>*>> CPU_interrupt_handlers = getCPUDedicatedInterruptHandlers();
    for(u32int x=0; x<CPU_interrupt_handlers.size(); x++)
    {
        Pair<u8int, Runnable<void, registers_t*>*> p = CPU_interrupt_handlers.at(x);
        config.interrupt->seedHandler(p);
    }
    
    //asm volatile("int $7");

    printf("Variable: %d, Hex: %u, String: %s\n", 5, 17, "Test");

    config.sched = new Scheduler((config.ps)->getLinearMemoryManager());

    /*
    for(u32int x=0; x<5; x++)
    {
        config.sched->schedule();
    }

    Runnable<void>* run = makeRunnable(printage, (u32int) 2);
    config.sched->createNewThread(run);
    config.sched->schedule();

    printf("BACK IN KERNEL_MAIN\n");
    
    config.sched->schedule();
    config.sched->schedule();
    config.sched->schedule();
    */
    /*
    init_serial(COM1_PORT);
    while(true)
    {
        //write_serial(COM1_PORT, 'Z');
        char c = read_serial(COM1_PORT);
        write_serial(COM1_PORT,c);
        printf("%c", c);
    }
    */

    /*
    Hardware::DISABLE_CPU_INTERRUPTS();
    */
    //init_timer(50); // Initialise timer to 50Hz 

    /*
    clib::printf("printf\n");
    clib::fprintf(stdout, "fprintf stdio\n");
    clib::fprintf(stderr, "fprintf stderr\n");
    */
/*
    String s = String("StringTest");
    for(u32int x=0; x<5; x++)
    {
        const char* name = s.toCString();
        printf("%s\n", name);
        delete name;
        s += "z";
        s += String("c") + String("y");
    }

    //Serial s = Serial(1);
    //
    RootFileNode* root = new RootFileNode();

    fileops_init(root);

    SerialCharacterDevice *serial = new SerialCharacterDevice(1);
    ConsoleCharacterDevice *console = new ConsoleCharacterDevice();
    EmulatorShutdownCharacterDevice *shutdown = new EmulatorShutdownCharacterDevice();

    KeyboardCharacterDevice *keyboard = new KeyboardCharacterDevice();
    Pair<u8int, Runnable<void,registers_t*>*> keyboard_handler = keyboard->getKeyboardInterruptHandler();
    config.interrupt->seedHandler(keyboard_handler);

    PITCharacterDevice *pit = new PITCharacterDevice();
    Pair<u8int, Runnable<void,registers_t*>*> pit_handler = pit->getPITInterruptHandler();
    config.interrupt->seedHandler(pit_handler);
    
    FolderNode* folder = new FolderNode("dev");

    root->addNode(folder);
    folder->addNode(serial);
    folder->addNode(console);
    folder->addNode(shutdown);
    folder->addNode(keyboard);
    folder->addNode(pit);

    // list the contents of /
    fileSystemPrint(root);
    
    Hardware::ENABLE_CPU_INTERRUPTS();
*/
/*
    int i = 0;
    struct dirent *node = 0;
    while ( (node = readdir_fs(fs_root, i)) != 0)
    {
        clib::printf("Found file ");
        clib::printf("%s", node->name);
        fs_node_t *fsnode = finddir_fs(fs_root, node->name);

        if ((fsnode->flags & 0x7) == FS_DIRECTORY)
        {
            clib::printf("\n\t(directory)\n");
        }
        else
        {
            clib::printf("\n\t contents: \"");
            unsigned char buf[256];
            u32int sz = read_fs(fsnode, 0, 256, buf);
            unsigned int j;
            for (j = 0; j < sz; j++)
            {
                 clib::printf("%c", buf[j]);
            }
            
            clib::printf("\"\n");
        }
        i++;
    }
    */

    /*
    // Start multitasking.
    initialise_tasking();

    int ret = fork();
    printf("fork() returned(%d), and getpid() returned(%d)\n", ret, getpid());
    */

    Runnable<void>* forever_run1 = makeRunnable(loopfunction, ',');
    config.sched->createNewThread(forever_run1);
    Runnable<void>* forever_run2 = makeRunnable(loopfunction, '.');
    config.sched->createNewThread(forever_run2);
    printf("START\n");

    config.interrupt->seedHandler(config.sched->getInterruptHandler());

    SysCall sc = SysCall();
    config.interrupt->seedHandler(sc.getInterruptHandler());
    sc.seedHandler(config.sched->getYieldSysCall());
    sc.seedHandler(config.sched->getBlockSysCall());
    sc.seedHandler(config.sched->getCurrentThreadSysCall());
    sc.seedHandler(config.sched->getKillThreadSysCall());

    Hardware::ENABLE_CPU_INTERRUPTS();

    loopfunction('!');

    // Run the terminal
    Console terminal;
    terminal.run();

    for(;;);

    // Clean up!
    executeDestructors();
    // A code will be easy to spot, in the debug log 
    return 0xDEADDEAD;
}
