#ifndef _OSDEV_KERNEL_CONSOLE_HPP
#define _OSDEV_KERNEL_CONSOLE_HPP

#include <misc/common.hpp>

class Console
{
    public:
        Console();
        void run();
};

#endif //_OSDEV_KERNEL_CONSOLE_HPP
