#include "Scheduler.hpp"

// Thread entry point (Assembly)
ASM_LINK void start_thread();
// Thread entry point (called from Assembly)
ASM_LINK void start_thread_cpp(Runnable<void> *func)
{
    // Invoke the provided function
    func->invoke();
    // Once done, free it
    delete func;
    // And then kill the thread
    Scheduler::killThread();
    // Spin until killed
    while(true);
}

Scheduler::Scheduler(LinearMemoryManager *lmm)
    : currentThread(nullptr), sc(lmm), lmm(lmm)
{
    PCB *process = new PCB(lmm->getKernelDirectory());
    process->seedThread();
    // And lets add the newborn thread to our scheduler
    TCB *thread = new TCB(process);
    currentThread = thread;
    currentThread->setRunning();
}

void Scheduler::createNewThread(Runnable<void> *func)
{
    // And lets add the newborn thread to our scheduler
    TCB *thread = new TCB(currentThread->getProcess());
    currentThread->getProcess()->seedThread();

    // Lets allocate a stack
    Pair<u32int,u32int> allocated_stack = sc.setupStack(currentThread->getProcess());
    u32int stack_location = allocated_stack.getFirst();
    u32int stack_size     = allocated_stack.getSecond();

    const u32int FLAGS_RESERVED1    = (1<<1); /* read as one */
	const u32int FLAGS_INTERRUPT    = (1<<9);
	const u32int FLAGS_ID           = (1<<21);

    registers_t regs;
    regs.eip = (u32int) start_thread;
    regs.useresp = stack_location + stack_size;
    regs.eax = 0;
    regs.ebx = 0;
    regs.ecx = 0;
    regs.edx = 0;
    regs.edi = 0;
    regs.esi = (u32int) func;
    regs.ebp = 0;
    regs.cs = 0x08;
    regs.ds = 0x10;
    regs.ss = 0x10;
    regs.eflags = FLAGS_RESERVED1 | FLAGS_INTERRUPT | FLAGS_ID;

    thread->saveAll(&regs);
    threads.put(thread);
}

Pair<u8int, Runnable<void, registers_t*>*> Scheduler::getInterruptHandler()
{
    Runnable<void, registers_t*> *handler = makeRunnable(*this, &Scheduler::scheduleNext, TCB::RUNNABLE);
    return Pair<u8int, Runnable<void, registers_t*>*>(IRQ0, handler);
}
Pair<u8int, Runnable<void, registers_t*>*> Scheduler::getYieldSysCall()
{
    Runnable<void, registers_t*> *handler = makeRunnable(*this, &Scheduler::scheduleNext, TCB::RUNNABLE);
    return Pair<u8int, Runnable<void, registers_t*>*>(yield_id, handler);
}

Pair<u8int, Runnable<void, registers_t*>*> Scheduler::getBlockSysCall()
{
    Runnable<void, registers_t*> *handler = makeRunnable(*this, &Scheduler::scheduleNext, TCB::BLOCKED);
    return Pair<u8int, Runnable<void, registers_t*>*>(block_id, handler);
}

Pair<u8int, Runnable<void, registers_t*>*> Scheduler::getCurrentThreadSysCall()
{
    Runnable<void, registers_t*> *handler = makeRunnable(*this, &Scheduler::getCurrentThread);
    return Pair<u8int, Runnable<void, registers_t*>*>(currentThread_id, handler);
}

Pair<u8int, Runnable<void, registers_t*>*> Scheduler::getKillThreadSysCall()
{
    Runnable<void, registers_t*> *handler = makeRunnable(*this, &Scheduler::scheduleNext, TCB::TERMINATED);
    return Pair<u8int, Runnable<void, registers_t*>*>(killThread_id, handler);
}

void Scheduler::yield()
{
    SysCall::triggerSysCall(yield_id);
}

void Scheduler::block()
{
    SysCall::triggerSysCall(block_id);
}

TCB* Scheduler::getCurrentThread()
{
    TCB* eax = nullptr;
    SysCall::triggerSysCall(currentThread_id);
    asm volatile("mov %%eax, %0" : "=r"(eax));
    return eax;
}

void Scheduler::killThread()
{
    SysCall::triggerSysCall(killThread_id);
}

void Scheduler::getCurrentThread(registers_t *regs)
{
    regs->eax = (u32int) currentThread;
}

void Scheduler::scheduleNext(TCB::ThreadState leave_state, registers_t *regs)
{
    currentThread->saveAll(regs);
    currentThread->setStatus(leave_state);
    threads.put(currentThread);

    while(true)
    {
        currentThread = threads.get();
        TCB::ThreadState still_alive = currentThread->getStatus();
        switch(still_alive)
        {
            case TCB::RUNNING:
                // Running on another CPU
                // Put it back into the threads list
                threads.put(currentThread);
                // And find another thread
                continue;
            case TCB::RUNNABLE:
                // This thread is runnable
                // Lets run it
                break;
            case TCB::BLOCKED:
                // This thread is blocked
                // Put it back into the threads list
                threads.put(currentThread);
                // And find another thread
                continue;
            case TCB::TERMINATED:
                // printf("Thread (%d) died\n",  currentThread->getTID());
                // This thread is terminated
                // Let's delete it
                delete currentThread;
                // And find another one to run
                continue;
            default:
                // This is not good!
                PANIC("UNHANDLED THREAD STATE");
                break;
        }
        currentThread->restoreAll(regs);
        currentThread->setRunning();
        // Load PCB as well
        // TODO: Only do this, if the pagedir has changed
        // (we run another process)
        lmm->switch_page_directory(currentThread->getProcess()->getPageDirectory());
        return;
    }
}
