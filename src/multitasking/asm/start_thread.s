.global start_thread
start_thread:
    push    %esi
    call start_thread_cpp
    # This above call never returns
    # This jump is just for security
    jmp start_thread
