#include "SysCall.hpp"

SysCall::SysCall()
{
}

void SysCall::triggerSysCall(u32int syscall_id)
{
    asm volatile("mov %0, %%eax; \
                  int $100"
                  : : "r"(syscall_id));
}

Pair<u8int, Runnable<void, registers_t*>*> SysCall::getInterruptHandler()
{
    Runnable<void, registers_t*> *handler = makeRunnable(*this, &SysCall::executeSysCall);
    return Pair<u8int, Runnable<void, registers_t*>*>(interrupt_handler_id, handler);
}

void SysCall::seedHandler(Pair<u8int, Runnable<void, registers_t*>*> handler)
{
    u8int id = handler.getFirst();
    if(findHandler(id) != nullptr)
    {
        PANIC("ADDING A SYSCALL HANDLER, WHEN ANOTHER HANDLER IS ATTACHTED");
    }
    handlers.push_back(handler);
}

Runnable<void, registers_t*>* SysCall::findHandler(u8int interrupt_id)
{
    for(u32int x=0; x<handlers.size(); x++)
    {
        Pair<u8int, Runnable<void, registers_t*>*> handler = handlers.at(x);
        u8int id = handler.getFirst();
        if(id == interrupt_id)
        {
            Runnable<void, registers_t*>* runnable = handler.getSecond();
            return runnable;
        }
    }
    return nullptr;
}

void SysCall::executeSysCall(registers_t* regs)
{
    u32int interrupt_id = regs->eax;
    Runnable<void, registers_t*>* runnable = findHandler(interrupt_id);
    if(runnable == nullptr)
    {
        PANIC("NO SUCH SYSCALL");
    }
    runnable->invoke(regs);
}
