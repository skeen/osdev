#ifndef _OSDEV_MULTITASKING_SYSCALL_HPP
#define _OSDEV_MULTITASKING_SYSCALL_HPP

#include <misc/common.hpp>
#include <misc/datastructures/StaticFIFO.hpp>
#include <misc/datastructures/Delegate.hpp>
#include <misc/datastructures/Pair.hpp>

#include <tables/InterruptServiceRoutine.h>

class SysCall
{
    private:
        static const u32int interrupt_handler_id = 100;

    public:
        SysCall();
        // Shouldn't be called directy, but rather through some class
        // As syscalls may be remapped whenever
        static void triggerSysCall(u32int syscall_id);

        Pair<u8int, Runnable<void, registers_t*>*> getInterruptHandler();
        void seedHandler(Pair<u8int, Runnable<void, registers_t*>*> handler);
    private:
        Runnable<void, registers_t*>* findHandler(u8int interrupt_id);
        void executeSysCall(registers_t* regs);

        Vector<Pair<u8int, Runnable<void, registers_t*>*>> handlers;
};

#endif //_OSDEV_MULTITASKING_SYSCALL_HPP
