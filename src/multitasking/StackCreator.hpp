#ifndef _OSDEV_MULTITASKING_STACKCREATOR_HPP
#define _OSDEV_MULTITASKING_STACKCREATOR_HPP

#include <paging/LinearMemoryManager.hpp>

#include <misc/datastructures/Pair.hpp>

#include "PCB.hpp"

//TODO: Possibly move this and create thread to PCB
class StackCreator
{
    public:
        StackCreator(LinearMemoryManager* lmm);

        Pair<u32int,u32int> setupStack(PCB* process);
    private:
        u32int getStackPointer();
        u32int setupStack(u32int location, page_directory_t* page_directory);
        LinearMemoryManager* lmm;
};

#endif //_OSDEV_MULTITASKING_STACKCREATOR_HPP
