#include "TCB.hpp"

#include "Scheduler.hpp"
/*
void TCB::thread_run(TCB *thread)
{
    Runnable<void>* invokeable = thread->getRunnable();
    invokeable->invoke();
    thread->setTerminated();
    if(thread->sched == nullptr)
    {
        PANIC("CANNOT KILL THE KERNEL/IDLE THREAD");
    }
    while(true)
    {
        thread->sched->schedule();
    }
    PANIC("thread_run exiting (should never happen)");
}
*/
/*
TCB::TCB()
    : thread_id(getID()), esp(0), ebp(0), eip(0), status(ThreadState::RUNNING), run(nullptr), sched(nullptr)
{
}

TCB::TCB(u32int init_esp, u32int init_ebp, Runnable<void>* run, Scheduler* sched)
    : thread_id(getID()), esp(init_esp), ebp(init_ebp), eip((u32int) &TCB::thread_run), status(ThreadState::NEW), run(run), sched(sched)
{
}
*/
/*
void TCB::setESP(u32int new_esp)
{
    esp = new_esp;
}

void TCB::setEBP(u32int new_ebp)
{
    ebp = new_ebp;
}

void TCB::setEIP(u32int new_eip)
{
    eip = new_eip;
}

u32int TCB::getESP() const
{
    return esp;
}

u32int TCB::getEBP() const
{
    return ebp;
}

u32int TCB::getEIP() const
{
    return eip;
}
*/
u32int TCB::getTID() const
{
    return thread_id;
}

u32int TCB::getID()
{
    static u32int id = 0;
    id++;
    return id;
}

TCB::ThreadState TCB::getStatus() const
{
    return status;
}

void TCB::setRunnable()
{
    if(status == ThreadState::TERMINATED)
    {
        PANIC("MAKING A DEAD THREAD RUNNABLE");
    }
    status = ThreadState::RUNNABLE;
}


void TCB::setRunning()
{
    if(status == ThreadState::TERMINATED)
    {
        PANIC("RUNNING A DEAD THREAD");
    }
    status = ThreadState::RUNNING;
}

void TCB::setBlocked()
{
    if(status == ThreadState::TERMINATED)
    {
        PANIC("RUNNING A DEAD THREAD");
    }
    status = ThreadState::BLOCKED;
}

void TCB::setTerminated()
{
    status = ThreadState::TERMINATED;
}
