#ifndef _OSDEV_MULTITASKING_PCB_HPP
#define _OSDEV_MULTITASKING_PCB_HPP

#include <misc/datastructures/Vector.hpp>
#include <paging/Paging.hpp>

class PCB
{
    public:
        PCB(page_directory_t* page_directory)
            : process_id(getID()), page_directory(page_directory), num_threads(0)
        {
        }

        u32int getPID()
        {
            return process_id;
        }

        page_directory_t* getPageDirectory()
        {
            return page_directory;
        }

        u32int getID()
        {
            static u32int id = 0;
            id++;
            return id;
        }

        // TODO: Scheduler only
        void seedThread()
        {
            num_threads = num_threads + 1;
        }
    private:
        // Process ID
        u32int process_id;
        // Page directory
        page_directory_t *page_directory;
        // The number of threads
        u32int num_threads;
};

#endif //_OSDEV_MULTITASKING_PCB_HPP
