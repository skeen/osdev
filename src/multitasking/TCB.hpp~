#ifndef _OSDEV_MULTITASKING_TCB_HPP
#define _OSDEV_MULTITASKING_TCB_HPP

#include <misc/common.hpp>

#include "PCB.hpp"

class Scheduler;

#include <tables/InterruptServiceRoutine.h>

//Thread Control Block
class TCB
{
    public:
        enum ThreadState
        {
            RUNNABLE,
            RUNNING,
            BLOCKED,
            TERMINATED
        };

    public:
        // Initialize a TCB with a unique ID
        //TCB();
        TCB(PCB* process)
            : thread_id(getID()), status(ThreadState::RUNNABLE), process(process)
        {
        }

        void saveAll(registers_t *src)
        {
            registers.eip = src->eip;
            registers.useresp = src->useresp;
            registers.eax = src->eax;
            registers.ebx = src->ebx;
            registers.ecx = src->ecx;
            registers.edx = src->edx;
            registers.edi = src->edi;
            registers.esi = src->esi;
            registers.ebp = src->ebp;
            registers.cs = src->cs;
            registers.ds = src->ds;
            registers.ss = src->ss;
            registers.eflags = src->eflags;
        }

        void restoreAll(registers_t *dest)
        {
            dest->eip = registers.eip;
            dest->useresp = registers.useresp;
            dest->eax = registers.eax;
            dest->ebx = registers.ebx;
            dest->ecx = registers.ecx;
            dest->edx = registers.edx;
            dest->edi = registers.edi;
            dest->esi = registers.esi;
            dest->ebp = registers.ebp;
            dest->cs = registers.cs;
            dest->ds = registers.ds;
            dest->ss = registers.ss;
            dest->eflags = registers.eflags;
        }

        u32int getTID() const;

        // Gets this threads state
        ThreadState getStatus() const;

        void setStatus(ThreadState state)
        {
            status = state;
        }
        
        // Sets the ThreadState to RUNNABLE
        void setRunnable();
        // Sets the ThreadState to RUNNING
        void setRunning();
        // Sets the ThreadState to TERMINATED
        void setTerminated();
        // Sets the ThreadState to BLOCKED
        void setBlocked();
    private:
        u32int getID();
        
        // Start a thread given a TCB
        static void thread_run(TCB *thread);
    private:
        // Thread ID
        u32int thread_id;
        // Is the thread alive?
        ThreadState status;
        // The registers of the thread
        // (Used when not running)
        registers_t registers;
        // The owning process
        PCB* process;
};

#endif //_OSDEV_MULTITASKING_TCB_HPP
