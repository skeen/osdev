#include "StackCreator.hpp"

StackCreator::StackCreator(LinearMemoryManager* lmm)
: lmm(lmm)
{
}

Pair<u32int,u32int> StackCreator::setupStack(PCB* process)
{
    page_directory_t* page_directory = process->getPageDirectory();
    u32int location = getStackPointer();
    u32int size = sizeof(page_frame_t);
    u32int allocated_location = setupStack(location, page_directory);
    return Pair<u32int, u32int>(allocated_location, size);
}

u32int StackCreator::getStackPointer()
{
    static PhysicalPageAddress stackLoc = PhysicalMemoryManager::floorPageAddress(0xFFFFFFFF);
    u32int temp = stackLoc.getAddress();
    stackLoc = PhysicalMemoryManager::decrementPageAddress(stackLoc);
    stackLoc = PhysicalMemoryManager::decrementPageAddress(stackLoc);
    return temp;
}

u32int StackCreator::setupStack(u32int location, page_directory_t* page_directory)
{
    lmm->allocPage(location, page_directory);
    return location;
}
