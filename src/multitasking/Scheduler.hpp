#ifndef _OSDEV_MULTITASKING_SCHEDULER_HPP
#define _OSDEV_MULTITASKING_SCHEDULER_HPP

#include "TCB.hpp"
#include "StackCreator.hpp"
#include "SysCall.hpp"

#include <misc/common.hpp>
#include <misc/datastructures/StaticFIFO.hpp>
#include <misc/datastructures/Delegate.hpp>
#include <misc/datastructures/Pair.hpp>

#include <tables/InterruptServiceRoutine.h>

class Scheduler
{
    private:
        // TODO: Assign these on compile time
        static const u32int yield_id = 0;
        static const u32int block_id = 1;
        static const u32int currentThread_id = 2;
        static const u32int killThread_id = 3;

    public:
        Scheduler(LinearMemoryManager *lmm);
        void createNewThread(Runnable<void> *func);
        //void createNewProcess(Runnable<void> *func);

        Pair<u8int, Runnable<void, registers_t*>*> getInterruptHandler();

        Pair<u8int, Runnable<void, registers_t*>*> getYieldSysCall();
        Pair<u8int, Runnable<void, registers_t*>*> getBlockSysCall();
        Pair<u8int, Runnable<void, registers_t*>*> getCurrentThreadSysCall();
        Pair<u8int, Runnable<void, registers_t*>*> getKillThreadSysCall();

        static void yield();
        static void block();
        static TCB* getCurrentThread();
        static void killThread();
    private:
        void getCurrentThread(registers_t *regs);
        void scheduleNext(TCB::ThreadState leave_state, registers_t *regs);
    private:
        // TODO: Support for multicore
        TCB *currentThread;
        // TODO: Use standard FIFO
        StaticFIFO<TCB*,10> threads;
        // The stack creation
        StackCreator sc;
        LinearMemoryManager *lmm;
};

#endif //_OSDEV_MULTITASKING_SCHEDULER_HPP
