#ifndef _OSDEV_MULTITASKING_DATASTRUCTURES_SPINLOCK_HPP
#define _OSDEV_MULTITASKING_DATASTRUCTURES_SPINLOCK_HPP

#include <misc/common.hpp>

// Use Mutex in mostcases
class Spinlock
{
    public:
        Spinlock(bool locked = false)
            : locked(locked)
        {
        }

        void lock()
        {
            // Spin while trying to lock
            while(!__sync_bool_compare_and_swap(&locked, 0, 1))
            {
                // Do nothing, we use spinlocks in multicore environments,
                // so it might change anytime soon
            }
        }

        void unlock()
        {
            asm volatile (""); // acts as a memory barrier.
            locked = 0;
        }
    private:
        volatile bool locked;
};

#endif //_OSDEV_MULTITASKING_DATASTRUCTURES_SPINLOCK_HPP
