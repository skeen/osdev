#ifndef _OSDEV_MULTITASKING_DATASTRUCTURES_SEMAPHORE_HPP
#define _OSDEV_MULTITASKING_DATASTRUCTURES_SEMAPHORE_HPP

#include "Spinlock.hpp"

#include <misc/common.hpp>

// TODO: Needs a wait queue
class Semaphore
{
    public:
        // Initialize the Semaphore with a value of x
        Semaphore(u32int x = 1)
            : permits(x);
        {
        }

        // acquire / wait
        void acquire(u32int n = 1)
        {
            spin.lock();
            while(n > available())
            {
                spin.unlock();
                Scheduler::yield();
                spin.lock();
            }
            decrement(n);
            spin.unlock();
            return;
        }

        // try to acquire (returns a bool of whether it succeded)
        bool try_acquire(u32int n = 1)
        {
            bool status = false;
            spin.lock();
            if(n <= available())
            {
                decrement(n);
                status = true;
            }
            spin.unlock();
            return status;
        }
        // release aka. signal
        void release(u32int n = 1)
        {
            spin.lock();
            increment(n);
            wakeup();
            spin.unlock();

        }
        // returns the number of current permits
        // Note; This might have changed since return
        u32int available()
        {
            // No locking as it might have been changed anyway
            return permits;
        }
    private:
        void decrement(u32int amount)
        {
            permits -= amount;
            if(permits < 0)
            {
                PANIC("NEGATIVE SEMAPHORE");
            }
        }
        
        void increment(u32int amount)
        {
            permits += amount;
        }
        u32int permits;
        Spinlock spin;
};

#endif //_OSDEV_MULTITASKING_DATASTRUCTURES_SEMAPHORE_HPP
