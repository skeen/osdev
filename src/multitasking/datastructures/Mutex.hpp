#ifndef _OSDEV_MULTITASKING_DATASTRUCTURES_MUTEX_HPP
#define _OSDEV_MULTITASKING_DATASTRUCTURES_MUTEX_HPP

#include <misc/common.hpp>

class Mutex
{
    public:
        Mutex(bool locked = false)
            : locked(locked)
        {
        }

        void lock()
        {
            // Spin while trying to lock
            while(!__sync_bool_compare_and_swap(&locked, 0, 1))
            {
                // Might as well just yield,
                // this thread is not going to unlock it
                // And Spinlock is for multicore
                Scheduler::yield();
            }
        }

        void unlock()
        {
            asm volatile (""); // acts as a memory barrier.
            locked = 0;
        }
    private:
        volatile bool locked;
};

#endif //_OSDEV_MULTITASKING_DATASTRUCTURES_MUTEX_HPP
