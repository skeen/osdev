


class TimedWait
{
    private:
        static const u32int sleep_id = 4;
    public:

        static void sleep(u32int ticks)
        {
            asm volatile("mov %0, %%eax" : "r"(ticks));
            SysCall::triggerSysCall(currentThread_id);
        }
    private:
        void add_to_waiting_queue(u32int sleep_ticks, TCB* thread)
        {
            Pair<u32int, TCB*> head = waiting_queue.head;
            if(head == nullptr)
            {
                // Insert a head
                waiting_queue.insert(nullptr, Pair(sleep_ticks, thread));
                return;
            }
            u32int timeout = head.getFirst();
            if(sleep_ticks < timeout)
            {
                // Remove the head
                waiting_queue.remove(nullptr);
                // Insert a new head
                auto node = waiting_queue.insert(nullptr, Pair(sleep_ticks, thread));
                // And insert the old head behind it
                u32int new_timeout = timeout-sleep_ticks;
                waiting_queue.insert(node, Pair(new_timeout, thread));
                return;
            }
            else // sleepticks >= timeout
            {
                // The next node after head
                auto next = head.next;

                u32int remaining_sleep_ticks = sleep_ticks;
                while(remaining_sleep_ticks >= timeout)
                {
                    // If we're at the end of the queue
                    if(head.next == nullptr)
                    {
                        // Simply insert us
                        insert(head, Pair(remaining_sleep_ticks, thread));
                        return;
                    }
                    // Oterwise reduce
                    remaining_sleep_ticks = remaining_sleep_ticks - timeout;
                    
                    head = next;
                    next = next.next;
                    timeout = next.getFirst();
                }
                // We are inbetween two nodes
                auto node = insert(head, Pair(remaining_sleep_ticks, thread));
                u32int new_timeout = timeout-sleep_ticks;
                waiting_queue.insert(node, Pair(new_timeout, thread));
            }

            insert(Pair(sleep_ticks, thread));
        }

        void sleep(registers_t *regs)
        {
            // Find out the sleep time
            u32int sleep_ticks = regs->eax;
            // Get the current thread
            TCB* thread = Scheduler::getCurrentThread();
            // Put it to sleep
            thread.setBlocked();
            // Add to the correct position within the queue
            add_to_waiting_queue(sleep_ticks, thread);
        }

        void tick(registers_t *regs)
        {
            Pair<u32int, TCB*> head = waiting_queue.get();
            u32int timeout = head.getFirst();
            TCB* thread = head.getSecond();
            // Decrease with 1
            timeout--;
            // Set all the ones done waiting as runnable
            while(timeout == 0)
            {
                thread->setRunnable();
                // Fetch next one
                head = waiting_queue.get();
                timeout = head.getFirst();
                thread = head.getSecond();
            }
            // Push the last one back
            waiting_queue.push_front(Pair(timeout, thread));
        }

        // Delay, Thread
        FIFO<Pair<u32int, TCB*>> waiting_queue;
};
