#ifndef _OSDEV_VFS_FILESYSTEM_HPP
#define _OSDEV_VFS_FILESYSTEM_HPP

#include <misc/common.hpp>
#include <misc/datastructures/String.hpp>

// struct for node types
enum FileNode_Flags_struct
{
    File,
    Directory,
    CharDevice/*,
    BlockDevice,
    Pipe,
    Symlink,
    Mountpoint,
    Mountpoint_Directory
    */
};
typedef enum FileNode_Flags_struct FileNode_Flags;

// struct for node permissions
class FileNode_Permission_Flags_struct
{
    //---------
    //Functions
    public:
        FileNode_Permission_Flags_struct(u16int raw_init)
        : raw(raw_init)
        {
        }
    private:
    protected:
    //---------
    //Variables
    public:
        union
        {
            u16int raw;
            struct
            {
                // User
                bool UserRead : 1;
                bool UserWrite : 1;
                bool UserExecute : 1;
                // Group
                bool GroupRead : 1;
                bool GroupWrite : 1;
                bool GroupExecute : 1;
                // Other
                bool OtherRead : 1;
                bool OtherWrite : 1;
                bool OtherExecute : 1;
            };
        };
    private:
    protected:
};

typedef struct FileNode_Permission_Flags_struct FileNode_Permission_Flags;

// Type of user id
typedef u32int uid_t;

// Type of group id
typedef u32int gid_t;

// Type of inode
typedef u32int inode_t;

struct dirent_struct
{
    // Filename
    String name;
    // Inode number.
    inode_t ino;
};

typedef struct dirent_struct dirent_t;

// FileNode Class
class FileNode
{
    //---------
    //Functions
    public:
        FileNode(String name, FileNode_Permission_Flags permissions,
                 uid_t uid, gid_t gid, FileNode_Flags node_type);

        // Get the name
        String getName() const;
        // Get inode number
        inode_t getInode() const;
        // Get node type
        FileNode_Flags getNodeType() const;

        // Virtual Functions
        virtual void open(u8int read, u8int write);
        virtual void close();
        u32int read(u32int offset, u32int size, u8int *buffer);
        u32int write(u32int offset, u32int size, u8int *buffer);
        dirent_t* readdir(u32int index);
        FileNode* finddir(String name);
    protected:
        virtual u32int read_impl(u32int offset, u32int size, u8int *buffer);
        virtual u32int write_impl(u32int offset, u32int size, u8int *buffer);
        virtual dirent_t* readdir_impl(u32int index);
        virtual FileNode* finddir_impl(String name);
    //---------
    //Variables
    public:
    private:
    protected:
        // Filename
        String name;
        // Permissions mask
        FileNode_Permission_Flags permissions;
        // Owning User
        uid_t uid;
        // Owning Group
        gid_t gid;
        // Node Type
        FileNode_Flags node_type;
        // Inode number.
        inode_t ino;
        // File Length
        u32int length;
        // An implementation-defined number.
        u32int free_impl;
        // Pointer used by mountpoints and symlinks
        FileNode *ptr;
};

#endif //_OSDEV_VFS_FILESYSTEM_HPP
