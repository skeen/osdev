#include "Folder.hpp"

FolderNode::FolderNode(String name)
    : FileNode(name, FileNode_Permission_Flags(0777),
            0, 0, FileNode_Flags_struct::Directory)
{
}

bool FolderNode::addNode(FileNode *node)
{
    FileNode* filenode = finddir_impl(node->getName());
    if(filenode == nullptr)
    {
        nodes.push_back(node);
        return true;
    }
    else
    {
        return false;
    }
}

bool FolderNode::removeNode(FileNode *node)
{
    for(u32int x=0; x<nodes.size(); x++)
    {
        FileNode* filenode = nodes[x];
        if(filenode == node)
        {
            nodes.erase(nodes.begin() + x);
            return true;
        }
    }
    return false;
}

dirent_t* FolderNode::readdir_impl(u32int index)
{
    if (index == 0)
    {
        dirent.name = getName();
        dirent.ino = 0;
        return &dirent;
    }

    if (index-1 >= nodes.size())
    {
        return nullptr;
    }

    dirent.name = nodes[index-1]->getName();
    dirent.ino = nodes[index-1]->getInode();

    return &dirent;    
}

FileNode* FolderNode::finddir_impl(String name)
{
    for (u32int i = 0; i < nodes.size(); i++)
    {
        if (name == nodes[i]->getName())
        {
            return nodes[i];
        }
    }
    return nullptr;
}
