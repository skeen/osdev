#ifndef _OSDEV_VFS_CDEV_FILENODE_HPP
#define _OSDEV_VFS_CDEV_FILENODE_HPP 

#include "FileSystem.hpp"

class CharacterDeviceFileNode : public FileNode
{
    public:
        CharacterDeviceFileNode(String device_name)
            : FileNode(device_name, FileNode_Permission_Flags(0777),
                       0, 0, FileNode_Flags_struct::CharDevice)
        {
        }
};

#endif //_OSDEV_VFS_CDEV_FILENODE_HPP
