#ifndef _OSDEV_ROOT_FILESYSTEM_HPP
#define _OSDEV_ROOT_FILESYSTEM_HPP

#include "Folder.hpp"

#include <misc/datastructures/Vector.hpp>

class RootFileNode : public FolderNode
{
    //---------
    //Functions
    public:
        // Create the default root file node
        RootFileNode()
            : FolderNode("/")
        {
        }

    private:
    protected:
    //---------
    //Variables
    public:
    private:
    protected:
};

#endif //_OSDEV_NODE_FILESYSTEM_HPP
