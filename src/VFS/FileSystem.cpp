#include "FileSystem.hpp"

FileNode::FileNode(String name, FileNode_Permission_Flags permissions,
                 uid_t uid, gid_t gid, FileNode_Flags node_type)
    : name(name), permissions(permissions), uid(uid), gid(gid), node_type(node_type),
      ino(0), length(0), free_impl(0), ptr(nullptr)
{
}

String FileNode::getName() const
{
    return name;
}

inode_t FileNode::getInode() const
{
    return ino;
}

FileNode_Flags FileNode::getNodeType() const
{
    return node_type;
}

void FileNode::open(u8int read, u8int write)
{
}

void FileNode::close()
{
}

u32int FileNode::read(u32int offset, u32int size, u8int *buffer)
{
    return read_impl(offset, size, buffer);
}

u32int FileNode::read_impl(u32int offset, u32int size, u8int *buffer)
{
    return 0;
}

u32int FileNode::write(u32int offset, u32int size, u8int *buffer)
{
    return write_impl(offset, size, buffer);
}

u32int FileNode::write_impl(u32int offset, u32int size, u8int *buffer)
{
    return 0;
}

dirent_t* FileNode::readdir(u32int index)
{
    if(node_type == FileNode_Flags::Directory)
    {
        return readdir_impl(index);
    }
    else
    {
        return 0;
    }
}

dirent_t* FileNode::readdir_impl(u32int index)
{
    return 0;
}

FileNode* FileNode::finddir(String name)
{
    if(node_type == FileNode_Flags::Directory)
    {
        return finddir_impl(name);
    }
    else
    {
        return 0;
    }
}

FileNode* FileNode::finddir_impl(String name)
{
    return 0;
}
