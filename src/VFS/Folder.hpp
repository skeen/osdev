#ifndef _OSDEV_VFS_FOLDERNODE_HPP
#define _OSDEV_VFS_FOLDERNODE_HPP

#include "FileSystem.hpp"

#include <misc/datastructures/Vector.hpp>

class FolderNode : public FileNode
{
    //---------
    //Functions
    public:
        // Create the default root file node
        FolderNode(String name);

        bool addNode(FileNode *node);
        bool removeNode(FileNode *node);
    private:
        virtual dirent_t* readdir_impl(u32int index);
        virtual FileNode* finddir_impl(String name);
    protected:
    //---------
    //Variables
    public:
    private:
        dirent_t dirent;
        Vector<FileNode*> nodes;
    protected:
};

#endif //_OSDEV_VFS_FOLDERNODE_HPP
