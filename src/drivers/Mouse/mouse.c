/**
 * @file	mouse.c
 * @author  Emil Madsen <skeen@cs.au.dk>
 * @version 1.0
 *
 * @section LICENSE
 *
 * Public Domain.
 *
 * @section DESCRIPTION
 *
 * Implements all of the functionality described in mouse.h
 */

#include <mouse.h>

#include <types.h>
#include <isr.h>
#include <monitor.h>

u8int mouse_cycle=0;
u8int mouse_byte[3];

mouse_movement movement;

// Doxygen in mouse.h
mouse_movement mouse_get_movement()
{
	return movement;
}

void mouse_callback(registers_t *regs)
{
	// Hardware sanity checks
	u8int control_byte = inb(0x64);
	if(!CHECK_BIT(control_byte, 0))
	{
		// Data not ready!
		monitor_write("not ready\n");
		return;
	}
	if(!CHECK_BIT(control_byte, 5))
	{
		// Data not from mouse!
		monitor_write("not from mouse\n");
		return;
	}

	// Hardware is sane, read a byte.
	mouse_byte[mouse_cycle++] = inb(0x60);

	if (mouse_cycle == 3) 
	{ 
		mouse_cycle = 0;
		// Load mouse_byte 0 as mouse_flags
		mouse_flags mf = *(mouse_flags*) &mouse_byte[0];

		// Sanity check mouse_flags
		if(!mf.true_bit)
		{
			monitor_write("bad mouse flags\n");
			return;
		}

		// If overflow throw away bytes
		if(mf.X_overflow || mf.Y_overflow)
		{
			monitor_write("mouse overflow\n");
			return;
		}
	
		// Handle mouse buttons	
		if(mf.left_btn)
		{
			monitor_write("left_btn\n");
		}
		if(mf.middle_btn)
		{
			monitor_write("middle_btn\n");
		}
		if(mf.right_btn)
		{
			monitor_write("right_btn\n");
		}
		
		// Handle mouse movement
		// Load movement
		movement.x = mouse_byte[1];
		movement.y = mouse_byte[2];
		
		// Handle sign_bits	
		if(mf.X_sign_bit)
		{
			movement.x = movement.x * -1;
		}
		if(mf.Y_sign_bit)
		{
			movement.y = movement.y * -1;
		}

		// If no movement, this was simply a button_down
		if(movement.x==0 && movement.y==0)
		{
			monitor_write("No movement\n");
		}
		else // Otherwise there was actually movement
		{
			/*
			monitor_write("x=");
			monitor_write_dec(movement.x);
			monitor_write("\t");
			monitor_write("y=");
			monitor_write_dec(movement.y);
			monitor_write("\n");
			*/
		}
	}
}

inline void mouse_wait(u8int a_type) //unsigned char
{
	u32int _time_out=100000; //unsigned int
	if(a_type==0)
	{
		while(_time_out--) //Data
		{
			if((inb(0x64) & 1)==1)
			{
				return;
			}
		}
		return;
	}
	else
	{
		while(_time_out--) //Signal
		{
			if((inb(0x64) & 2)==0)
			{
				return;
			}
		}
		return;
	}
}

inline void mouse_write(u8int a_write) //unsigned char
{
	//Wait to be able to send a command
	mouse_wait(1);
	//Tell the mouse we are sending a command
	outb(0x64, 0xD4);
	//Wait for the final part
	mouse_wait(1);
	//Finally write
	outb(0x60, a_write);
}

u8int mouse_read()
{
	//Get's response from mouse
	mouse_wait(0); 
	return inb(0x60);
}

// Doxygen in mouse.h
void mouse_initialize()
{
	movement.x = 0;
	movement.y = 0;

	u8int _status;  //unsigned char

	//Enable the auxiliary mouse device
	mouse_wait(1);
	outb(0x64, 0xA8);

	//Enable the interrupts
	mouse_wait(1);
	outb(0x64, 0x20);
	mouse_wait(0);
	_status=(inb(0x60) | 2);
	mouse_wait(1);
	outb(0x64, 0x60);
	mouse_wait(1);
	outb(0x60, _status);

	//Tell the mouse to use default settings
	mouse_write(0xF6);
	mouse_read();  //Acknowledge

	//Enable the mouse
	mouse_write(0xF4);
	mouse_read();  //Acknowledge

	//Setup the mouse handler
	register_interrupt_handler(IRQ12, &mouse_callback);
}
