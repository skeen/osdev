/**
 * @file	mouse.h
 * @author  Emil Madsen <skeen@cs.au.dk>
 * @version 1.0
 *
 * @section LICENSE
 *
 * Public Domain.
 *
 * @section DESCRIPTION
 *
 * Defines a simple interface, for interacting with the mouse.
 */

#ifndef _OSDEV_MOUSE_H
#define _OSDEV_MOUSE_H

// TODO: Document several functions using info from:
//		 http://wiki.osdev.org/Mouse_Input

#include <types.h>

/**
 * The mouse flags (mouse byte 0) structure.
 */
typedef struct
{
	/** Indicates whether left mouse button is down */
	bool left_btn	: 1;
	
	/** Indicates whether right mouse button is down */
	bool right_btn	: 1;
	
	/** Indicates whether middle mouse button is down */
	bool middle_btn : 1;

	/** This bit should always be true, can be used for sanity checking */
	bool true_bit 	: 1;
	
	/** Indicates whether the X movement is negative, needed as the movement is unsigned */
	bool X_sign_bit : 1;
	
	/** Indicates whether the Y movement is negative, needed as the movement is unsigned */
	bool Y_sign_bit : 1;
	
	/** Indicates whether the X movement has overflows */
	bool X_overflow : 1;
	
	/** Indicates whether the Y movement has overflows */
	bool Y_overflow : 1;
} mouse_flags;

/**
 * A structure to represent mouse movement.
 */
typedef struct
{
	/** x movement */
	s16int x;
	/** y movement */
	s16int y;
} mouse_movement;

/**
 * Sets up the mouse, for usage.
 * Must be called before any other mouse_* functions.
 *
 * @return 			void	Returns void.
 */
void mouse_initialize();

/**
 * Gets the last read mouse movement
 *
 * @return 			mouse_movement	Returns the mouse movement.
 */
mouse_movement mouse_get_movement();

#endif //_OSDEV_MOUSE_H
