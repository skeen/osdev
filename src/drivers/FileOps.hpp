struct file_operations
{
    int (*read)(char*,int);
    int (*write)(const char*,int);
    int (*open)();
    int (*release)();
};

#define MODULE_INIT(x)
#define MODULE_EXIT(x)

int getUniqueID()
{
    static int unique_id = 0;
    return unique_id++;
}

const int UNSUPPORTED_FUNCTIONALITY = 1;

int install_irq_handler(int unique_id, int irq, void (*handler)(struct regs *r))
{
    return 0;
}

int uninstall_irq_handler(int unique_id, int irq)
{
    return 0;
}
