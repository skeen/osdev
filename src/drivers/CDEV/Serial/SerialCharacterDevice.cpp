#include "Serial.hpp"

Serial::Serial(u8int port_id)
{
    switch(port_id)
    {
        case 1:
            PORT = COM1_PORT;
            break;
        case 2:
            PORT = COM2_PORT;
            break;
        case 3:
            PORT = COM3_PORT;
            break;
        case 4:
            PORT = COM4_PORT;
            break;
        default:
            PANIC("UNABLE TO TO CONNECT TO NON_EXISTING SERIAL PORT");
            break;
    }
    // Disable all interrupts
    Hardware::outb(PORT + 1, 0x00);
    // Enable DLAB (set baud rate divisor)
    Hardware::outb(PORT + 3, 0x80);    
    // Set divisor to 3 (lo byte) 38400 baud
    Hardware::outb(PORT + 0, 0x03);    
    //                  (hi byte)
    Hardware::outb(PORT + 1, 0x00);    
    // 8 bits, no parity, one stop bit    
    Hardware::outb(PORT + 3, 0x03);
    // Enable FIFO, clear them, with 14-byte threshold
    Hardware::outb(PORT + 2, 0xC7);    
    // IRQs enabled, RTS/DSR set
    Hardware::outb(PORT + 4, 0x0B);    

}

char Serial::read()
{
    while (serial_received() == 0);

    return Hardware::inb(PORT);
}

void Serial::write(char a)
{
    while (is_transmit_empty() == 0);

    Hardware::outb(PORT,a);
}

void Serial::write(const char* str)
{
    int x=0;
    while(str[x] != '\0')
    {
        write(str[x]);
        x = x + 1;
    }
}

void Serial::write(String str)
{
    for(u32int x=0; x<str.length(); x++)
    {
        write(str.at(x));
    }
}

u8int Serial::is_transmit_empty()
{
    return Hardware::inb(PORT + 5) & 0x20;
}

u8int Serial::serial_received()
{
    return Hardware::inb(PORT + 5) & 1;
}
