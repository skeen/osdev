#ifndef _OSDEV_DRIVER_SERIAL_HPP
#define _OSDEV_DRIVER_SERIAL_HPP

#include <misc/common.hpp>
#include <misc/datastructures/String.hpp>

class Serial
{
    //---------
    //Functions
    public:
        Serial(u8int port_id);

        char read();
        void write(char a);
        void write(const char* str);
        void write(String str);
    private:
        u8int is_transmit_empty();
        u8int serial_received();
    //---------
    //Variables
    public:
    private:
        // COM port addresses
        const int COM1_PORT = 0x3F8;
        const int COM2_PORT = 0x2F8;
        const int COM3_PORT = 0x3E8;
        const int COM4_PORT = 0x2E8;
        // This classes COM port
        // Assigned in constructor, and shouldn't be reassigned ever
        u16int PORT;
};

#endif //_OSDEV_DRIVER_SERIAL_HPP
