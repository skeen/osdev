#ifndef _OSDEV_DRIVER_SERIAL_CDEV_HPP
#define _OSDEV_DRIVER_SERIAL_CDEV_HPP

#include "Serial.hpp"

#include <misc/common.hpp>

#include <VFS/CharacterDeviceFileNode.hpp>

class SerialCharacterDevice : public CharacterDeviceFileNode
{
    public:
        SerialCharacterDevice(u8int port_id)
            : CharacterDeviceFileNode("Serial"), s(port_id)
        {
        }

        u32int read_impl(u32int offset, u32int size, u8int *buffer)
        {
            buffer[0] = s.read();
            return 1;
        }

        u32int write_impl(u32int offset, u32int size, u8int *buffer)
        {
            for(u32int x=0; x<size; x++)
            {
                char c = buffer[x];
                s.write(c);
            }
            return size;
        }
    private:
        Serial s;

};

#endif //_OSDEV_DRIVER_SERIAL_CDEV_HPP
