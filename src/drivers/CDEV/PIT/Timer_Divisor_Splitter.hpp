#ifndef _OSDEV_TIMER_DIVISOR_SPLITTER_H
#define _OSDEV_TIMER_DIVISOR_SPLITTER_H

#include <misc/common.hpp>
#include <misc/compiler.h>

class Timer_Divisor_Splitter
{
    //----------
    // Datatypes
    public:
    private:
        struct splitter
        {
            u8int low;
            u8int high;
        };
        union base
        {
            u16int raw;
            splitter split;
        };
    protected:
    //----------
    // Functions
    public:
        Timer_Divisor_Splitter(u16int raw);

        u16int getLow();
        u16int getHigh();
    private:
    protected:

    //----------
    // Variables
    public:
    private:
        base data;
    protected:
};

#endif //_OSDEV_TIMER_DIVISOR_SPLITTER_H
