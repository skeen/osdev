#ifndef _OSDEV_DRIVER_PIT_CDEV_HPP
#define _OSDEV_DRIVER_PIT_CDEV_HPP

#include <misc/common.hpp>
#include <misc/datastructures/Delegate.hpp>
#include <misc/datastructures/Pair.hpp>

#include <VFS/CharacterDeviceFileNode.hpp>

#include <tables/InterruptServiceRoutine.h>

class PITCharacterDevice : public CharacterDeviceFileNode
{
    public:
        PITCharacterDevice();
        PITCharacterDevice(u32int frequency);

        Pair<u8int, Runnable<void, registers_t>*> getPITInterruptHandler();

        // Non-blocking read
        u32int read_impl(u32int offset, u32int size, u8int *buffer);
        u32int write_impl(u32int offset, u32int size, u8int *buffer);
    private:
        u32int ticks;
        u32int frequency;

        void pit_interrupt_handler(registers_t regs);
        void set_timer_frequency(u32int frequency);
};

#endif //_OSDEV_DRIVER_KEYBOARD_CDEV_HPP
