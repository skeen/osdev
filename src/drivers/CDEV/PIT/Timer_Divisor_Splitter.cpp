#include "Timer_Divisor_Splitter.hpp"

Timer_Divisor_Splitter::Timer_Divisor_Splitter(u16int raw)
{
    data.raw = raw;
}

u16int Timer_Divisor_Splitter::getLow()
{
    return data.split.low;
}
u16int Timer_Divisor_Splitter::getHigh()
{
    return data.split.high;
}
