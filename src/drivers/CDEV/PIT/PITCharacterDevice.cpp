#include "PITCharacterDevice.hpp"

#include "Timer_Divisor_Splitter.hpp"

#include <misc/datastructures/String.hpp>

PITCharacterDevice::PITCharacterDevice()
    : CharacterDeviceFileNode("PIT"), ticks(0), frequency(50)
{
    set_timer_frequency(frequency);
}


PITCharacterDevice::PITCharacterDevice(u32int frequency)
    : CharacterDeviceFileNode("PIT"), ticks(0), frequency(frequency)
{
    set_timer_frequency(frequency);
}

Pair<u8int, Runnable<void, registers_t>*> PITCharacterDevice::getPITInterruptHandler()
{
    Runnable<void, registers_t> *handler = makeRunnable(*this, &PITCharacterDevice::pit_interrupt_handler);
    return Pair<u8int, Runnable<void, registers_t>*>(IRQ0, handler);
}

void PITCharacterDevice::pit_interrupt_handler(registers_t regs)
{
    ticks++;
}

u32int PITCharacterDevice::read_impl(u32int offset, u32int size, u8int *buffer)
{
    String info_string = "timer running at ";
    info_string.append(frequency);
    info_string.append(" HZ\n");
    info_string.append(ticks);
    info_string.append(" elapsed so far\n");

    for(u32int x=0; x<size; x++)
    {
        if(x == info_string.length())
        {
            return x;
        }
        buffer[x] = info_string.at(x);
    }
    return size;
}

u32int PITCharacterDevice::write_impl(u32int offset, u32int size, u8int *buffer)
{
    String s = "";
    for(u32int x=0; x<size; x++)
    {
        s.append((char) buffer[x]);
    }
    u32int frequency = s.parseInteger();
    set_timer_frequency(frequency);
    return size;
}

void PITCharacterDevice::set_timer_frequency(u32int frequency)
{
    // The value we send to the PIT is the value to divide it's input clock
    // (1193180 Hz) by, to get our required frequency. Important to note is
    // that the divisor must be small enough to fit into 16-bits.
    u16int divisor = 1193180 / frequency;

    // Send the command byte.
    Hardware::outb(0x43, 0x36);

    Timer_Divisor_Splitter splitter = Timer_Divisor_Splitter(divisor);

    // Divisor has to be sent byte-wise, so split here into upper/lower bytes.
    u8int l = splitter.getLow();
    u8int h = splitter.getHigh();

    // Send the frequency divisor.
    Hardware::outb(0x40, l);
    Hardware::outb(0x40, h);
} 
