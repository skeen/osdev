#ifndef _OSDEV_DRIVER_TIMER_CDEV_HPP
#define _OSDEV_DRIVER_TIMER_CDEV_HPP

#include <misc/common.hpp>

#include <VFS/CharacterDeviceFileNode.hpp>

class TimerCharacterDevice : public CharacterDeviceFileNode
{
    public:
        TimerCharacterDevice(u8int port_id)
            : CharacterDeviceFileNode("Timer")
        {
        }
    private:
};

#endif //_OSDEV_DRIVER_SERIAL_CDEV_HPP
