// The timer should hook the timer interrupt, in order to get ticks.
//
// It should block processes trying to write, for the amount of time specified
// in their write request (given in number of ticks).
// Once the requested time has elapsed, the write request should complete, and
// thereby return to the callee.
//
// Effective implementation of the timer system is based upon a linked list,
// wheres each node holds a semaphore to wake up (once the timer is completed)
// as well as the number of ticks till wakeup.
//
// For performence the following structure is used; each link doesn't contain
// it's own number of ticks, but instead it's delay after the following node.
//
// An example;
//  Three timers are set to 3,7 and 11 ticks, the naive approach would be the
//  following list; [3,7,11] (with their corresponding locks).
//  The approach used instead is the following; [3,4,4] (with their respective
//  locks). The idea is that we now only have to decrement the first element in
//  the list (and possibly fire it, whenever it reaches zero (also one should
//  check the following for 0 delay)).

/*
struct
{
    Semaphore sem;
    unsigned int delay;
} timer;

LinkedList<timer> timer_list;


void add_timer(timer)
{
    // Add to the right place, and figure out delay
    // timer_list.put_tail(timer);
}

void tick_timers()
{
    timer t = timer_list.get_head();
    t.delay--;
    timer_list.put_head(t);
    fire_timers();
}

void fire_timers()
{
    while(timer_list.has_more())
    {
        timer t = timer_list.get_head();
        if (t.delay == 0)
        {
            Semaphore sem = t.sem;
            sem.up();
        }
        else // More time needs to pass
        {
            break;
        }
    }
}
*/
