#ifndef _OSDEV_DRIVER_EMULATOR_SHUTDOWN_CDEV_HPP
#define _OSDEV_DRIVER_EMULATOR_SHUTDOWN_CDEV_HPP

#include "EmulatorShutdown.h"

#include <misc/common.hpp>

#include <VFS/CharacterDeviceFileNode.hpp>

class EmulatorShutdownCharacterDevice : public CharacterDeviceFileNode
{
    public:
        EmulatorShutdownCharacterDevice()
            : CharacterDeviceFileNode("EmulatorShutdown")
        {
        }

        u32int write_impl(u32int offset, u32int size, u8int *buffer)
        {
            if(buffer[0] == '1')
            {
                EmulatorShutdown();
            }
            return 1;
        }

};

#endif //_OSDEV_DRIVER_EMULATOR_SHUTDOWN_CDEV_HPP
