#include "EmulatorShutdown.h"

#include <misc/common.hpp>

void EmulatorShutdown()
{
    Hardware::outw( 0xB004, 0x0 | 0x2000);
}
