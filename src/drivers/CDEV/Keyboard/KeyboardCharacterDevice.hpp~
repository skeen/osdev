#ifndef _OSDEV_DRIVER_KEYBOARD_CDEV_HPP
#define _OSDEV_DRIVER_KEYBOARD_CDEV_HPP

#include <misc/common.hpp>
#include <misc/datastructures/StaticFIFO.hpp>
#include <misc/datastructures/Delegate.hpp>
#include <misc/datastructures/Pair.hpp>

#include <VFS/CharacterDeviceFileNode.hpp>

#include <tables/InterruptServiceRoutine.h>

#include <multitasking/Spinlock.hpp>

class KeyboardCharacterDevice : public CharacterDeviceFileNode
{
    public:
        KeyboardCharacterDevice();

        Pair<u8int, Runnable<void, registers_t>*> getKeyboardInterruptHandler();

        // Non-blocking read
        u32int read_impl(u32int offset, u32int size, u8int *buffer);
    private:
        void keyboard_interrupt_handler(registers_t regs);

        Spinlock s;
        StaticFIFO<unsigned char,256> device_buffer;
};

#endif //_OSDEV_DRIVER_KEYBOARD_CDEV_HPP
