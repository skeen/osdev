#include "KeyboardCharacterDevice.hpp"

KeyboardCharacterDevice::KeyboardCharacterDevice()
    : CharacterDeviceFileNode("Keyboard"), s(true)
{
}

Pair<u8int, Runnable<void, registers_t>*> KeyboardCharacterDevice::getKeyboardInterruptHandler()
{
    Runnable<void, registers_t> *handler = makeRunnable(*this, &KeyboardCharacterDevice::keyboard_interrupt_handler);
    return Pair<u8int, Runnable<void, registers_t>*>(IRQ1, handler);
}

// Non-blocking read
u32int KeyboardCharacterDevice::read_impl(u32int offset, u32int size, u8int *buffer)
{   
    /*
    Util::memset(buffer,0,size);

    u32int i;
    for(i=0; i<size; i++)
    {
        // Make sure theres more input left
        if(device_buffer.isEmpty())
        {
            break;
        }
        else // There's more input
        {
            unsigned char scancode = device_buffer.get();
            buffer[i] = scancode;
        }
    }
    */
    // If we're out of input, block and wait for some
    // TODO: Remove the spinlock
    while(device_buffer.isEmpty())
    {
        s.lock();
    }
    unsigned char scancode = device_buffer.get();
    buffer[0] = scancode;
    return 1;
}

void KeyboardCharacterDevice::keyboard_interrupt_handler(registers_t regs)
{
    const u16int keyboard_port = 0x60;
    /* Read from the keyboard's data buffer */
    unsigned char scancode = Hardware::inb(keyboard_port);

    // If the buffer is writeable (not full), write to it,
    // otherwise just ignore the scancode. Thereby loosing it.
    if (device_buffer.isFull() == false)
    {
        device_buffer.put(scancode);
    }
    
    s.unlock();
}
