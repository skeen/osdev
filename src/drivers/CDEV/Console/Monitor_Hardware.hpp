/**
 * @file	Monitor_Hardware.hpp
 * @author  Emil Madsen <skeen@cs.au.dk>
 * @version 1.0
 *
 * @section LICENSE
 *
 * Public Domain.
 *
 * @section DESCRIPTION
 *
 * Defines an implementation of the character printer interface, that prints to
 * the screen on the x86.
 */

#ifndef _OSDEV_MONITOR_HARDWARE_HPP
#define _OSDEV_MONITOR_HARDWARE_HPP

#include <misc/common.hpp>
#include "color_byte.hpp"

class Monitor_Hardware
{
    //----------
    // Datatypes
    public:
    private:
        /**
         * A structure to represent a colored character
         */
        typedef union
        {
            /** The format that the hardware expects */
            u16int color_char;
            /** The humanfriendly format */
            struct
            {
                u8int character;
                u8int color;
            };
        } color_character;

        /**
         * A structure to represent a point on the screen.
         */
        typedef struct
        {
            /** x coordinate */
            u8int x;
            /** y coordinate */
            u8int y;
        } coordinate;
    protected:
    //----------
    // Functions
    public:
        static Monitor_Hardware& instance()
        {
            static Monitor_Hardware s_instance;
            s_instance.initialize();
            return s_instance;
        }

        /**
         * Sets up the monitor for printing to the screen.
         *
         * @return 			void	Returns void.
         */
        void initialize();

        /**
         * Sets text color for the writing.
         *
         * @param[in]  color	color_byte	The wanted color byte.
         * @return 	   			void		Returns void.
         */
        void set_color(color_byte newcolor);
        /**
         * Gets the text color for the writing.
         *
         * @return 	   			color_byte	Returns the color_byte currently used for
         * writing.
         */
        color_byte get_color();
        /**
         * Write a single character out to the screen.
         *
         * @param[in]  c		char	The ASCII character to be printed.
         * @return 				void	Returns void.
         */
        virtual void put(const char c);
        /**
         * Clears the screen.
         *
         * @return 			void	Returns void.
         */
        virtual void clear();
        private:
        /**
         * Private constructor (use instance())
         */
        Monitor_Hardware() {}

        /**
         * Calculate the cursors pointer offset into the video memory
         * @return 				u16int	Returns the coords index.
         */
        u16int cursor_offset();

        /**
         * Updates the hardware cursor according to  the current software position
         *
         * @return 				void	Returns void.
         */
        void move_cursor();

        /**
         * Scrolls the text on the screen up by one line, if necessary.
         *
         * @return 				void	Returns void.
         */
        void scroll();

        /**
         * Detects monitor type (color/monochrome), using video mapped memory.
         *
         * @return 			bool	Returns false for color monitor and true for
         * monochrome.
         * Note; NOT USED
         */
        bool detect_type();

        /**
         * Generates the colored character code, based upon current color.
         *
         * @param[in]  c	char		    The character to be transformed.
         * @return 			color_character	Returns the generated colored character code.
         */
        color_character get_colored_char(char c);
    protected:
    //----------
    // Variables
    public:
    private:
        /** 
         * Pointer variable describing the VGA framebuffers memory mapped position.
         *
         * The framebuffer region starts at: 0xB8000 (for color monitors)
         *                                   0xB0000 (for monochrome) (not supported)
         * and stretches 4000bytes (25height*80width*2bytes).
         *
         * Note; The pointer is constant (pointed object is not)
         */
        static u16int* const video_memory;

        /** Variable describing if the object has been initialized yet */
        bool initialized = false;

        /** Variable describing the cursors position */
        coordinate cursor;

        /** Variable describing the current output color */
        color_byte color;

        /** constant describing the width resolution of the VGA framebuffer */
        static const u8int MONITOR_WIDTH     = 80;
        /** constant describing the height resolution of the VGA framebuffer */
        static const u8int MONITOR_HEIGHT    = 25;
        /** constant describing the tab width used */
        static const u8int MONITOR_TAB_WIDTH = 4;
    protected:
};

#endif //_OSDEV_MONITOR_HARDWARE_HPP
