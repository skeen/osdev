/**
 * @file	color_byte.hpp
 * @author  Emil Madsen <skeen@cs.au.dk>
 * @version 1.0
 *
 * @section LICENSE
 *
 * Public Domain.
 *
 * @section DESCRIPTION
 *
 * Defines a simple interface, for colors
 */

#ifndef _OSDEV_COLOR_BYTE_HPP
#define _OSDEV_COLOR_BYTE_HPP

#include <misc/common.hpp>

/**
 * A class used to represent the color_byte structure.
 * (used to set colors of the kernel monitor)
 */ 
class color_byte
{
    //----------
    // Datatypes
    public:
        /** enum with constants describing the color code for different colors */
        enum colorpart
        { 
            BLACK          = 0,
            BLUE           = 1,
            GREEN          = 2,
            CYAN           = 3,
            RED            = 4,
            MAGENTA        = 5,
            BROWN          = 6,
            LIGHT_GRAY     = 7,
            DARK_GRAY      = 8,
            LIGHT_BLUE     = 9,
            LIGHT_GREEN    = 10,
            LIGHT_CYAN     = 11,
            LIGHT_RED      = 12,
            LIGHT_MAGENTA  = 13,
            LIGHT_BROWN    = 14,
            WHITE          = 15
        };
    private:
    protected:
    
    //----------
    // Functions
    public:
        /**
         * The default constructor, sets up a color byte with;
         * white foreground color and black background color
         */
        color_byte();
        /**
         * This constructor loads a complete color byte
         *
         * @param[in]  color	u8int	The wanted color byte.
         */
        color_byte(u8int color);
        /**
         * This constructor build the colorbyte based upon the specified
         * foreground and background colorparts
         * 
         * @param[in]  foreground	colorpart/u8int	The wanted foreground color code.
         * @param[in]  background	colorpart/u8int	The wanted backgorund color code.
         */
        color_byte(enum color_byte::colorpart foreground, enum color_byte::colorpart background);
        color_byte(u8int foreground, u8int background);
        /**
         * Exports the internal represented color byte.
         *
         * @return 	   			u8int		Returns the u8int colorbyte.
         */
        u8int getColorByte();
        /**
         * Exports the internal represented foreground color code.
         *
         * @return 	   			colorpart		Returns the foreground color code.
         * Note; This value is implicit castable to u8int.
         */
        enum color_byte::colorpart getForegroundColor();
        /**
         * Exports the internal represented background color code.
         *
         * @return 	   			colorpart		Returns the background color code.
         * Note; This value is implicit castable to u8int.
         */
        enum color_byte::colorpart getBackgroundColor();
    private:
         /**
         * Sets the internal represented color byte, to the argument.
         * 
         * @param[in]  color	u8int	The wanted color byte.
         * @return 				void	Returns void.
         */
        void setColorByte(u8int color);
        /**
         * Sets the foreground colorpart of the color byte.
         * 
         * @param[in]  color    colorpart/u8int	The wanted foreground color code.
         * @return 				void	        Returns void.
         */
        void setForegroundColor(enum color_byte::colorpart color);
        void setForegroundColor(u8int color);
        /**
         * Sets the background colorpart of the color byte.
         * 
         *
         * @param[in]  color	colorpart/u8int	The wanted background color code.
         * @return 				void	        Returns void.
         */
        void setBackgroundColor(enum color_byte::colorpart color);
        void setBackgroundColor(u8int color);
    protected:
    //----------
    // Variables
    public:
    private:
        /** 
         * A union of the color byte and
         *  a struct representing its components
         */
        union
        {
            /** Variable describing the color byte */
            u8int color;
            /** Struct dividing the color byte into it's components */
            struct
            {
                /** The foreground color component (4bits) */
                enum color_byte::colorpart foreground : 4;
                /** The background color component (4bits)*/
                enum color_byte::colorpart background : 4;
            } colorcomponents;
            /** Struct dividing the color byte into it's components (with ints) */
            struct
            {
                /** The foreground color component (4bits) */
                u8int foreground : 4;
                /** The background color component (4bits)*/
                u8int background : 4;
            } colorcomponents_int;
        };
    protected:
};

#endif //_OSDEV_COLOR_BYTE_HPP
