#ifndef _OSDEV_DRIVER_CONSOLE_CDEV_HPP
#define _OSDEV_DRIVER_CONSOLE_CDEV_HPP

#include "Monitor_Hardware.hpp"

#include <misc/common.hpp>

#include <VFS/CharacterDeviceFileNode.hpp>

class ConsoleCharacterDevice : public CharacterDeviceFileNode
{
    public:
        ConsoleCharacterDevice()
            : CharacterDeviceFileNode("Console")
        {
             monitor = &(Monitor_Hardware::instance());
        }

        u32int write_impl(u32int offset, u32int size, u8int *buffer)
        {
            for(u32int x=0; x<size; x++)
            {
                char c = buffer[x];
                monitor->put(c);
            }
            return size;
        }
    private:
        Monitor_Hardware *monitor = nullptr;

};

#endif //_OSDEV_DRIVER_SERIAL_CDEV_HPP
