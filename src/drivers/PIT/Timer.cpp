#include "Timer.h"
#include "Timer_Divisor_Splitter.hpp"

#include <monitor/Printf.hpp>
#include <tables/InterruptServiceRoutine.h>

//#include <task.h>

u32int tick = 0;

void timer_callback(registers_t* regs)
{
    tick++;
    //printf("Tick: %d\n", tick);
    /*
    task_switch();
    */
}

void init_timer(u32int frequency)
{
    // Firstly, register our timer callback.
    //register_interrupt_handler(IRQ0, timer_callback);

    // The value we send to the PIT is the value to divide it's input clock
    // (1193180 Hz) by, to get our required frequency. Important to note is
    // that the divisor must be small enough to fit into 16-bits.
    u16int divisor = 1193180 / frequency;

    // Send the command byte.
    Hardware::outb(0x43, 0x36);

    Timer_Divisor_Splitter splitter = Timer_Divisor_Splitter(divisor);

    // Divisor has to be sent byte-wise, so split here into upper/lower bytes.
    u8int l = splitter.getLow();
    u8int h = splitter.getHigh();

    // Send the frequency divisor.
    Hardware::outb(0x40, l);
    Hardware::outb(0x40, h);
} 

