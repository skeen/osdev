#ifndef _OSDEV_TIMER_H
#define _OSDEV_TIMER_H
 
#include <misc/common.hpp>

void init_timer(u32int frequency);

#endif //_OSDEV_TIMER_H
