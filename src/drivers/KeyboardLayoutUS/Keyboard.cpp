#include <Drivers/FileOps.hpp>

#include <misc/common.hpp>

#include <KeyboardLayoutUS.hpp>

const int BUFFER_SIZE = 256;

unsigned char transformation_buffer[BUFFER_SIZE];

int console_read(char *buffer, int count);
int console_write(const char *ubuf, int count);
int console_open();
int console_release();

struct file_operations fops =
{
    .read    = console_read,
    .write   = console_write,
    .open    = console_open,
    .release = console_release
};

int console_read(char *buffer, int count)
{
    int i;
    for(i = 0; i<count; i++)
    {
        unsigned char scancode = transformation_buffer[i];
        /* If the top bit of the byte we read from the keyboard is
         *  set, that means that a key has just been released */
        if (scancode & 0x80)
        {
            /* You can use this one to see if the user released the
             *  shift, alt, or control keys... */
            continue;
        }
        else
        {
            /* Here, a key was just pressed. Please note that if you
             *  hold a key down, you will get repeated key press
             *  interrupts. */

            /* Just to show you how this works, we simply translate
             *  the keyboard scancode into an ASCII value, and then
             *  display it to the screen. You can get creative and
             *  use some flags to see if a shift is pressed and use a
             *  different layout, or you can add another 128 entries
             *  to the above layout to correspond to 'shift' being
             *  held. If shift is held using the larger lookup table,
             *  you would add 128 to the scancode when you look for it */
            buffer[i] = kbdus[scancode];
        }
    }

    return i;
}

int console_write(const char *buffer, int count)
{
    // Clear the buffer
    Util::memset(transformation_buffer,0,BUFFER_SIZE);
    // Check for overflow
    if (count > BUFFER_SIZE)
    {
        Util::memcpy(transformation_buffer,buffer,BUFFER_SIZE);
        return BUFFER_SIZE;
    }
    else
    {
        Util::memcpy(transformation_buffer,buffer,count);
        return count;
    }
}

int console_open()
{
   return 0;
}

int console_release()
{
   return 0;
}

void module_init()
{
    Util::memset(transformation_buffer,0,BUFFER_SIZE);
}

void module_exit()
{
}

MODULE_INIT(module_init);
MODULE_EXIT(module_exit);
