/**
 * @file	isr.h
 * @author  Emil Madsen <skeen@cs.au.dk>
 * @version 1.0
 *
 * @section LICENSE
 *
 * Public Domain.
 *
 * @section DESCRIPTION
 *
 * Defines the interface and structures for high level interrupt service
 * routines (isr).
 */

#ifndef _OSDEV_TABLES_INTERRUPT_SERVICE_ROUTINE_H
#define _OSDEV_TABLES_INTERRUPT_SERVICE_ROUTINE_H

#include <misc/common.hpp>
#include <misc/compiler.h>

#include <misc/datastructures/Vector.hpp>
#include <misc/datastructures/Pair.hpp>
#include <misc/datastructures/Delegate.hpp>

typedef struct
{
    /** Data segment selector */
    u32int ds;
    /** Registers pushed by the 'pusha' instruction */
    u32int edi, esi, ebp, esp, ebx, edx, ecx, eax;
    /** Interrupt number and error code (if any) */
    u32int interrupt_number, error_code;
    /** Registers and such, that are automatically pushed by the CPU */
    u32int eip, cs, eflags, useresp, ss;
} PACKED registers_t;

static_assert(sizeof(registers_t)==16*4,  "Incorrect size of type");

ASM_LINK void isr_handler(registers_t *regs);
ASM_LINK void irq_handler(registers_t *regs);

// A few defines to make life a little easier
const u8int IRQ0  = 32;
const u8int IRQ1  = 33;
const u8int IRQ2  = 34;
const u8int IRQ3  = 35;
const u8int IRQ4  = 36;
const u8int IRQ5  = 37;
const u8int IRQ6  = 38;
const u8int IRQ7  = 39;
const u8int IRQ8  = 40;
const u8int IRQ9  = 41;
const u8int IRQ10 = 42;
const u8int IRQ11 = 43;
const u8int IRQ12 = 44;
const u8int IRQ13 = 45;
const u8int IRQ14 = 46;
const u8int IRQ15 = 47;

class InterruptService
{
    public:
        void interrupt(registers_t *regs)
        {
            u32int interrupt_number = regs->interrupt_number;
            for(u32int x=0; x<handlers[interrupt_number].size(); x++)
            {
                Runnable<void, registers_t*> *run = handlers[interrupt_number].at(x);
                run->invoke(regs);
            }
        }

        // Return the number of handlers for a given error
        u32int size(u8int index)
        {
            return handlers[index].size();
        }

        void seedHandler(Pair<u8int, Runnable<void, registers_t*>*> p)
        {
            u32int index = p.getFirst();
            Runnable<void, registers_t*>* run = p.getSecond();

            seedHandler(index, run);
        }

        void seedHandler(u8int index, Runnable<void, registers_t*>* run)
        {
            handlers[index].push_back(run);
        }
        
    private:
        Vector<Runnable<void, registers_t*>*> handlers[256];
};

extern InterruptService* service;

void seedInterruptService(InterruptService* service_init);

#endif //_OSDEV_TABLES_INTERRUPT_SERVICE_ROUTINE_H
