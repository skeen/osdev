#include "GlobalDescriptorTable.hpp"

GDT_Granularity_Splitter::GDT_Granularity_Splitter(u32int raw)
{
    data.raw = raw;
}

GDT_Granularity_Splitter::GDT_Granularity_Splitter(bool granularity, bool operand_size)
{
    data.split.G = granularity;
    data.split.D = operand_size;
    data.split.ZERO = 0;
    data.split.A = 0;

    data.split.pad = 0xF;
}

u8int GDT_Granularity_Splitter::getRaw()
{
    return data.raw;
}
