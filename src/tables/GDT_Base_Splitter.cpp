#include "GlobalDescriptorTable.hpp"

GDT_Base_Splitter::GDT_Base_Splitter(u32int raw)
{
    data.raw = raw;
}

u16int GDT_Base_Splitter::getLow()
{
    return data.split.low;
}

u8int GDT_Base_Splitter::getMiddle()
{
    return data.split.middle;
}

u8int GDT_Base_Splitter::getHigh()
{
    return data.split.high;
}
