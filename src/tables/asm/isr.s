/**
 * @file	isr.s
 * @author  Emil Madsen <skeen@cs.au.dk>
 * @version 1.0
 *
 * @section LICENSE
 *
 * Public Domain.
 *
 * @section DESCRIPTION
 *
 * Contains the interrupt service routine wrappers.
 * Handles the interrupt --> C handler assembly
 */

# This macro creates a stub for an ISR which does NOT pass it's own
# error code (adds a dummy errcode byte).
.macro ISR_NOERRCODE interrupt_no
    .global isr\interrupt_no        # Make a global entry point
    isr\interrupt_no:              
        cli                         # Disable interrupts
        pushl $0x0                  # Push a dummy error code
        pushl $\interrupt_no        # Push the interrupt number
        jmp isr_common_stub         # Jump to the common isr handler
.endm

# This macro creates a stub for an ISR which passes it's own error code.
.macro ISR_ERRCODE interrupt_no
    .global isr\interrupt_no
    isr\interrupt_no:               # Make a global entry point
        cli                         # Disable interrupts
        # Interrupt number already pushed by CPU
        pushl $\interrupt_no        # Push the interrupt number
        jmp isr_common_stub         # Jump to the common isr handler
.endm

# Use the above macros to generate isr handlers
ISR_NOERRCODE 0
ISR_NOERRCODE 1
ISR_NOERRCODE 2
ISR_NOERRCODE 3
ISR_NOERRCODE 4
ISR_NOERRCODE 5
ISR_NOERRCODE 6
ISR_NOERRCODE 7
ISR_ERRCODE   8
ISR_NOERRCODE 9
ISR_ERRCODE   10
ISR_ERRCODE   11
ISR_ERRCODE   12
ISR_ERRCODE   13
ISR_ERRCODE   14
ISR_NOERRCODE 15
ISR_NOERRCODE 16
ISR_NOERRCODE 17
ISR_NOERRCODE 18
ISR_NOERRCODE 19
ISR_NOERRCODE 20
ISR_NOERRCODE 21
ISR_NOERRCODE 22
ISR_NOERRCODE 23
ISR_NOERRCODE 24
ISR_NOERRCODE 25
ISR_NOERRCODE 26
ISR_NOERRCODE 27
ISR_NOERRCODE 28
ISR_NOERRCODE 29
ISR_NOERRCODE 30
ISR_NOERRCODE 31

# This is our common ISR stub. It saves the processor state, sets
# up for kernel mode segments, calls the C-level fault handler,
# and finally restores the stack frame.
isr_common_stub:
    # Pushes edi,esi,ebp,esp,ebx,edx,ecx,eax
    pusha                   

    # ds = 16lower bits of eax
    xor %eax, %eax
    mov %ds, %ax     
    # Saves the data segment descriptor (eax-->ax-->ds)    
    push %eax                 

    # Load the kernel data segment descriptor
    mov $0x10, %ax   
    mov %ax, %ds 
    mov %ax, %es
    mov %ax, %fs
    mov %ax, %gs

    # Call our isr handler (C code)
    pushl %esp
    call isr_handler
    addl $4, %esp

    # Reload the original data segment descriptor
    pop %ebx       
    mov %bx, %ds 
    mov %bx, %es 
    mov %bx, %fs 
    mov %bx, %gs

    # Pops all the registers we saved in the start
    popa                     
    # Clears the stack of the pushed arguments (in the assembly macro above)
    # (Error code and pushed ISR number) 
    # This is a quicker form, instead of:
    # popl %eax
    # popl %eax
    add $0x8, %esp

    # Reenables interrupts
    sti
    # Returns (pops 5 things at once; CS, EIP, EFLAGS, SS, and ESP)
    iret 
