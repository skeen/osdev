/**
 * @file	gdt.s
 * @author  Emil Madsen <skeen@cs.au.dk>
 * @version 1.0
 *
 * @section LICENSE
 *
 * Public Domain.
 *
 * @section DESCRIPTION
 *
 * Contains the setup code for the global descriptor table
 */

# Export this function, as it needs to be called from C code (gdt_flush())
.global gdt_flush

gdt_flush:
    # Get the pointer to the GDT, passed as a parameter.
    mov 0x4(%esp), %eax 
    # Load the new GDT pointer
    lgdtl (%eax)       

    # 0x10 is the offset in the GDT to our data segment
    mov $0x10, %ax
    # Load all data segment selectors
    mov %ax, %ds         
    mov %ax, %es 
    mov %ax, %fs 
    mov %ax, %gs
    mov %ax, %ss 

    # 0x08 is the offset to our code segment: Far jump!
    ljmp $0x8, $gdt_flush.flush

gdt_flush.flush:
    ret
