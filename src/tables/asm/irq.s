/**
 * @file	irq.s
 * @author  Emil Madsen <skeen@cs.au.dk>
 * @version 1.0
 *
 * @section LICENSE
 *
 * Public Domain.
 *
 * @section DESCRIPTION
 *
 * Contains the interrupt request routine wrappers.
 * Handles the interrupt --> C handler assembly
 */

# This macro creates a stub for an ISR which does NOT pass it's own
# error code (adds a dummy errcode byte).
.macro IRQ interrupt_no remap
    .global irq\interrupt_no        # Make a global entry point
    irq\interrupt_no:              
        cli                         # Disable interrupts
        pushl $0x0                  # Push a dummy error code
        pushl $\remap               # Push the remap number
        jmp irq_common_stub         # Jump to the common isr handler
.endm

# Use the above macros to generate irq handlers
IRQ   0,    32
IRQ   1,    33
IRQ   2,    34
IRQ   3,    35
IRQ   4,    36
IRQ   5,    37
IRQ   6,    38
IRQ   7,    39
IRQ   8,    40
IRQ   9,    41
IRQ  10,    42
IRQ  11,    43
IRQ  12,    44
IRQ  13,    45
IRQ  14,    46
IRQ  15,    47

IRQ  100,    100

# This is our common IRQ stub. It saves the processor state, sets
# up for kernel mode segments, calls the C-level fault handler,
# and finally restores the stack frame.
# This is our common IRQ stub. It saves the processor state, sets
# up for kernel mode segments, calls the C-level fault handler,
# and finally restores the stack frame.
irq_common_stub:
    jmp fixup_call_stack
fixup_call_stack_complete:

#TODO: Use pushad & popad
    # Pushes edi,esi,ebp,esp,ebx,edx,ecx,eax
    # pushad
    pushl %eax
	pushl %ecx
	pushl %edx
	pushl %ebx
	pushl %esp
	pushl %ebp
	pushl %esi
	pushl %edi                  

    # ds = 16lower bits of eax
    movl %ds, %ebp     
    # Saves the data segment descriptor (eax-->ax-->ds)    
    pushl %ebp                 

    # Load the kernel data segment descriptor
    movw $0x10, %bp   
    movl %ebp, %ds 
    movl %ebp, %es
    movl %ebp, %fs
    movl %ebp, %gs

    # Call our isr handler (C code)
    pushl %esp
    call irq_handler
    addl $4, %esp

    # Reload the original data segment descriptor
    popl %ebp       
    movl %ebp, %ds 
    movl %ebp, %es 
    movl %ebp, %fs 
    movl %ebp, %gs

    # Pops all the registers we saved in the start
    # popad
    popl %edi
	popl %esi
	popl %ebp
    addl $4, %esp # Don't pop %esp, may not be defined.
	popl %ebx
	popl %edx
	popl %ecx
	popl %eax                    
    # Clears the stack of the pushed arguments (in the assembly macro above)
    # (Error code and pushed ISR number) 
    # This is a quicker form, instead of:
    # popl %eax
    # popl %eax
    addl $0x8, %esp

    jmp fix_return_stack
fix_return_stack_complete:

    # Reenables interrupts
    # sti
    # Returns (pops 5 things at once; CS, EIP, EFLAGS, SS, and ESP)
    iret

fixup_call_stack:
    mov %eax, -4-8(%esp) # Save eax
	mov 0(%esp), %eax # int_no
	mov %eax, 0-8(%esp)
	mov 4(%esp), %eax # err_code
	mov %eax, 4-8(%esp)
	mov 8(%esp), %eax # eip
	mov %eax, 8-8(%esp)
	mov 12(%esp), %eax # cs
	mov %eax, 12-8(%esp)
	mov 16(%esp), %eax # eflags
	mov %eax, 16-8(%esp)
	# Next up we have to fake what the CPU should have done: pushed ss and esp.
	mov %esp, %eax
	addl $5*4, %eax # Calculate original esp
	mov %eax, 20-8(%esp)
	mov %ss, %eax
	mov %eax, 24-8(%esp)
	# Now that we moved the stack, it's time to really handle the interrupt.
	mov -4-8(%esp), %eax
	subl $8, %esp
    jmp fixup_call_stack_complete

fix_return_stack:
	mov %eax, -4(%esp) # IEAX, Clobbered as copying temporary
	mov %ebx, -8(%esp) # IEBX, Clobbered as pointer to new stack
	mov %ecx, -12(%esp) # IECX, Clobbered as new stack selector
	mov 12(%esp), %ebx # Pointer to new stack
	sub $3*4, %ebx # Point to eip on the new stack (see diagram)
	movw 16(%esp), %cx # New ss
	mov -4(%esp), %eax # interrupted eax value
	mov %eax, -12(%ebx)
	mov -8(%esp), %eax # interrupted ebx value
	mov %eax, -16(%ebx)
	mov -12(%esp), %eax # interrupted ecx value
	mov %eax, -20(%ebx)
	mov 8(%esp), %eax # eflags
	mov %eax, 8(%ebx)
	mov 4(%esp), %eax # cs
	mov %eax, 4(%ebx)
	mov 0(%esp), %eax # eip
	mov %eax, 0(%ebx)
	mov %cx, %ss # Load new stack selector
	mov %ebx, %esp # Load new stack pointer
	mov -12(%esp), %eax # restore interrupted eax value
	mov -16(%esp), %ebx # restore interrupted ebx value
	mov -20(%esp), %ecx # restore interrupted ecx value
	jmp fix_return_stack_complete
    

