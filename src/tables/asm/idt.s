/**
 * @file	idt.s
 * @author  Emil Madsen <skeen@cs.au.dk>
 * @version 1.0
 *
 * @section LICENSE
 *
 * Public Domain.
 *
 * @section DESCRIPTION
 *
 * Contains the setup code for the interrupt descriptor table
 */

# Export this function, as it needs to be called from C code (idt_flush())
.global idt_flush
    
idt_flush:
    # Get the pointer to the IDT, passed as a parameter. 
    mov 0x4(%esp), %eax 
    # Load the IDT pointer.
    lidtl (%eax)
    ret
