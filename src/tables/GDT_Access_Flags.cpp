#include "GlobalDescriptorTable.hpp"

GDT_Access_Flags::GDT_Access_Flags(u8int raw)
{
    data.raw = raw;
}


GDT_Access_Flags::GDT_Access_Flags(bool Present, u8int ring_level, bool code_segment, bool segment_direction, bool read_write_enabled)
{
    bitField bits;
    bits.P = Present;
    bits.DPL = ring_level;
    bits.DT = 1;
    bits.EX = code_segment;
    bits.DC = segment_direction;
    bits.RW = read_write_enabled;
    bits.AC = 0;

    data.format = bits;
}

bool GDT_Access_Flags::isCodeSegment()
{
    return data.format.EX;
}

bool GDT_Access_Flags::isDataSegment()
{
    return !isCodeSegment();
}

bool GDT_Access_Flags::isPresent()
{
    return data.format.P;
}

u8int GDT_Access_Flags::getRingLevel()
{
    return data.format.DPL;
}

bool GDT_Access_Flags::hasBeenAccessed()
{
    return data.format.AC;
}

u8int GDT_Access_Flags::getRaw()
{
    return data.raw;
}

GDT_Access_Flags::bitField GDT_Access_Flags::getField()
{
    return data.format;
}
