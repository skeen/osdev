// 
// descriptor_tables.h - Defines the interface for initialising the GDT and IDT.
//                       Also defines needed structures.
//                       Based on code from Bran's kernel development tutorials.
//                       Rewritten for JamesM's kernel development tutorials.
//

#ifndef _OSDEV_INTERRUPT_DESCRIPTOR_TABLE_HPP
#define _OSDEV_INTERRUPT_DESCRIPTOR_TABLE_HPP

#include "GlobalDescriptorTable.hpp"

class IDT_Flags
{
    //----------
    // Datatypes
    public:
        struct bitField
        {
            u8int MAGIC : 5; // MAGIC NUMBER, should always be 0b0110 or 14 in decimal
            u8int DPL   : 2; // Descriptor privilege level, 2 bits. - Contains the ring level, 0 = highest (kernel), 3 = lowest (user applications).
            bool P      : 1; // Present bit. - Is segment present? (1 = Yes). If '0' it will cause an 'Interrupt Not Handled' exception.
        } PACKED;

        static_assert(sizeof(bitField)==1,  "Incorrect size of type");
    private:
        // Access flags, determine what ring this segment can be used in.
        union flags
        {
            u8int raw;
            bitField format;
        };
    protected:

    //----------
    // Functions
    public:
        IDT_Flags(u8int raw);
        IDT_Flags(bool Present, u8int ring_level);

        bool isPresent();
        u8int getRingLevel();

        u8int getRaw();
        bitField getField();
    private:
    protected:
    //----------
    // Variables
    public:
    private:
        flags data;
    protected:
};

class IDT_Base_Splitter
{
    //----------
    // Datatypes
    public:
    private:
        struct splitter
        {
            u16int low;
            u16int high;
        } PACKED;

        static_assert(sizeof(splitter)==4,  "Incorrect size of type");

        union base
        {
            u32int raw;
            splitter split;
        };
    protected:
    //----------
    // Functions
    public:
        IDT_Base_Splitter(u32int raw);

        u16int getLow();
        u16int getHigh();
    private:
    protected:

    //----------
    // Variables
    public:
    private:
        base data;
    protected:
};

class InterruptDescriptorTable
{
    //----------
    // Datatypes
    public:
    private:
        // A struct describing an interrupt gate.
        struct idt_entry_struct
        {
            u16int base_low;             // The lower 16 bits of the address to jump to when this interrupt fires.
            u16int selector;            // Kernel segment selector.
            u8int  always0;             // This must always be zero.
            // More flags. See documentation.
            u8int flags;               
            u16int base_high;             // The upper 16 bits of the address to jump to.
        } PACKED;

        typedef struct idt_entry_struct idt_entry_t;

        static_assert(sizeof(idt_entry_t)==8,  "Incorrect size of type");

        // A struct describing a pointer to an array of interrupt handlers.
        // This is in a format suitable for giving to 'lidt'.
        struct idt_ptr_struct
        {
            u16int limit;
            u32int base;                // The address of the first element in our idt_entry_t array.
        } PACKED;

        typedef struct idt_ptr_struct idt_ptr_t;

        static_assert(sizeof(idt_ptr_t)==6,  "Incorrect size of type");
    protected:
        //----------
        // Functions
    public:
        static InterruptDescriptorTable& instance()
        {
            static InterruptDescriptorTable s_instance;
            s_instance.initialize();
            return s_instance;
        }
    private:
        InterruptDescriptorTable() {}
        // Initialisation routine - zeroes all the interrupt service routines,
        void initialize();
        void remapIRQ();
        void initialize_InterruptServiceRoutines();
        void initialize_InterruptRequests();
        void idt_set_gate(u8int number, IDT_Base_Splitter base, u16int selector, IDT_Flags flags);
    protected:
        //----------
        // Variables
    public:
    private:
        /** Variable describing if the object has been initialized yet */
        bool initialized = false;

        idt_entry_t idt_entries[256];
        idt_ptr_t   idt_ptr;
    protected:
};

#endif //_OSDEV_INTERRUPT_DESCRIPTOR_TABLE_HPP
