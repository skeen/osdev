#ifndef _OSDEV_GLOBAL_DESCRIPTOR_TABLE_HPP
#define _OSDEV_GLOBAL_DESCRIPTOR_TABLE_HPP

#include <misc/common.hpp>
#include <misc/compiler.h>

class GDT_Access_Flags
{
    //----------
    // Datatypes
    public:
        struct bitField
        {
            bool AC     : 1; // Accessed bit. Just set to 0. The CPU sets this to 1 when the segment is accessed.
            bool RW     : 1; // Readable bit/Writable bit. Is the code segment readable / data segment writeable.
            bool DC     : 1; // Direction bit/Conforming bit. 0=the segment grows up. 1=the segment grows down.
            bool EX     : 1; // Executable bit. If 1 code in this segment can be executed, ie. a code selector. If 0 it is a data selector.
            bool DT     : 1; // Descriptor type (Always 1?).
            u8int DPL   : 2; // Descriptor privilege level, 2 bits. - Contains the ring level, 0 = highest (kernel), 3 = lowest (user applications).
            bool P      : 1; // Present bit. - Is segment present? (1 = Yes). This must be 1 for all valid selectors.
        } PACKED;
        
        static_assert(sizeof(bitField)==1,  "Incorrect size of type");
    private:
        // Access flags, determine what ring this segment can be used in.
        union access
        {
            u8int raw;
            bitField format;
        };
    protected:

    //----------
    // Functions
    public:
        GDT_Access_Flags(u8int raw);
        GDT_Access_Flags(bool Present, u8int ring_level, bool code_segment, bool segment_direction, bool read_write_enabled);

        bool isPresent();
        bool isCodeSegment();
        bool isDataSegment();

        u8int getRingLevel();

        bool hasBeenAccessed();

        u8int getRaw();
        bitField getField();
    private:
    protected:
    //----------
    // Variables
    public:
    private:
        access data;
    protected:
};

class GDT_Base_Splitter
{
    //----------
    // Datatypes
    public:
    private:
        struct splitter
        {
            u16int low;
            u8int  middle;
            u8int  high;
        } PACKED;
        
        static_assert(sizeof(splitter)==4,  "Incorrect size of type");

        union base
        {
            u32int raw;
            splitter split;
        };
    protected:

    //----------
    // Functions
    public:
        GDT_Base_Splitter(u32int raw);

        u16int getLow();
        u8int  getMiddle();
        u8int  getHigh();
    private:
    protected:

    //----------
    // Variables
    public:
    private:
        base data;
    protected:
};

class GDT_Limit_Splitter
{
    //----------
    // Datatypes
    public:
    private:
        struct splitter
        {
            u16int low;
            u8int  middle : 4;
            u8int  pad1 : 4;
            u8int  pad2;
        } PACKED;

        static_assert(sizeof(splitter)==4,  "Incorrect size of type");

        union limit
        {
            u32int raw;
            splitter split;
        };
    protected:

    //----------
    // Functions
    public:
        GDT_Limit_Splitter(u32int raw);

        u16int getLow();
        u8int  getMiddle();
    private:
    protected:

    //----------
    // Variables
    public:
    private:
        limit data;
    protected:
};

class GDT_Granularity_Splitter
{
    //----------
    // Datatypes
    public:
    private:
        struct splitter
        {
            u8int pad   : 4; // Padding
            bool A      : 1; // Available for system use (always zero).
            bool ZERO   : 1; // Should always be zero.
            bool D      : 1; // Operand size (0 = 16bit, 1 = 32bit)
            bool G      : 1; // Granularity (0 = 1 byte, 1 = 1kbyte)
        } PACKED;

        static_assert(sizeof(splitter)==1,  "Incorrect size of type");

        union granularity
        {
            u8int raw;
            splitter split;
        };
    protected:

    //----------
    // Functions
    public:
        GDT_Granularity_Splitter(u32int raw);
        GDT_Granularity_Splitter(bool granularity, bool operand_size);

        u8int getRaw();
    private:
    protected:

    //----------
    // Variables
    public:
    private:
        granularity data;
    protected:
};

class GlobalDescriptorTable
{
    //----------
    // Datatypes
    public:
    private:
        // This structure contains the value of one GDT entry.
        // We use the attribute 'packed' to tell GCC not to change
        // any of the alignment in the structure.
        struct gdt_entry_struct
        {
            u16int limit_low;           // The lower 16 bits of the limit.
            u16int base_low;            // The lower 16 bits of the base.
            u8int  base_middle;         // The next 8 bits of the base.
            u8int  access;
            struct
            {
                u8int limit_mid : 4;
                u8int granularity : 4;
            };
            u8int  base_high;           // The last 8 bits of the base.
        } PACKED;

        typedef struct gdt_entry_struct gdt_entry_t;

        static_assert(sizeof(gdt_entry_t)==8,  "Incorrect size of type");

        // This struct describes a GDT pointer. It points to the start of
        // our array of GDT entries, and is in the format required by the
        // lgdt instruction.
        struct gdt_ptr_struct
        {
            u16int limit;               // The upper 16 bits of all selector limits.
            u32int base;                // The address of the first gdt_entry_t struct.
        } PACKED;

        typedef struct gdt_ptr_struct gdt_ptr_t;
        
        static_assert(sizeof(gdt_ptr_t)==6,  "Incorrect size of type");
    protected:
        //----------
        // Functions
    public:
        static GlobalDescriptorTable& instance()
        {
            static GlobalDescriptorTable s_instance;
            s_instance.initialize();
            return s_instance;
        }
    private:
        GlobalDescriptorTable() {}
        void initialize();
        void gdt_set_gate(s32int number, GDT_Base_Splitter base, GDT_Limit_Splitter limit, GDT_Access_Flags access, GDT_Granularity_Splitter gran);
    protected:
        //----------
        // Variables
    public:
    private:
        /** Variable describing if the object has been initialized yet */
        bool initialized = false;

        gdt_entry_t gdt_entries[5];
        gdt_ptr_t   gdt_ptr;
    protected:
};

#endif //_OSDEV_GLOBAL_DESCRIPTOR_TABLE_HPP
