#include "GlobalDescriptorTable.hpp"

GDT_Limit_Splitter::GDT_Limit_Splitter(u32int raw)
{
    data.raw = raw;
}

u16int GDT_Limit_Splitter::getLow()
{
    return data.split.low;
}

u8int  GDT_Limit_Splitter::getMiddle()
{
    return data.split.middle;
}
