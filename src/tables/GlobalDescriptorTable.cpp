#include "GlobalDescriptorTable.hpp"

// Lets us access our ASM functions from our C code.
ASM_LINK void gdt_flush(u32int);

void GlobalDescriptorTable::initialize()
{
    if(initialized == true)
    {
        return;
    }
    else
    {
        initialized = true;
        gdt_ptr.limit = (sizeof(gdt_entry_t) * 5) - 1;
        gdt_ptr.base  = (u32int)&gdt_entries;

        // Null segment
        gdt_set_gate(0, GDT_Base_Splitter(0), GDT_Limit_Splitter(0), GDT_Access_Flags(0), GDT_Granularity_Splitter(0));                
        // Kernel mode code segment
        // Present = true, ring = 0, code_segment = true, segment_direction = up, readable = true
        gdt_set_gate(1, GDT_Base_Splitter(0), GDT_Limit_Splitter(0xFFFFFFFF), GDT_Access_Flags(true, 0, true, 0, true), GDT_Granularity_Splitter(true, true));
        // Kernel mode data segment
        // Present = true, ring = 0, code_segment = false, segment_direction = up, writeable = true
        gdt_set_gate(2, GDT_Base_Splitter(0), GDT_Limit_Splitter(0xFFFFFFFF), GDT_Access_Flags(true, 0, false, false, true), GDT_Granularity_Splitter(true, true));
        // User mode code segment
        // Present = true, ring = 3, code_segment = true, segment_direction = up, readable = true
        gdt_set_gate(3, GDT_Base_Splitter(0), GDT_Limit_Splitter(0xFFFFFFFF), GDT_Access_Flags(true, 3, true, false, true), GDT_Granularity_Splitter(true, true)); 
        // User mode data segment
        // Present = true, ring = 3, code_segment = false, segment_direction = up, writeable = true
        gdt_set_gate(4, GDT_Base_Splitter(0), GDT_Limit_Splitter(0xFFFFFFFF), GDT_Access_Flags(true, 3, false, false, true), GDT_Granularity_Splitter(true, true)); 

        gdt_flush((u32int)&gdt_ptr);
    }
}

// Set the value of one GDT entry.
void GlobalDescriptorTable::gdt_set_gate(s32int number, GDT_Base_Splitter base, GDT_Limit_Splitter limit, GDT_Access_Flags access, GDT_Granularity_Splitter gran)
{
    gdt_entries[number].base_low    = base.getLow();
    gdt_entries[number].base_middle = base.getMiddle();
    gdt_entries[number].base_high   = base.getHigh();

    gdt_entries[number].limit_low   = limit.getLow();
    gdt_entries[number].limit_mid   = limit.getMiddle();

    gdt_entries[number].granularity = gran.getRaw();
    gdt_entries[number].access      = access.getRaw();
}
