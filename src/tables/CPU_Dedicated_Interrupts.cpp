#include <monitor/printf.hpp>
#include "CPU_Dedicated_Interrupts.h"

const char* const CPU_dedicated_interrupts[32] =
{
    "Division by zero exception",   // 0
    "Debug exception",              // 1
    "Non maskable interrupt",       // 2
    "Breakpoint exception",         // 3
    "'Into detected overflow'",     // 4
    "Out of bounds exception",      // 5
    "Invalid opcode exception",     // 6
    "No coprocessor exception",     // 7
    "Double fault",                 // 8  (error code)
    "Coprocessor segment overrun",  // 9
    "Bad TSS",                      // 10 (error code)
    "Segment not present",          // 11 (error code)
    "Stack fault",                  // 12 (error code)
    "General protection fault",     // 13 (error code)
    "Page fault",                   // 14 (error code)
    "Unknown interrupt exception",  // 15
    "Coprocessor fault",            // 16
    "Alignment check exception",    // 17
    "Machine check exception",      // 18
    "Reserved"                      // 19-31
};

void page_fault_debugging(registers_t *regs)
{
    // Address
    u32int faulting_address = 0;
    INLINE_ASSEMBLY("mov %%cr2, %0" : "=r" (faulting_address));
    // Error code
    bool present  = CHECK_BIT(regs->error_code, 0);
    bool rw       = CHECK_BIT(regs->error_code, 1); 
    bool user     = CHECK_BIT(regs->error_code, 2); 
    bool reserved = CHECK_BIT(regs->error_code, 3); 
    bool fetch    = CHECK_BIT(regs->error_code, 4); 
        
    // Page present
    printf("Page Present:\t");
    if(rw == true)
    {
        printf("true");
    }
    else
    {
        printf("false");
    }
    printf("\n");

    // Fault type
    printf("Fault doing:\t");
    if(rw == true)
    {
        printf("write");
    }
    else
    {
        printf("read");
    }
    printf("\n");

    // Processor state
    printf("Processor-mode:\t");
    if(user == true)
    {
        printf("user-mode");
    }
    else
    {
        printf("kernel-mode");
    }
    printf("\n");

    // Reversed bits
    printf("Reserved bits:\t");
    if(reserved == false)
    {
        printf("not ");
    }
    printf("overwritten\n");

    // Fetch?
    printf("Fault:\t\t\t");
    if(fetch == false)
    {
        printf("not ");
    }
    printf("during a fetch instruction\n");

    // Address
    printf("Fault Address:\t%x\n", faulting_address);

    PANIC("PAGE-FAULT");
}

void CPU_Dedicated_Interrupt_Handler(registers_t *regs)
{
    printf("==============================\n");
    printf("CPU Dedicated Interrupt Fired!\n");
    printf("==============================\n");
    printf("TYPE:\t\t\t%s\n", CPU_dedicated_interrupts[regs->interrupt_number]);
    switch(regs->interrupt_number)
    {
        case 8:  case 10: case 11:
        case 12: case 13:
            printf("Error code: %x\n", regs->error_code);
            break;

        case 14:
            page_fault_debugging(regs);
            break;

        default:
            break;
    }
    PANIC("INTERNAL CPU ERROR");
}

// The first pair, holds the lower and upper bounds of which to load the
// Runnable to
Vector<Pair<u8int, Runnable<void, registers_t*>*>> getCPUDedicatedInterruptHandlers()
{
    Vector<Pair<u8int, Runnable<void, registers_t*>*>> handlers;
    for(int x=0; x<32; x++)
    {
        Runnable<void, registers_t*>* run = makeRunnable(CPU_Dedicated_Interrupt_Handler);
        Pair<u8int, Runnable<void, registers_t*>*> p = Pair<u8int, Runnable<void, registers_t*>*>(x, run);
        handlers.push_back(p);
    }
    return handlers;
}
