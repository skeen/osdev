#include "InterruptDescriptorTable.hpp"

IDT_Base_Splitter::IDT_Base_Splitter(u32int raw)
{
    data.raw = raw;
}

u16int IDT_Base_Splitter::getLow()
{
    return data.split.low;
}

u16int IDT_Base_Splitter::getHigh()
{
    return data.split.high;
}
