#include <misc/datastructures/Vector.hpp>
#include <misc/datastructures/Pair.hpp>
#include <misc/datastructures/Delegate.hpp>

#include "InterruptServiceRoutine.h"

Vector<Pair<u8int, Runnable<void, registers_t*>*>> getCPUDedicatedInterruptHandlers();
