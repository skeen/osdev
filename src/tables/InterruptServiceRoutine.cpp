/**
 * @file	isr.c
 * @author  Emil Madsen <skeen@cs.au.dk>
 * @version 1.0
 *
 * @section LICENSE
 *
 * Public Domain.
 *
 * @section DESCRIPTION
 *
 * Defines the interrupt service routines and the interrupt request handleres.
 */

#include <misc/common.hpp>
#include "InterruptServiceRoutine.h"
#include <monitor/printf.hpp>

InterruptService* service = nullptr;

void seedInterruptService(InterruptService* service_init)
{
    service = service_init;
}

// This gets called from our ASM interrupt handler stub.
ASM_LINK void isr_handler(registers_t *regs)
{
    // This line is important. When the processor extends the 8-bit interrupt number
    // to a 32bit value, it sign-extends, not zero extends. So if the most significant
    // bit (0x80) is set, regs.int_no will be very large (about 0xffffff80).
    u8int int_no = regs->interrupt_number & 0xFF;
    if (service->size(int_no) != 0)
    {
        service->interrupt(regs);
    }
    else
    {
        printf("unhandled interrupt: %x\n", regs->interrupt_number);
        printf("recieved error_code: %x\n", regs->error_code);
        PANIC("UNHANDLED CPU INTERRUPT");
    }
}

// This gets called from our ASM interrupt handler stub.
ASM_LINK void irq_handler(registers_t *regs)
{
    service->interrupt(regs);
    // Send an EOI (end of interrupt) signal to the PICs.
    // If this interrupt involved the slave.
    if (regs->interrupt_number >= 40)
    {
        // Send reset signal to slave.
        Hardware::outb(0xA0, 0x20);
    }
    // Send reset signal to master. (As well as slave, if necessary).
    Hardware::outb(0x20, 0x20);
}
