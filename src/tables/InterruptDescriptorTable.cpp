#include <misc/common.hpp>
#include "InterruptDescriptorTable.hpp"

#include "CPU_Dedicated_Interrupts.h"

// Lets us access our ASM functions from our C code.
ASM_LINK void idt_flush(u32int);

// These ASM_LINK directives let us access the addresses of our ASM ISR handlers.
ASM_LINK void isr0 ();
ASM_LINK void isr1 ();
ASM_LINK void isr2 ();
ASM_LINK void isr3 ();
ASM_LINK void isr4 ();
ASM_LINK void isr5 ();
ASM_LINK void isr6 ();
ASM_LINK void isr7 ();
ASM_LINK void isr8 ();
ASM_LINK void isr9 ();
ASM_LINK void isr10();
ASM_LINK void isr11();
ASM_LINK void isr12();
ASM_LINK void isr13();
ASM_LINK void isr14();
ASM_LINK void isr15();
ASM_LINK void isr16();
ASM_LINK void isr17();
ASM_LINK void isr18();
ASM_LINK void isr19();
ASM_LINK void isr20();
ASM_LINK void isr21();
ASM_LINK void isr22();
ASM_LINK void isr23();
ASM_LINK void isr24();
ASM_LINK void isr25();
ASM_LINK void isr26();
ASM_LINK void isr27();
ASM_LINK void isr28();
ASM_LINK void isr29();
ASM_LINK void isr30();
ASM_LINK void isr31();

ASM_LINK void irq0();
ASM_LINK void irq1();
ASM_LINK void irq2();
ASM_LINK void irq3();
ASM_LINK void irq4();
ASM_LINK void irq5();
ASM_LINK void irq6();
ASM_LINK void irq7();
ASM_LINK void irq8();
ASM_LINK void irq9();
ASM_LINK void irq10();
ASM_LINK void irq11();
ASM_LINK void irq12();
ASM_LINK void irq13();
ASM_LINK void irq14();
ASM_LINK void irq15();

ASM_LINK void irq100();

void InterruptDescriptorTable::remapIRQ()
{
    // Remap the irq table.
    /* ICW1 */
    Hardware::outb(0x20, 0x11); /* Master port A */
    Hardware::outb(0xA0, 0x11); /* Slave port A */
    /* ICW2 */
    Hardware::outb(0x21, 0x20); /* Master offset of 0x20 in the IDT */
    Hardware::outb(0xA1, 0x28); /* Master offset of 0x28 in the IDT */
    /* ICW3 */
    Hardware::outb(0x21, 0x04); /* Slaves attached to IR line 2 */
    Hardware::outb(0xA1, 0x02); /* This slave in IR line 2 of master */
    /* ICW4 */
    Hardware::outb(0x21, 0x01); /* Set as master */
    Hardware::outb(0xA1, 0x01); /* Set as slave */
    /* MASK NONE */
    Hardware::outb(0x21, 0x0); /* master PIC NO MASK */
    Hardware::outb(0xA1, 0x0); /* slave PIC NO MASK */
}

void InterruptDescriptorTable::initialize_InterruptServiceRoutines()
{
    idt_set_gate( 0, IDT_Base_Splitter((u32int)isr0) , 0x08, IDT_Flags(true, 0));
    idt_set_gate( 1, IDT_Base_Splitter((u32int)isr1) , 0x08, IDT_Flags(true, 0));
    idt_set_gate( 2, IDT_Base_Splitter((u32int)isr2) , 0x08, IDT_Flags(true, 0));
    idt_set_gate( 3, IDT_Base_Splitter((u32int)isr3) , 0x08, IDT_Flags(true, 0));
    idt_set_gate( 4, IDT_Base_Splitter((u32int)isr4) , 0x08, IDT_Flags(true, 0));
    idt_set_gate( 5, IDT_Base_Splitter((u32int)isr5) , 0x08, IDT_Flags(true, 0));
    idt_set_gate( 6, IDT_Base_Splitter((u32int)isr6) , 0x08, IDT_Flags(true, 0));
    idt_set_gate( 7, IDT_Base_Splitter((u32int)isr7) , 0x08, IDT_Flags(true, 0));
    idt_set_gate( 8, IDT_Base_Splitter((u32int)isr8) , 0x08, IDT_Flags(true, 0));
    idt_set_gate( 9, IDT_Base_Splitter((u32int)isr9) , 0x08, IDT_Flags(true, 0));
    idt_set_gate(10, IDT_Base_Splitter((u32int)isr10), 0x08, IDT_Flags(true, 0));
    idt_set_gate(11, IDT_Base_Splitter((u32int)isr11), 0x08, IDT_Flags(true, 0));
    idt_set_gate(12, IDT_Base_Splitter((u32int)isr12), 0x08, IDT_Flags(true, 0));
    idt_set_gate(13, IDT_Base_Splitter((u32int)isr13), 0x08, IDT_Flags(true, 0));
    idt_set_gate(14, IDT_Base_Splitter((u32int)isr14), 0x08, IDT_Flags(true, 0));
    idt_set_gate(15, IDT_Base_Splitter((u32int)isr15), 0x08, IDT_Flags(true, 0));
    idt_set_gate(16, IDT_Base_Splitter((u32int)isr16), 0x08, IDT_Flags(true, 0));
    idt_set_gate(17, IDT_Base_Splitter((u32int)isr17), 0x08, IDT_Flags(true, 0));
    idt_set_gate(18, IDT_Base_Splitter((u32int)isr18), 0x08, IDT_Flags(true, 0));
    idt_set_gate(19, IDT_Base_Splitter((u32int)isr19), 0x08, IDT_Flags(true, 0));
    idt_set_gate(20, IDT_Base_Splitter((u32int)isr20), 0x08, IDT_Flags(true, 0));
    idt_set_gate(21, IDT_Base_Splitter((u32int)isr21), 0x08, IDT_Flags(true, 0));
    idt_set_gate(22, IDT_Base_Splitter((u32int)isr22), 0x08, IDT_Flags(true, 0));
    idt_set_gate(23, IDT_Base_Splitter((u32int)isr23), 0x08, IDT_Flags(true, 0));
    idt_set_gate(24, IDT_Base_Splitter((u32int)isr24), 0x08, IDT_Flags(true, 0));
    idt_set_gate(25, IDT_Base_Splitter((u32int)isr25), 0x08, IDT_Flags(true, 0));
    idt_set_gate(26, IDT_Base_Splitter((u32int)isr26), 0x08, IDT_Flags(true, 0));
    idt_set_gate(27, IDT_Base_Splitter((u32int)isr27), 0x08, IDT_Flags(true, 0));
    idt_set_gate(28, IDT_Base_Splitter((u32int)isr28), 0x08, IDT_Flags(true, 0));
    idt_set_gate(29, IDT_Base_Splitter((u32int)isr29), 0x08, IDT_Flags(true, 0));
    idt_set_gate(30, IDT_Base_Splitter((u32int)isr30), 0x08, IDT_Flags(true, 0));
    idt_set_gate(31, IDT_Base_Splitter((u32int)isr31), 0x08, IDT_Flags(true, 0));
}

void InterruptDescriptorTable::initialize_InterruptRequests()
{
    idt_set_gate(32, IDT_Base_Splitter((u32int)irq0),  0x08, IDT_Flags(true, 0));
    idt_set_gate(33, IDT_Base_Splitter((u32int)irq1),  0x08, IDT_Flags(true, 0));
    idt_set_gate(34, IDT_Base_Splitter((u32int)irq2),  0x08, IDT_Flags(true, 0));
    idt_set_gate(35, IDT_Base_Splitter((u32int)irq3),  0x08, IDT_Flags(true, 0));
    idt_set_gate(36, IDT_Base_Splitter((u32int)irq4),  0x08, IDT_Flags(true, 0));
    idt_set_gate(37, IDT_Base_Splitter((u32int)irq5),  0x08, IDT_Flags(true, 0));
    idt_set_gate(38, IDT_Base_Splitter((u32int)irq6),  0x08, IDT_Flags(true, 0));
    idt_set_gate(39, IDT_Base_Splitter((u32int)irq7),  0x08, IDT_Flags(true, 0));
    idt_set_gate(40, IDT_Base_Splitter((u32int)irq8),  0x08, IDT_Flags(true, 0));
    idt_set_gate(41, IDT_Base_Splitter((u32int)irq9),  0x08, IDT_Flags(true, 0));
    idt_set_gate(42, IDT_Base_Splitter((u32int)irq10), 0x08, IDT_Flags(true, 0));
    idt_set_gate(43, IDT_Base_Splitter((u32int)irq11), 0x08, IDT_Flags(true, 0));
    idt_set_gate(44, IDT_Base_Splitter((u32int)irq12), 0x08, IDT_Flags(true, 0));
    idt_set_gate(45, IDT_Base_Splitter((u32int)irq13), 0x08, IDT_Flags(true, 0));
    idt_set_gate(46, IDT_Base_Splitter((u32int)irq14), 0x08, IDT_Flags(true, 0));
    idt_set_gate(47, IDT_Base_Splitter((u32int)irq15), 0x08, IDT_Flags(true, 0));

    // TODO: Move somewhere better
    idt_set_gate(100, IDT_Base_Splitter((u32int)irq100), 0x08, IDT_Flags(true, 0));
}

void InterruptDescriptorTable::initialize()
{
    if(initialized == true)
    {
        return;
    }
    else
    {
        initialized = true;

        idt_ptr.limit = sizeof(idt_entry_t) * 256 -1;
        idt_ptr.base  = (u32int)&idt_entries;

        Util::memset(&idt_entries, 0, sizeof(idt_entry_t)*256);

        remapIRQ();
        initialize_InterruptServiceRoutines();
        initialize_InterruptRequests();
        
        idt_flush((u32int)&idt_ptr);
    }
}

void InterruptDescriptorTable::idt_set_gate(u8int number, IDT_Base_Splitter base, u16int selector, IDT_Flags flags)
{
    idt_entries[number].base_low  = base.getLow();
    idt_entries[number].base_high = base.getHigh();

    idt_entries[number].selector  = selector;
    idt_entries[number].always0   = 0;

    idt_entries[number].flags     = flags.getRaw();
}
