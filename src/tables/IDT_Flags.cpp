#include "InterruptDescriptorTable.hpp"

IDT_Flags::IDT_Flags(u8int raw)
{
    data.raw = raw;
}


IDT_Flags::IDT_Flags(bool Present, u8int ring_level)
{
    bitField bits;
    bits.P = Present;
    bits.DPL = ring_level;
    bits.MAGIC = 14;

    data.format = bits;
}

bool IDT_Flags::isPresent()
{
    return data.format.P;
}

u8int IDT_Flags::getRingLevel()
{
    return data.format.DPL;
}

u8int IDT_Flags::getRaw()
{
    return data.raw;
}

IDT_Flags::bitField IDT_Flags::getField()
{
    return data.format;
}

