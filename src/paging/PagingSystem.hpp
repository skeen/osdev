#ifndef _OSDEV_PAGING_SYSTEM_HPP
#define _OSDEV_PAGING_SYSTEM_HPP

#include "PhysicalMemoryManager.hpp"
#include "LinearMemoryManager.hpp"

#include <misc/new.hpp>

class PagingSystem
{
    public:
        /**
          Sets up the environment, page directories etc and
          enables paging.
         **/
        PagingSystem(multiboot_info_t* mbd);

        LinearMemoryManager* getLinearMemoryManager();

    private:
        u8int physical_pool[sizeof(PhysicalMemoryManager)];
        u8int linear_pool[sizeof(LinearMemoryManager)];

        PhysicalMemoryManager* pmm;
        LinearMemoryManager* lmm;

};

#endif //_OSDEV_PAGING_SYSTEM_HPP
