#include "Paging.hpp"

virtual_address::virtual_address(u32int virtual_addr)
{
    addr.virtual_addr = virtual_addr;
}

virtual_address::virtual_address(void* virtual_addr)
{
    addr.virtual_addr = (u32int) virtual_addr;
}

u32int virtual_address::getPageDirectoryIndex()
{
    return addr.virtual_addr_split.page_directory_index;
}

u32int virtual_address::getPageTableIndex()
{
    return addr.virtual_addr_split.page_table_index;
}

u32int virtual_address::getOffsetIntoPage()
{
    return addr.virtual_addr_split.offset_into_page;
}
