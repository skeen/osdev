#include "Paging.hpp"

void PhysicalMemoryManagerPageChanger::setAddress(page_t* page, PhysicalPageAddress ptr)
{
    page->setAddress(ptr);
}

void PhysicalMemoryManagerPageChanger::setPresent(page_t* page, bool present)
{
    page->setPresent(present);
}
