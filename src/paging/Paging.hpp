#ifndef _OSDEV_PAGING_H
#define _OSDEV_PAGING_H

#include <misc/common.hpp>
#include <misc/compiler.h>
#include <misc/multiboot.h>

const u32int PAGES_PER_TABLE        = 1024;
const u32int PAGES_PER_DIRECTORY    = 1024;
const u32int BYTES_PER_PAGE         = 4096; 
#define PAGE_ALIGNMENT 4096

// physical address
class PhysicalAddress
{
    //---------
    //Functions
    public:
        PhysicalAddress(u32int address)
        : address(address)
        {
        }

        u32int getAddress() const
        {
            return address;
        }

        bool operator<(const PhysicalAddress &other) const
        {
            return (address < other.address);
        }

        bool operator==(const PhysicalAddress &other) const
        {
            return (address == other.address);
        }

        PhysicalAddress& operator+=(const PhysicalAddress &rhs)
        {
            address += rhs.address;
            return *this;
        }

        const PhysicalAddress operator+(const PhysicalAddress &other) const
        {
            return PhysicalAddress(address + other.address);
        }

        const PhysicalAddress operator-(const PhysicalAddress &other) const
        {
            return PhysicalAddress(address - other.address);
        }

        const PhysicalAddress operator&(const PhysicalAddress &other) const
        {
            return PhysicalAddress(address & other.address);
        }
    private:
    protected:
    //---------
    //Variables
    public:
    private:
    protected:
        u32int address;
};

// Tagging interface representing a page-aligned address
// (Type-system now handles page alignedness)
class PhysicalPageAddress : public PhysicalAddress
{
    //---------
    //Functions
    public:
        PhysicalPageAddress(u32int address)
        : PhysicalAddress(address)
        {
        }

        PhysicalPageAddress(const PhysicalAddress &phys)
        : PhysicalAddress(phys.getAddress())
        {
        }
    private:
    protected:
    //---------
    //Variables
    public:
    private:
    protected:
};

struct ALIGN(PAGE_ALIGNMENT) page_frame_struct
{
    // Just 'BYTES_PER_PAGE' of random bytes
    // This structure is used by the LinearMemoryManager
    // And the PhysicalMemoryManager
    u8int bytes[BYTES_PER_PAGE];
} PACKED;

typedef struct page_frame_struct page_frame_t;

static_assert(sizeof(page_frame_t)==BYTES_PER_PAGE,  "Incorrect size of type");
static_assert(alignof(page_frame_t)==PAGE_ALIGNMENT,  "Incorrect align of type");

class PhysicalMemoryManagerPageChanger;
class LinearMemoryManagerPageChanger;
class IdentityMapperPageChanger;

class page_struct
{
    public:
        /**
         * Constructs a NULL page_struct
         */
        page_struct();

        /**
         * Gets the physical address associated with this page_struct
         */
        PhysicalPageAddress getAddress();

        /**
         * Returns whether a this specific page is present in memory
         */
        bool isPresent();
    private:
        /**
         * Set the address of this pagestruct to ptr
         * Note: This function is not a worker, but rather used by the friend
         * classes (encapsulation is protected using the attorney-client idiom)
         */
        void setAddress(PhysicalPageAddress ptr);

        /**
         * Sets this page as present
         * Note: This function is not a worker, but rather used by the friend
         * classes (encapsulation is protected using the attorney-client idiom)
         */
        void setPresent(bool present);

        /**
         * Set the values of read/write and usermode flags
         * Note: This function is not a worker, but rather used by the friend
         * classes (encapsulation is protected using the attorney-client idiom)
         */
        void setReadWrite(bool value);
        void setUserMode(bool value);
    private:
        bool Present : 1;       // Set if the page is present in memory.
        bool ReadWrite : 1;     // If set, that page is writeable. If unset, the page is read-only. This does not apply when code is running in kernel-mode (unless a flag in CR0 is set).
        bool UserMode : 1;      // If set, this is a user-mode page. Else it is a supervisor (kernel)-mode page. User-mode code cannot write to or read from kernel-mode pages.
        bool WriteThrough : 1;
        bool CacheDisabled : 1;
        bool Accessed : 1;      // Set if the page has been accessed (Gets set by the CPU).
        bool Dirty : 1;         // Set if the page has been written to (dirty).
        bool Zero : 1;
        bool Global : 1;                  
        u8int AVAIL : 3;        // These 3 bits are unused and available for kernel-use.
        u32int Frame : 20;      // The high 20 bits of the frame address in physical memory.

        friend PhysicalMemoryManagerPageChanger;
        friend LinearMemoryManagerPageChanger;
        friend IdentityMapperPageChanger;
} PACKED;

typedef struct page_struct page_t;

static_assert(sizeof(page_t)==4,  "Incorrect size of type");

class PhysicalMemoryManager;

/**
 * This is a rather special class, as it implements the attorney-client idiom
 * Which allows the PhysicalMemoryManager single handly access to some of the page_t's private methods.
 * This design pattern is used to increase capsulation of the page_t class.
 *
 * Note: All members are intentionally private, as only the friend class
 * (PhysicalMemoryManager) should have access to it.
 */
class PhysicalMemoryManagerPageChanger
{
    private:
        static void setAddress(page_t* page, PhysicalPageAddress ptr);
        static void setPresent(page_t* page, bool present);
        
        // The only class that will be allowed to access the above functions
        friend PhysicalMemoryManager;
};

class LinearMemoryManager;

/**
 * This is a rather special class, as it implements the attorney-client idiom
 * Which allows the LinearMemoryManager single handly access to some of the page_t's private methods.
 * This design pattern is used to increase capsulation of the page_t class.
 *
 * Note: All members are intentionally private, as only the friend class
 * (LinearMemoryManager) should have access to it.
 */
class LinearMemoryManagerPageChanger
{
    private:
        static void setAddress(page_t* page, PhysicalPageAddress ptr);
        static void setPresent(page_t* page, bool present);

        static void setReadWrite(page_t* page, bool value);
        static void setUserMode(page_t* page, bool value);

        // The only class that will be allowed to access the above functions
        friend LinearMemoryManager;
};

class IdentityMapper;

/**
 * This is a rather special class, as it implements the attorney-client idiom
 * Which allows the IdentityMapper single handly access to some of the page_t's private methods.
 * This design pattern is used to increase capsulation of the page_t class.
 *
 * Note: All members are intentionally private, as only the friend class
 * (IdentityMapper) should have access to it.
 */
class IdentityMapperPageChanger
{
    private:
        static void setAddress(page_t* page, PhysicalPageAddress ptr);
        static void setPresent(page_t* page, bool present);
        static void setReadWrite(page_t* page, bool value);
        static void setUserMode(page_t* page, bool value);

        // The only class that will be allowed to access the above functions
        friend IdentityMapper;
};

struct ALIGN(PAGE_ALIGNMENT) page_table_struct
{
    public:
        page_table_struct();

        page_t getPage(u16int index);
        void setPage(u16int index, page_t page);
    private:
        page_t pages[PAGES_PER_TABLE];
} PACKED;

typedef struct page_table_struct page_table_t;

static_assert(sizeof(page_table_t)==4096,  "Incorrect size of type");
static_assert(alignof(page_table_t)==PAGE_ALIGNMENT,  "Incorrect align of type");

class page_table_ptr_struct
{
    public:
        /**
         * Default NULL intializer
         */
        page_table_ptr_struct();
        /**
         * The general initializer
         */
        page_table_ptr_struct(page_table_struct* tableAddress, bool present, bool readwrite, bool usermode);
        /**
         * Use this function to get the pointee of this pointer object
         */
        page_table_struct* getPointee();
        
    private:
        bool Present : 1;
        bool ReadWrite : 1;
        bool UserMode : 1;
        bool WriteThrough : 1;
        bool CacheDisabled : 1;
        bool Accessed : 1;
        bool Zero : 1;
        bool PageSize : 1;
        bool Global : 1;
        u8int AVAIL : 3;
        u32int Table : 20;
} PACKED;

typedef struct page_table_ptr_struct page_table_ptr_t;

static_assert(sizeof(page_table_ptr_t)==4,  "Incorrect size of type");

class virtual_address
{
    private:
        // Follows the layout at;
        // http://wiki.osdev.org/File:Paging_Structure.gif
        struct virtual_address_splitter
        {
            u32int offset_into_page     : 12;
            u32int page_table_index     : 10;
            u32int page_directory_index : 10;
        } PACKED;

        static_assert(sizeof(virtual_address_splitter)==4,  "Incorrect size of type");

        union address
        {
            virtual_address_splitter virtual_addr_split;
            u32int virtual_addr;
        };

    public:
        virtual_address(u32int virtual_addr);
        virtual_address(void* virtual_addr);
        virtual_address(PhysicalAddress virtual_addr);

        u32int getPageDirectoryIndex();
        u32int getPageTableIndex();
        u32int getOffsetIntoPage();
    private:
        address addr;
};

struct ALIGN(PAGE_ALIGNMENT) page_directory_struct
{
    public:
        /** 
         * Create a full NULL initialized page_directory
         */
        page_directory_struct();

        page_table_ptr_struct getTablePointer(u16int index);
        void setTablePointer(u16int index, page_table_ptr_struct ptr);

        PhysicalAddress getPhysicalAddress(virtual_address v);
    private:
        /**
          Array of pointers to pagetables.
         **/
        page_table_ptr_t tables[PAGES_PER_DIRECTORY];
} PACKED;

typedef struct page_directory_struct page_directory_t;

static_assert(sizeof(page_directory_t)==4096,  "Incorrect size of type");
static_assert(alignof(page_directory_t)==PAGE_ALIGNMENT,  "Incorrect align of type");

#endif //_OSDEV_PAGING_H
