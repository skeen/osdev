#include "PhysicalMemoryManager.hpp"

extern const u32int end;

#include <misc/new.hpp>

PhysicalMemoryManager::PhysicalMemoryManager(multiboot_info_t* mbd)
{
    // We'll just read the multiboot header for the information we need;
    void* memory_map_address = (void*) mbd->mmap_addr;
    memory_map* memory_map_array = (memory_map*) memory_map_address;
    // Calculate the number of memory map entries
    // Assumes all entries are of size 'memory_map'
    u8int number_of_memory_map_entries = (mbd->mmap_length / sizeof(memory_map));
    // We'll create a bitset to hold this information
    // We'll default all pages as being set (taken)
    bitset = new (bitset_buffer) StaticBitset<number_of_pages>(PageStatus::TAKEN);

    // Now is the time to map out the free memory;
    // We'll do this by looping through all the memory map entries finding the
    // ones that are free for usage
    for(u8int x=0; x<number_of_memory_map_entries; x++)
    {
        // Load the entry;
        memory_map memory_map_entry = memory_map_array[x];
        // If it's not usable, dont mind about it
        if(memory_map_entry.type != MULTIBOOT_HEADER_MEMORY_MAP_TYPE_USABLE)
        {
            continue;
        }
        // Find the start and ending address of the current memory entry
        PhysicalAddress start_address = (PhysicalAddress) memory_map_entry.base_address;
        size_t length = (size_t) memory_map_entry.length;
        // If it's useable, we need to mark it as usable.
        freeRegion(start_address, length);
    }
    // We'll now protect our kernel structure, by setting all
    // addresses from 0x0 up to the end (linker symbol) memory as taken.
    PhysicalAddress start_page_address = 0x00000000;
    // Find the page end address
    PhysicalAddress end_address = (PhysicalAddress) (u32int) &end;
    // We'll Ceil this address, to protect data below it
    PhysicalAddress end_page_address = ceilPageAddress(end_address);

    // And lets mark it as used;
    takePageRegion(start_page_address, end_page_address);
}

void PhysicalMemoryManager::freeRegion(PhysicalAddress start_address, size_t length)
{
    // Find the end physical address of the free region.
    PhysicalAddress end_address = start_address + length;

    // Ceil the start_address to nearest page_address
    // As we could otherwise trample with data on the page we're at
    PhysicalAddress start_page_address = ceilPageAddress(start_address);
    // Floor the end_address to nearest page_address
    // Again to avoid trempling stuff
    PhysicalAddress end_page_address = floorPageAddress(end_address);
    // However as we might not be able to claim the entire page,
    // we'll decrement to the page once.
    end_page_address = decrementPageAddress(end_page_address);
    // Mark all pages from start_page_address up to the end_page_address as free.
    freePageRegion(start_page_address, end_page_address);
}

void PhysicalMemoryManager::freePageRegion(PhysicalAddress start_page_address, PhysicalAddress end_page_address)
{
    for(PhysicalAddress iterator_page_address = start_page_address;
                        iterator_page_address < end_page_address;
                        iterator_page_address = incrementPageAddress(iterator_page_address))
    {
        // Mark one page free
        u32int index = indexFromAddress(iterator_page_address);
        bitset->clear(index);
    }
}

void PhysicalMemoryManager::takePageRegion(PhysicalAddress start_page_address, PhysicalAddress end_page_address)
{
    for(PhysicalAddress iterator_page_address = start_page_address;
                        iterator_page_address < end_page_address;
                        iterator_page_address = incrementPageAddress(iterator_page_address))
    {
        // Mark one page taken
        u32int index = indexFromAddress(iterator_page_address);
        bitset->set(index);
    }
}

void PhysicalMemoryManager::allocPhysPage(page_t *page)
{
    // Check if the frame was already allocated
    if(page->isPresent() == true)
    {
        // Just return
        // return 0;
        // TODO: Alternative:
        PANIC("TRYING TO REALLOCATE ALLOCATED PAGE");
    }
    else // Page not yet allocated
    {
        // Locate the first free page's location
        u32int index = bitset->firstClear();
        // Let's claim this page!
        bitset->set(index);

        // And we'll set the pages frame address to that of the frame we just
        // allocated, also this will set the present bit in the page
        // The calls are a bit wierd in order to improve encapsulation
        PhysicalMemoryManagerPageChanger::setAddress(page, addressFromIndex(index));
        PhysicalMemoryManagerPageChanger::setPresent(page, true);
    }
}

void PhysicalMemoryManager::freePhysPage(page_t* page)
{
    // Check if the frame was already deallocated
    if(page->isPresent() == false)
    {
        // Just return
        // return 0;
        // TODO: Alternative:
        PANIC("TRYING TO DEALLOCATE DEALLOCATED PAGE");
    }
    else
    {
        // Let's find the index of the page
        PhysicalAddress ptr = page->getAddress();
        u32int index = indexFromAddress(ptr);

        // Let's free the page in the bitmap
        bitset->clear(index);

        // Let's invalidate the page by setting it's address to 0
        // This also sets the present bit to zero.
        // The calls are a bit wierd in order to improve encapsulation
        PhysicalMemoryManagerPageChanger::setAddress(page, 0);
        PhysicalMemoryManagerPageChanger::setPresent(page, false);
    }
}

PhysicalAddress PhysicalMemoryManager::addressFromIndex(u32int index)
{
    // Lookup the speficied pageframe, in the array
    page_frame_t* page_frame = &(page_frames[index]);
    // And return its address
    return PhysicalAddress((u32int) page_frame);
}

u32int PhysicalMemoryManager::indexFromAddress(PhysicalAddress ptr)
{
    // As ptr is page aligned, we can simply divide it by the size of the
    // page_frame datatype, and we'll get the index.
    // (as the page_frame array starts at 0x00000000).
    u32int tmp = ptr.getAddress();
    tmp = tmp / sizeof(page_frame_t);
    return tmp;
}

PhysicalPageAddress PhysicalMemoryManager::floorPageAddress(PhysicalAddress ptr)
{
    PhysicalAddress tmp = ptr;
    tmp = tmp & 0xFFFFF000;
    return (PhysicalPageAddress) tmp; 
}

PhysicalPageAddress PhysicalMemoryManager::ceilPageAddress(PhysicalAddress ptr)
{
    // Check if it's already aligned
    bool isAligned = checkPageAddress(ptr);
    // If it is simply return it
    if(isAligned == true)
    {
        return ptr;
    }
    // Otherwise lets find it
    else
    {
        // Get to the nearst floor address
        PhysicalPageAddress tmp = floorPageAddress(ptr);
        // Increment it to get the ceil
        return incrementPageAddress(tmp);
    }
}

PhysicalPageAddress PhysicalMemoryManager::incrementPageAddress(PhysicalPageAddress ptr)
{
    // Check the precondition
    bool isAligned = checkPageAddress(ptr);
    if(isAligned == false)
    {
        PANIC("TRIED TO INCREMENT NON PAGE-ALIGNED ADDRESS");
    }
    PhysicalAddress tmp = ptr;
    tmp = tmp + sizeof(page_frame_t);
    return (PhysicalPageAddress) tmp;
}

PhysicalPageAddress PhysicalMemoryManager::decrementPageAddress(PhysicalPageAddress ptr)
{
    // Check the precondition
    bool isAligned = checkPageAddress(ptr);
    if(isAligned == false)
    {
        PANIC("TRIED TO INCREMENT NON PAGE-ALIGNED ADDRESS");
    }
    PhysicalAddress tmp = ptr;
    tmp = tmp - sizeof(page_frame_t);
    return (PhysicalPageAddress) tmp;
}

bool PhysicalMemoryManager::checkPageAddress(PhysicalAddress ptr)
{
    // Get the nearest paging address
    PhysicalAddress tmp = floorPageAddress(ptr);
    // If we get our stating address, then it was already aligned
    if(tmp == ptr)
    {
        return true;
    }
    // Otherwise it wasn't
    else
    {
        return false;
    }
}

