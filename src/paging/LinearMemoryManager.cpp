#include "LinearMemoryManager.hpp"
#include "IdentityMapper.hpp"

#include <misc/new.hpp>

u32int read_cr0()
{
    u32int val;
    INLINE_ASSEMBLY( "movl %%cr0, %0"
                  : "=r" (val));
    return val;
}

PhysicalAddress read_cr3()
{
    u32int val;
    INLINE_ASSEMBLY( "movl %%cr3, %0"
                  : "=r" (val));
    return PhysicalAddress(val);
}

void write_cr0(u32int val)
{
    INLINE_ASSEMBLY("movl %0, %%cr0"
                    :
                    : "r" (val)); 
}

void write_cr3(PhysicalAddress val)
{
    INLINE_ASSEMBLY("movl %0, %%cr3"
                    :
                    : "r" (val));
}

void flush_single_tlb(u32int* ptr)
{
    INLINE_ASSEMBLY("invlpg %0"
                  : 
                  : "m"(*ptr) 
                  : "memory" // clobber memory to avoid optimizer re-ordering access before invlpg, which may cause nasty bug.
     );
}

void flush_tlb()
{
    PhysicalAddress phys = read_cr3();
    u32int val = phys.getAddress();
    write_cr3(val);
}

void set_paging(bool b)
{
    if(b)
    {
        // set the paging bit in CR0 to 1
        write_cr0(read_cr0() | 0x80000000);
    }
    else
    {
        // clear the paging bit in CR0 to 0
        write_cr0(read_cr0() & 0x7FFFFFFF);
    }
}

bool paging_enabled()
{
    return (read_cr0() & 0x80000000) ? false : true;
}

void set_paging_directory(PhysicalAddress dir)
{
    write_cr3(dir);
}

PhysicalAddress get_paging_directory()
{
    return read_cr3();
}

LinearMemoryManager::LinearMemoryManager(PhysicalMemoryManager* pmm)
: pmm(pmm)
{
    // Allocate the default page table
    page_t identity_page = page_t();
    pmm->allocPhysPage(&identity_page);
    // Get the allocated address
    PhysicalAddress identity_page_address = identity_page.getAddress();
    // And clear it
    Util::memset((void*) identity_page_address.getAddress(), 0, sizeof(page_frame_struct));
    // Identity map first 4mb
    page_table_struct* identity_page_table = new ((void*) identity_page_address.getAddress()) page_table_struct();
    
    {
        u16int i=0;
        u32int virt=0x00000000;
        PhysicalAddress frame=0x0;
        while(i<1024)
        {
            // Sanity check
            if (i != virtual_address(virt).getPageTableIndex())
            {
                PANIC("VIRTUAL MISMATCH!");
            }


            // Create a page
            page_t page = page_t();

            LinearMemoryManagerPageChanger::setPresent(&page, true);
            LinearMemoryManagerPageChanger::setAddress(&page, frame);

            // Add it to the page table
            identity_page_table->setPage(i, page);

            // Increment variables
            i++;
            frame+=4096;
            virt+=4096;
        }
    }

    // Allocate the 3gb page table
    page_t table2_page = page_t();
    pmm->allocPhysPage(&table2_page);
    // Get the allocated address
    PhysicalAddress table2_page_address = table2_page.getAddress();
    // And clear it
    Util::memset((void*) table2_page_address.getAddress(), 0, sizeof(page_frame_struct));
    // map 1mb to 3gb
    page_table_struct* table2_page_table = new ((void*) table2_page_address.getAddress()) page_table_struct();

    {
        u16int i=0;
        u32int virt=0xc0000000;
        PhysicalAddress frame=0x100000;
        while(i<1024)
        {
            u32int index = virtual_address(virt).getPageTableIndex();

            // Create a page
            page_t page = page_t();

            LinearMemoryManagerPageChanger::setPresent(&page, true);
            LinearMemoryManagerPageChanger::setAddress(&page, frame);

            // Add it to the page table
            table2_page_table->setPage(index, page);

            // Increment variables
            i++;
            frame+=4096;
            virt+=4096;
        }
    }

    // Create a page struct, and alllocate it.
    page_t kernel_directory_page = page_t();
    pmm->allocPhysPage(&kernel_directory_page);
    // Get the allocated address
    PhysicalAddress kernel_directory_address = kernel_directory_page.getAddress();
    // Construct a page_directory object at the given location
    kernel_directory = new ((void*) kernel_directory_address.getAddress()) page_directory_struct();

    // Map the above tables into the kernel directory
    virtual_address identity_virt = virtual_address((void*) 0x00000000);
    page_table_ptr_struct identity_ptr = page_table_ptr_struct(identity_page_table, true, true, false);
    kernel_directory->setTablePointer(identity_virt.getPageDirectoryIndex(), identity_ptr);
    
    virtual_address table2_virt = virtual_address((void*) 0xc0000000);
    page_table_ptr_struct table2_ptr = page_table_ptr_struct(table2_page_table, true, true, false);
    kernel_directory->setTablePointer(table2_virt.getPageDirectoryIndex(), table2_ptr);

    switch_page_directory(kernel_directory);
    set_paging(true);
}

void LinearMemoryManager::switch_page_directory(page_directory_t *dir)
{
    current_directory = dir;
    set_paging_directory((PhysicalAddress) (u32int) dir);
}

void LinearMemoryManager::allocPage(virtual_address v, page_directory_t *directory)
{
    // Let's create a page object
    page_t page = page_t();
    // And let's get some memory for it
    pmm->allocPhysPage(&page);
    // Then let's set some permissions on it
    // The calls are a bit wierd in order to improve encapsulation
    LinearMemoryManagerPageChanger::setReadWrite(&page, true);
    LinearMemoryManagerPageChanger::setUserMode(&page, false);
    // Now let's map the page into it's location within the tables
    page_table_struct* page_table = directory->getTablePointer(v.getPageDirectoryIndex()).getPointee();
    if (page_table == 0)
    {
        page_t page = page_t();
        pmm->allocPhysPage(&page);
        PhysicalAddress table_page_address = page.getAddress();
        page_table = new ((void*) table_page_address.getAddress()) page_table_struct();
        page_table_ptr_struct table_ptr = page_table_ptr_struct(page_table, true, true, false);
        directory->setTablePointer(v.getPageDirectoryIndex(), table_ptr);

    }

    page_table->setPage(v.getPageTableIndex(), page);

    flush_tlb();
}

void LinearMemoryManager::freePage(virtual_address v, page_directory_t *directory)
{
    // Let's find our corresponding page
    page_table_struct* page_table = directory->getTablePointer(v.getPageDirectoryIndex()).getPointee();
    page_t page = page_table->getPage(v.getPageTableIndex());
    // Now let's unmap it
    page_t empty_page = page_t();
    page_table->setPage(v.getPageTableIndex(), empty_page);
    // And let's deallocate the old page
    pmm->freePhysPage(&page);
    
    flush_tlb();
}

page_directory_t* LinearMemoryManager::getKernelDirectory()
{
    return kernel_directory;
}
