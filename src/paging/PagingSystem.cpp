#include "PagingSystem.hpp"

#include <monitor/printf.hpp>

PagingSystem::PagingSystem(multiboot_info_t* mbd)
{
    printf("Enabled PA");
    pmm = new (physical_pool) PhysicalMemoryManager(mbd);
    printf("GI");
    lmm = new (linear_pool) LinearMemoryManager(pmm);
    printf("NG\n");
}

LinearMemoryManager* PagingSystem::getLinearMemoryManager()
{
    return lmm;
}
