#include "Paging.hpp"

page_table_struct::page_table_struct()
{
    for(u16int x=0; x<PAGES_PER_TABLE; x++)
    {
        pages[x] = page_t();
    }
}

page_t page_table_struct::getPage(u16int index)
{
    return pages[index];
}

void page_table_struct::setPage(u16int index, page_t page)
{
    pages[index] = page;
}
