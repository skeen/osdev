#include "Paging.hpp"

void LinearMemoryManagerPageChanger::setAddress(page_t* page, PhysicalPageAddress ptr)
{
    page->setAddress(ptr);
}

void LinearMemoryManagerPageChanger::setPresent(page_t* page, bool present)
{
    page->setPresent(present);
}

void LinearMemoryManagerPageChanger::setReadWrite(page_t* page, bool value)
{
    page->setReadWrite(value);
}

void LinearMemoryManagerPageChanger::setUserMode(page_t* page, bool value)
{
    page->setUserMode(value);
}
