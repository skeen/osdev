#ifndef _OSDEV_PHYSICAL_MEMORY_MANAGER_HPP
#define _OSDEV_PHYSICAL_MEMORY_MANAGER_HPP

#include "Paging.hpp"

#include <misc/datastructures/Bitset.hpp>

enum PageStatus
{
    FREE = 0,
    TAKEN = 1
};

/**
 * Keeps track of free physical pages of RAM.
 */
class PhysicalMemoryManager
{
    public:
        // Construct the default PhysicalMemoryManager,
        // and use the multiboot header to configure the free memory areas.
        // Note; Allocated using the placement_malloc function. 
        PhysicalMemoryManager(multiboot_info_t* mbd);

        // Allocate a physical page for the provided page struct
        void allocPhysPage(page_t *page);
        
        // Free the physical page associated with the provided page struct
    	void freePhysPage(page_t *page);

        // Various static helper functions to operate on page addresses
        // Uses 'sizeof(page_frame_t)' to determinate page-alignment
        // Note: These are defined in PagingHelper.cpp
        /**
         * Transformate ptr into the nearest lower page-aligned address
         */
        static PhysicalPageAddress floorPageAddress(PhysicalAddress ptr);
        /**
         * Transformate ptr into the nearest upper page-aligned address
         */
        static PhysicalPageAddress ceilPageAddress(PhysicalAddress ptr);
        /**
         * Increment ptr by one page-aligned address
         * Requires; ptr to be page-aligned
         */
        static PhysicalPageAddress incrementPageAddress(PhysicalPageAddress ptr);
        /**
         * Checks whether the given address is page-aligned
         * Returns true if it is, false otherwise
         */
        static PhysicalPageAddress decrementPageAddress(PhysicalPageAddress ptr);
        /**
         * Checks whether the given address is page-aligned
         * Returns true if it is, false otherwise
         */
        static bool checkPageAddress(PhysicalAddress ptr);
    private:
        void freeRegion(PhysicalAddress start_address, size_t length);
        void freePageRegion(PhysicalAddress start_page_address, PhysicalAddress end_page_address);
        void takePageRegion(PhysicalAddress start_page_address, PhysicalAddress end_page_address);

        /**
         * Calculates the address of a pageframe, given an index.
         */
        static PhysicalAddress addressFromIndex(u32int index);

        /**
         * Calculates the index into the pageframe array,
         * given a page-aliged address.
         */
        static u32int indexFromAddress(PhysicalAddress ptr);

        // A bitset of page_frames
        static const u32int number_of_pages = (0xFFFFFFFF / 0x1000) + 1;
        u8int bitset_buffer[sizeof(StaticBitset<number_of_pages>)];
        Bitset* bitset;
        // The actual page_frames index
        static constexpr page_frame_t* page_frames = 0x00000000;
};

#endif //_OSDEV_PHYSICAL_MEMORY_MANAGER_HPP
