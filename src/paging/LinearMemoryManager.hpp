#ifndef _OSDEV_LINEAR_MEMORY_MANAGER_HPP
#define _OSDEV_LINEAR_MEMORY_MANAGER_HPP

#include "PhysicalMemoryManager.hpp"
#include "Paging.hpp"

/**
 * Uses the physical memory manager to allocate and free physical pages of RAM.
 * Uses these pages to reate and maintain page directories, page tables, etc.
 * (Also possibly adds support for swap space, memory mapped files, shared memory,
 *      allocation on demand and such).
 * Can only supply multiples of 4 KB (4,8,12,...)
 */ 
class LinearMemoryManager
{
    public:
        LinearMemoryManager(PhysicalMemoryManager* pmm);

        void allocPage(virtual_address v, page_directory_t *directory);
        void freePage(virtual_address v, page_directory_t *directory);

        page_directory_t* getKernelDirectory();
        /*
        u32int createAddressSpace(void);
        u32int destroyAddressSpace(void *addressSpace);
        */
        void switch_page_directory(page_directory_t *dir);
    private:

        /**
         * Allocate the following data type, and zero it out
         * These are each allocated using the ppm on a single page,
         * which is then returned, these are however not mapped into anything.
         */
        /*
        page_directory_t* allocate_page_directory(page_t* page);
        page_table_t* allocate_page_table(page_t* page);
        */

        PhysicalMemoryManager* pmm;

        page_directory_t* kernel_directory;
        page_directory_t* current_directory;
};

#endif //_OSDEV_LINEAR_MEMORY_MANAGER_HPP
