#include "Paging.hpp"

page_directory_struct::page_directory_struct()
{
    // Clean the table
    for(u32int x=0; x<PAGES_PER_DIRECTORY; x++)
    {
        tables[x] = page_table_ptr_struct();
    }
}

page_table_ptr_struct page_directory_struct::getTablePointer(u16int index)
{
    return tables[index];
}


void page_directory_struct::setTablePointer(u16int index, page_table_ptr_struct ptr)
{
    tables[index] = ptr;
}
