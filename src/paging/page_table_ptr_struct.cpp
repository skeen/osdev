#include "Paging.hpp"

page_table_ptr_struct::page_table_ptr_struct()
    : Present(0), ReadWrite(0), UserMode(0),
      WriteThrough(0), CacheDisabled(0), Accessed(0),
      Zero(0), PageSize(0), Global(0),
      AVAIL(0), Table(0)
{
}

page_table_ptr_struct::page_table_ptr_struct(page_table_struct* tableAddress, bool present, bool readwrite, bool usermode)
{
    // Address
    Table = (u32int) tableAddress >> 12;
    // Flags
    Present = present;
    ReadWrite = readwrite;
    UserMode = usermode;
    // Default values
    WriteThrough = false;
    CacheDisabled = false;
    Accessed = false;
    Zero = 0;
    PageSize = false;
    Global = false;
    AVAIL = 0;
}

page_table_struct* page_table_ptr_struct::getPointee()
{
    // The void pointer cast is not strictly nessicary, however it clearly
    // points out that we're actually working with a pointer, that is then being
    // cast into a specific type.
    PhysicalAddress tmp = Table << 12;
    return (page_table_struct*) (void*) tmp.getAddress();
}
