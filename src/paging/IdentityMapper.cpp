#include "IdentityMapper.hpp"

#include "PhysicalMemoryManager.hpp"

IdentityMapper::IdentityMapper()
{
}

void IdentityMapper::MapAll(page_directory_struct* directory)
{
    // holds the physical address where we want to start mapping these pages
    // from; in this case, we want to map pages from the very beginning of memory,
    // to the end of allocated kernel memory
    PhysicalAddress start_address = 0x0;
    PhysicalAddress end_address = 0xFFFFF000;//(void*) PhysicalMemoryAllocator::getEndOfAllocatedMemory();
    MapRange(directory, start_address, end_address);
}

void IdentityMapper::MapRange(page_directory_struct* directory, PhysicalAddress low, PhysicalAddress high)
{
    // holds the physical address where we want to start mapping these pages
    // from; in this case, we want to map pages from the very beginning of memory,
    // to the end of allocated kernel memory
    PhysicalAddress start_page_address = low;
    PhysicalAddress end_address = high;
    PhysicalAddress end_page_address = PhysicalMemoryManager::ceilPageAddress(end_address);

    //we will fill all 1024 entries, mapping 4 megabytes
    for(PhysicalAddress iterator_page_address = start_page_address;
            iterator_page_address < end_page_address;
            iterator_page_address = PhysicalMemoryManager::incrementPageAddress(iterator_page_address))
    {
        virtual_address v = virtual_address(iterator_page_address);
        page_t page = page_t();

        // The calls are a bit wierd in order to improve encapsulation
        IdentityMapperPageChanger::setAddress(&page, iterator_page_address);
        IdentityMapperPageChanger::setPresent(&page, true);
        IdentityMapperPageChanger::setReadWrite(&page, true);
        IdentityMapperPageChanger::setUserMode(&page, false);

        page_table_struct* page_table = directory->getTablePointer(v.getPageDirectoryIndex()).getPointee();
        page_table->setPage(v.getPageTableIndex(), page);
    }
}
