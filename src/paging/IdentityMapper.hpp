#ifndef _OSDEV_IDENTITY_MAPPER_HPP
#define _OSDEV_IDENTITY_MAPPER_HPP

#include "Paging.hpp"

class IdentityMapper
{
    public:
        static void MapAll(page_directory_struct* directory);
    private:
        IdentityMapper();
        static void MapRange(page_directory_struct* directory, PhysicalAddress start, PhysicalAddress end);
};

#endif //_OSDEV_IDENTITY_MAPPER_HPP
