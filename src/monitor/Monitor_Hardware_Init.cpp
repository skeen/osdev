/**
 * @file	Monitor_Hardware_Init.cpp
 * @author  Emil Madsen <skeen@cs.au.dk>
 * @version 1.0
 *
 * @section LICENSE
 *
 * Public Domain.
 *
 * @section DESCRIPTION
 *
 * Defines variables for Monitor_Hardware
 */
#include <Monitor_Hardware.hpp>

// Initialize the video memory address
u16int* const Monitor_Hardware::video_memory = (u16int* const) 0xB8000;
