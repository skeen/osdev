/**
 * @file	monitor.cpp
 * @author  Emil Madsen <skeen@cs.au.dk>
 * @version 1.0
 *
 * @section LICENSE
 *
 * Public Domain.
 *
 * @section DESCRIPTION
 *
 * Implements the simple interface, defined in Monitor_Hardware.hpp
 */

#include <Monitor_Hardware.hpp>

// Doxygen in monitor.h
bool Monitor_Hardware::detect_type()
{
	u16int *monitor_ptr = (u16int*) 0x410;
	char c = (*monitor_ptr) & 0x30;
	
	//c is 0x00 or 0x20 for color
	//c is 0x30 for mono.
	return (c==0x30);
}

void Monitor_Hardware::initialize()
{
    if(initialized == true)
    {
        return;
    }
    else
    {
        initialized = true;
    /* // Currently this is not checked or supported 
    bool isMonoChrome = detect_type();
	if(isMonoChrome)
	{
		video_memory_pointer = (u16int*) 0xB0000;
	}
	else
	{
		video_memory_pointer = (u16int*) 0xB8000;
	}
    */

    cursor.x = 0;
	cursor.y = 0;
    move_cursor();
  
    color_byte cb = color_byte();
	set_color(cb);

    clear();
    }
}

// Doxygen in monitor.h
void Monitor_Hardware::put(const char c)
{
    // Handle a backspace, by moving the cursor back one space writing a space
	// instead of what's there, and then going back again.
    if (c == 0x08 && cursor.x)
    {
        // Step 1 character back
		cursor.x--;
        // Write a space (clean it)
        char space_character = 0x20;
		put(space_character);
        // As we write the space, we advance once,
        // so lets step 1 character back again
		cursor.x--;
    }

    // Handle a tab by increasing the cursor's X, but only to a point
    // where it is divisible by MONITOR_TAB_WIDTH.
    else if (c == 0x09)
    {
        //TODO: Clean up, and make human readable
        cursor.x = (cursor.x+MONITOR_TAB_WIDTH) & ~(MONITOR_TAB_WIDTH-1);
    }

    // Handle carriage return
    else if (c == '\r')
    {
        cursor.x = 0;
    }

    // Handle newline by moving cursor back to left and increasing the row
    else if (c == '\n')
    {
        cursor.x = 0;
        cursor.y++;
    }

    // Handle any other printable character.
    else if(c >= ' ')
    {
        // Figure the location to set
        u16int* location = video_memory + cursor_offset();
        // Find the color character to print
        color_character colored_char = get_colored_char(c);
        // Output it
        *location = colored_char.color_char;
        // Advance 1 step
        cursor.x++;
    }

    // Check if we need to insert a new line because we have reached the end
    // of the screen.
    if (cursor.x >= MONITOR_WIDTH)
    {
        cursor.x = 0;
        cursor.y++;
    }

    // Scroll the screen if needed.
    scroll();
    // Move the hardware cursor.
    move_cursor();
}

// Doxygen in monitor.h
void Monitor_Hardware::clear()
{
    // Get a space character with the default colour attributes.
    char space_character = 0x20;
    color_character colored_space = get_colored_char(space_character);

    // Clean the entire screen
    for (int i = 0; i < MONITOR_WIDTH*MONITOR_HEIGHT; i++)
    {
        video_memory[i] = colored_space.color_char;
    }

    // Move the hardware cursor back to the start.
    cursor.x = 0;
    cursor.y = 0;
    move_cursor();
}

// Doxygen in monitor.h
void Monitor_Hardware::set_color(color_byte newcolor)
{
   	color = newcolor;
	//Refesh cursor to the new color
	move_cursor();
}

// Doxygen in monitor.h
color_byte Monitor_Hardware::get_color()
{
	return color;
}

// Doxygen in monitor.h
Monitor_Hardware::color_character Monitor_Hardware::get_colored_char(char c)
{
    color_character cc;
    cc.character = c;
    cc.color = color.getColorByte();
	return cc;
}

// Doxygen in monitor.h
u16int Monitor_Hardware::cursor_offset()
{
    u16int offset = cursor.y * MONITOR_WIDTH + cursor.x;
    return offset;
}

// Doxygen in monitor.h
void Monitor_Hardware::move_cursor()
{
    // Calculate the cursor offset
    u16int offset = cursor_offset();
    // Tell the VGA board we are setting the high cursor byte.
    Hardware::outb(0x3D4, 14);        
    // Send the high cursor byte.          
    Hardware::outb(0x3D5, offset >> 8);

    // Tell the VGA board we are setting the low cursor byte.
    Hardware::outb(0x3D4, 15);     
    // Send the low cursor byte.            
    Hardware::outb(0x3D5, offset);      
}

// Doxygen in monitor.h
void Monitor_Hardware::scroll()
{
    // Get a space character with the default colour attributes.
    char space_character = 0x20;
	color_character colored_space = get_colored_char(space_character);

	const u8int LAST_LINE 		 = MONITOR_HEIGHT;
	const u8int SECOND_LAST_LINE = (MONITOR_HEIGHT - 1);

    // Row 25 is the end, this means we need to scroll up
    if(cursor.y >= MONITOR_HEIGHT)
    {
        // Scroll the lines up, by rewriting memory, one line at a time.
		// Do this to all lines, except the last one
        for (int i = 0; i < SECOND_LAST_LINE*MONITOR_WIDTH; i++)
        {
            video_memory[i] = video_memory[i+MONITOR_WIDTH];
        }

        // The first is now scrolled out, and the last one has to be cleared.
		// Do this by writing 80 spaces to it.
        for (int i = SECOND_LAST_LINE*MONITOR_WIDTH; i < LAST_LINE*MONITOR_WIDTH; i++)
        {
            video_memory[i] = colored_space.color_char;
        }

        // The cursor should now be on the last line.
        cursor.y--;
    }
}
