/**
 * @file	color_byte.cpp
 * @author  Emil Madsen <skeen@cs.au.dk>
 * @version 1.0
 *
 * @section LICENSE
 *
 * Public Domain.
 *
 * @section DESCRIPTION
 *
 * Implements the color_byte class described in color_byte.hpp
 */

#include "color_byte.hpp"

// Doxygen in monitor.hpp
color_byte::color_byte()
{
    setForegroundColor(WHITE);
    setBackgroundColor(BLACK);
}

// Doxygen in monitor.hpp
color_byte::color_byte(u8int color)
{
    setColorByte(color);
}

// Doxygen in monitor.hpp
color_byte::color_byte(enum color_byte::colorpart foreground, enum color_byte::colorpart background)
{
    setForegroundColor(foreground);
    setBackgroundColor(background);
}

// Doxygen in monitor.hpp
color_byte::color_byte(u8int foreground, u8int background)
{
    setForegroundColor(foreground);
    setBackgroundColor(background);
}

// Doxygen in monitor.hpp
u8int color_byte::getColorByte()
{
    return color;
}

// Doxygen in monitor.hpp
enum color_byte::colorpart color_byte::getForegroundColor()
{
    return colorcomponents.foreground;
}

// Doxygen in monitor.hpp
enum color_byte::colorpart color_byte::getBackgroundColor()
{
    return colorcomponents.background;
}

// Doxygen in monitor.hpp
void color_byte::setColorByte(u8int color)
{
    this->color = color;
}

// Doxygen in monitor.hpp
void color_byte::setForegroundColor(enum color_byte::colorpart color)
{
    colorcomponents.foreground = color;
}

// Doxygen in monitor.hpp
void color_byte::setForegroundColor(u8int color)
{
    colorcomponents_int.foreground = color;
}

// Doxygen in monitor.hpp
void color_byte::setBackgroundColor(enum color_byte::colorpart color)
{
    colorcomponents.background = color;
}

// Doxygen in monitor.hpp
void color_byte::setBackgroundColor(u8int color)
{
    colorcomponents_int.background = color;
}
