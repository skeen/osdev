#ifndef _OSDEV_PRINTF_HPP
#define _OSDEV_PRINTF_HPP

#include <misc/Common.hpp>
#include "transformer.hpp"

template<typename T>
void print_type(char c, T value)
{
    PrintfErrorHandler::type_mismatch(c);
}

void print_type(char c, const char* value);
void print_type(char c, const char value);
void print_type(char c, s32int value);
void print_type(char c, u32int value);
           
void printf(const char *s);

template<typename T, typename... Args>
void printf(const char *s, T value, Args... args)
{
    // Loop though the entire string
    while (*s) 
    {
        char character = *s;
        // Check if our current character is a precentage
        if (character == '%')
        {
            // If that's the case, then check the next character, if it's a
            // precentage aswell, then print the precentage character, otherwise
            // call the function concerned with printing that type.
            char next_character = *(++s);
            if(next_character == '%')
            {
                // Print the precentage
                print_type('c', '%');
            }
            else
            {
                // Print whatever
                print_type(next_character, value);
                // And skip one character ahead
                s++;
            }
            // Print the rest of the string recursively
            printf(s, args...); 
            return;
        }
        else
        {
            // Character was not a precentage, just print it
            print_type('c', character);
            s++;
        }   
    }
    PrintfErrorHandler::extra_arguments();
}

#endif //_OSDEV_PRINTF_HPP
