/**
 * @file	Transformer.hpp
 * @author  Emil Madsen <skeen@cs.au.dk>
 * @version 1.0
 *
 * @section LICENSE
 *
 * Public Domain.
 *
 * @section DESCRIPTION
 *
 * Implements a simple type transformer, that transforms a given input into a
 * series of chars (passed onto the underlaying CharacterPrinter).
 */

#ifndef _OSDEV_TRANSFORMER_HPP
#define _OSDEV_TRANSFORMER_HPP

#include <misc/common.hpp>
#include "PrintfErrorHandler.hpp"

namespace Transformer
{
        /**
         * Clear the screen
         *
         * @return 				void		Returns void.
         */
        void clear_screen();

        /**
         * Write the C String out to the screen. 
         *
         * @param[in]  string	const char*	The C string to be printed.
         * @return 				void		Returns void.
         */
        void put_character(const char c);

        /**
         * Write the C String out to the screen. 
         *
         * @param[in]  string	const char*	The C string to be printed.
         * @return 				void		Returns void.
         */
        void put_string(const char* string);

        /**
         * Write the hex value of n to the monitor.
         *
         * @param[in]  n	u32int		The number to be converted and printed.
         * @return 			void		Returns void.
         */
        void put_hex(u32int n);

        /**
         * Write the hex value of n to the monitor.
         *
         * @param[in]  n	u32int		The number to be converted and printed.
         * @return 			void		Returns void.
         */
        void put_capital_hex(u32int n);

        /**
         * Write the hex value of n to the monitor.
         *
         * @param[in]  n	u32int		The number to be converted and printed.
         * @return 			void		Returns void.
         */
        void put_octal(u32int n);

        /**
         * Write the dec value of n to the monitor.
         *
         * @param[in]  n	u32int		The number to be converted and printed.
         * @return 			void		Returns void.
         */
        void put_decimal(u32int n);

        /**
         * Write the dec value of n to the monitor.
         *
         * @param[in]  n	u32int		The number to be converted and printed.
         * @return 			void		Returns void.
         */
        void put_signed_decimal(s32int n);

        /**
         * Write the bool value of boolean to the monitor.
         *
         * @param[in]  boolean	bool		The bool to be converted and printed.
         * @return 			    void		Returns void.
         */
        void put_boolean(bool boolean);

        /**
         * Write the binary value of n to the monitor.
         *
         * @param[in]  n	u32int		The number to be converted and printed.
         * @return 			void		Returns void.
         */
        void put_binary(u32int n);
}

#endif //_OSDEV_TRANSFORMER_HPP
