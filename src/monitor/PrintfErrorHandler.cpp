#include <PrintfErrorHandler.hpp>
#include <Printf.hpp>

namespace PrintfErrorHandler
{
    void extra_arguments()
    {
        printf("ERROR; Extra Arguments given to printf");
    }

    void type_mismatch(char c)
    {
        printf("ERROR; Type mismatch in printf");
    }

    void missing_arguments(char c)
    {
        printf("ERROR; Missing arguments(given %c) in printf", c);
    }
}
