/**
 * @file	Transformer.cpp
 * @author  Emil Madsen <skeen@cs.au.dk>
 * @version 1.0
 *
 * @section LICENSE
 *
 * Public Domain.
 *
 * @section DESCRIPTION
 *
 * Implements Transformer.hpp
 */
#include <Transformer.hpp>
#include <CharacterPrinter.hpp>
#include <Monitor_Hardware.hpp>

namespace Transformer
{
// Doxygen in Transformer.h
void put_character(const char c)
{
    Monitor_Hardware::instance().put(c);
}

// Doxygen in Transformer.h
void put_string(const char* c)
{
    // Start at string index 0
    int i = 0;
    // Keeping going while c[i] != '\0'
    while (c[i])
    {
        // Put the character at c[i] to the screen
        put_character(c[i]);
        // Advance i with 1
        i = i + 1;
    }
}

// Doxygen in Transformer.h
void put_hex(u32int n)
{
    //TODO: MAKE READABLE
    s32int tmp;

    put_string("0x");

    char noZeroes = 1;

    int i;
    for (i = 28; i > 0; i -= 4)
    {
        tmp = (n >> i) & 0xF;
        if (tmp == 0 && noZeroes != 0)
        {
            continue;
        }
    
        if (tmp >= 0xA)
        {
            noZeroes = 0;
            put_character(tmp-0xA+'a' );
        }
        else
        {
            noZeroes = 0;
            put_character( tmp+'0' );
        }
    }
  
    tmp = n & 0xF;
    if (tmp >= 0xA)
    {
        put_character(tmp-0xA+'a');
    }
    else
    {
       put_character(tmp+'0');
    }
}

// Doxygen in Transformer.h
void put_capital_hex(u32int n)
{
    //TODO: Seperate implemention
    put_hex(n);
}

// Doxygen in Transformer.h
void put_octal(u32int n)
{
    //TODO: Implement
}

// Doxygen in Transformer.h
void put_decimal(u32int n)
{
    //TODO: MAKE READABLE

    if (n == 0)
    {
        put_character('0');
        return;
    }

    s32int acc = n;
    char c[32];
    int i = 0;
    while (acc > 0)
    {
        c[i] = '0' + acc%10;
        acc /= 10;
        i++;
    }
    c[i] = 0;

    char c2[32];
    c2[i--] = 0;
    int j = 0;
    while(i >= 0)
    {
        c2[i--] = c[j++];
    }
    put_string(c2);
}

// Doxygen in Transformer.h
void put_signed_decimal(s32int n)
{
    if(n < 0)
    {
        put_character('-');
    }
    put_decimal(n);
}

// Doxygen in Transformer.h
void put_boolean(bool boolean)
{
    if(boolean)
    {
        put_string("true");
    }
    else
    {
        put_string("false");
    }
}

// Doxygen in Transformer.h
void put_binary(u32int n)
{
    if (n == 0 || n == 1) 
    {
        put_decimal(n);
    }
    else
    {
        put_binary(n >> 1);
        put_decimal(n & 0x1);
    }
}
}
