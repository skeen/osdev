#include <printf.hpp>

void printf(const char *s)
{
    while (*s) 
    {
        char character = *s;
        if (character == '%')
        {
            char next_character = *(++s);
            if(next_character != '%')
            {
                PrintfErrorHandler::missing_arguments(next_character);
            }
        }
        print_type('c', character);
        s++;
    }
}

void print_type(char c, const char* value)
{
    if (c == 's')
    {
        Transformer::put_string(value);
    }
    else
    {
        PrintfErrorHandler::type_mismatch(c);
    }
}

void print_type(char c, const char value)
{
    if (c == 'c')
    {
        Transformer::put_character(value);
    }
    else
    {
        PrintfErrorHandler::type_mismatch(c);
    }
}

void print_type(char c, s32int value)
{
    switch(c)
    {
        case 'd':
        case 'i':
            Transformer::put_signed_decimal(value);
            break;
        case 'o':
        case 'u':
        case 'x':
        case 'X':
            print_type(c, (u32int) value);
            break;
        default:
            PrintfErrorHandler::type_mismatch(c);
            break;
    }
}

void print_type(char c, u32int value)
{
    switch(c)
    {
        case 'd':
        case 'i':
            print_type(c, (s32int) value);
            break;

        case 'o':
            Transformer::put_octal(value);
            break;
        case 'u':
            Transformer::put_decimal(value);
            break;
        case 'x':
            Transformer::put_hex(value);
            break;
        case 'X':
            Transformer::put_capital_hex(value);
            break;
        default:
            PrintfErrorHandler::type_mismatch(c);
            break;
    }
}
