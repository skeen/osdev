/**
 * @file	CharacterPrinter.hpp
 * @author  Emil Madsen <skeen@cs.au.dk>
 * @version 1.0
 *
 * @section LICENSE
 *
 * Public Domain.
 *
 * @section DESCRIPTION
 *
 * Defines a pretty interface for a character printer.
 */

#ifndef _OSDEV_CHARACTER_PRINTER_HPP
#define _OSDEV_CHARACTER_PRINTER_HPP

class CharacterPrinter
{
    //----------
    // Datatypes
    public:
    private:
    protected:
    //----------
    // Functions
    public:
        /**
         * Write a single character out to the screen.
         *
         * @param[in]  c		char	The ASCII character to be printed.
         * @return 				void	Returns void.
         */
        virtual void put(const char c) = 0;

        /**
         * Clears the screen.
         *
         * @return 			void	Returns void.
         */
        virtual void clear() = 0;
    private:
    protected:
    //----------
    // Variables
    public:
    private:
    protected:
};

#endif //_OSDEV_CHARACTER_PRINTER_HPP
