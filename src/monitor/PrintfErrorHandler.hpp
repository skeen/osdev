#ifndef _OSDEV_PRINTF_ERROR_HANDLER_HPP
#define _OSDEV_PRINTF_ERROR_HANDLER_HPP

namespace PrintfErrorHandler
{
    void extra_arguments();
    void type_mismatch(char c);
    void missing_arguments(char c);
};  

#endif //_OSDEV_PRINTF_ERROR_HANDLER_HPP
