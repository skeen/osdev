#ifndef __CXA_PURE_VIRTUAL_CPP
#define __CXA_PURE_VIRTUAL_CPP

extern "C" void __cxa_pure_virtual()
{
    // Do nothing!
}

void *__dso_handle;
 
int __cxa_atexit(void (*destructor) (void *), void *arg, void *dso);
void __cxa_finalize(void *f);

namespace __cxxabiv1 
{
    __extension__ typedef int __guard __attribute__((mode(__DI__)));

    extern "C" int __cxa_guard_acquire (__guard *);
    extern "C" void __cxa_guard_release (__guard *);
    extern "C" void __cxa_guard_abort (__guard *);

    extern "C" int __cxa_guard_acquire (__guard *g) 
    {
        return !*(char *)(g);
    }

    extern "C" void __cxa_guard_release (__guard *g)
    {
        *(char *)g = 1;
    }

    extern "C" void __cxa_guard_abort (__guard *)
    {

    }
}


#endif //__CXA_PURE_VIRTUAL_CPP
